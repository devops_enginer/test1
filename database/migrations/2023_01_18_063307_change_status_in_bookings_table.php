<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class ChangeStatusInBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            DB::table('bookings')->where('status', 'pending')->update(['status' => 'canceled']);
            DB::statement("ALTER TABLE bookings MODIFY COLUMN status ENUM('booked', 'completed', 'canceled')");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            DB::table('bookings')->where('booked', 'pending')->update(['status' => 'canceled']);
            DB::statement("ALTER TABLE bookings MODIFY COLUMN status ENUM('pending', 'completed', 'canceled')");
        });
    }
}
