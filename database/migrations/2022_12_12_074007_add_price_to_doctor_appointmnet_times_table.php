<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriceToDoctorAppointmnetTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctor_appointment_times', function (Blueprint $table) {
            $table->decimal('price', 8, 2)->default(0.0)->after('doctor_appointment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctor_appointment_times', function (Blueprint $table) {
            $table->dropColumn('price');
        });
    }
}
