<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTapPaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tap_payment_methods', function (Blueprint $table) {
            $table->id();

            $table->string('token');
            $table->string('customer_id');
            $table->string('card_id');
            $table->string('card_type');
            $table->string('customer_name');
            $table->string('customer_email');
            $table->string('customer_phone');
            $table->string('card_holder_name');
            $table->string('card_number');
            $table->string('card_exp_month');
            $table->string('card_exp_year');
            $table->string('card_cvc');
            
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tap_payment_methods');
    }
}
