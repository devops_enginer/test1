<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriceToClinicTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_tests', function (Blueprint $table) {
            $table->decimal('sale_price', 8, 2)->default(0.0)->after('clinic_id');
            $table->decimal('test_price', 8, 2)->default(0.0)->after('clinic_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_tests', function (Blueprint $table) {
            $table->dropColumn('sale_price');
            $table->dropColumn('test_price');
        });
    }
}
