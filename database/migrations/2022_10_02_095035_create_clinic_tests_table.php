<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinic_tests', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('test_id');
            $table->foreign('test_id')
                ->references('id')->on('tests')
                ->onDelete('cascade');

            $table->unsignedBigInteger('clinic_id');
            $table->foreign('clinic_id')
                ->references('id')->on('clinics')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinic_tests');
    }
}
