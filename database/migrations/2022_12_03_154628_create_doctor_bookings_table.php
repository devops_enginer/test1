<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_bookings', function (Blueprint $table) {
            $table->id();

            $table->string('comment')->nullable();
            $table->string('status');
            $table->tinyInteger('is_rescheduled')->default(0);

            $table->unsignedBigInteger('doctor_appointment_time_id');
            $table->foreign('doctor_appointment_time_id')
                ->references('id')->on('doctor_appointment_times')
                ->onDelete('cascade');

            $table->unsignedBigInteger('reason_id')->nullable();
            $table->foreign('reason_id')
                ->references('id')->on('reasons')
                ->onDelete('cascade');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_bookings');
    }
}
