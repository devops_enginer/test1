<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingResultParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_result_parameters', function (Blueprint $table) {
            $table->id();

            $table->string('test');
            $table->string('result');
            $table->string('units')->nullable();
            $table->string('reference_range')->nullable();
            $table->string('methodology')->nullable();

            $table->unsignedBigInteger('result_id');
            $table->foreign('result_id')
                ->references('id')->on('results')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_result_parameters');
    }
}
