<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'full_name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make(123456),
            'phone_number' => '+971000000000',
            'account_type' => 'normal',
            'address_line_1' => 'Dubai',
            'address_line_2' => 'Dubai',
            'language' => 'en',
            'city_id' => 1,
            'country_id' => 1,
            'is_admin' => 1
        ]);
    }
}
