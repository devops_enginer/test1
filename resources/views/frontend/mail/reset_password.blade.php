<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ __('Reply') }}</title>
</head>
<body>
<h1>{{ __('Forget Password')}}</h1>

{{ __('Use This Token To Reset Your Password:')}}
<p>{{$token}}</p>
</body>
</html>
