<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>Register | Health Lab</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('dashboard/images/favicon.ico')}}">

    <!-- Bootstrap Css -->
    <link href="{{asset('dashboard/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{asset('dashboard/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{asset('dashboard/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css" />
    <!-- intlTelInput -->
    <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
    <style>
        .iti--allow-dropdown input, .iti {
            width: 100% !important;

        }
        .iti--allow-dropdown input{
            border: 1px solid #ced4da;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
        }

    </style>
</head>

<body data-topbar="colored">

<!-- <body data-layout="horizontal" data-topbar="colored"> -->


<!-- Background -->
<div class="account-pages"></div>

<!-- Begin page -->
<div class="wrapper-page">
    <div class="card">
        <div class="card-body">

            <div class="auth-logo">
                <h3 class="text-center">
                   <span class="logo-lg">
                        <img src="{{asset('assets/images/the-digital-hotel.webp')}}" alt="" height="50">
                    </span>
                </h3>
            </div>

            <div class="p-3">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label" for="name">Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Enter name">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="email">Email</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Enter email">
                    </div>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="mb-3">
                        <label class="form-label" for="password">Password</label>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Enter password">
                    </div>
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <div class="mb-3">
                        <label class="form-label" for="password_confirmation">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation"
                               placeholder="Confirm Password">
                    </div>
                    @error('password_confirmation')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="mb-3">
                        <label class="form-label" for="phone">Phone Number</label>
                        <input class="form-control @error('phone_number') is-invalid @enderror"  type="tel"
                               pattern="[+]{0,1}[0-9]{8,13}" name="phone_number" id="phone_number"
                               minlength="9" maxlength="13" value="+971"
                               autocomplete="phone_number" autofocus placeholder="Enter the phone" required>
                    </div>
                    <div class="mb-3 row">
                        <div class="col-12 text-end">
                            <button class="btn btn-primary w-md waves-effect waves-light"
                                    type="submit">Register</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div class="text-center">
        <p class="text-muted">Already have an account ? <a href="{{route('login')}}" class="text-white"> Login </a> </p>
        <p class="text-muted">©
            <script>document.write(new Date().getFullYear())</script> Health Lab
        </p>
    </div>
</div>

<!-- JAVASCRIPT -->
<script src="{{asset('dashboard/libs/jquery/jquery.min.js')}}"></script>
<script src="{{asset('dashboard/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('dashboard/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('dashboard/libs/simplebar/simplebar.min.js')}}"></script>
<script src="{{asset('dashboard/libs/node-waves/waves.min.js')}}"></script>
<script src="{{asset('dashboard/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>
<script>
    var inputt = document.querySelector('#phone_number');
    var countryData = window.intlTelInputGlobals.getCountryData();
    var addressDropdow = document.querySelector("#addresss-country");
    var it2 = window.intlTelInput(inputt, {
        utilScript: 'js/pages/utils.js'
    });
    for (var i = 0; i < countryData.length; i++) {
        var country = countryData[i];
        var optionNode = document.createElement("option");
        optionNode.value = country.iso2;
        var textNode = document.createTextNode(country.name);
        optionNode.appendChild(textNode);
        addressDropdow.appendChild(optionNode);
    }

    addressDropdow.value = it2.getSelectedCountryData().iso2;

    // listen to the telephone input for changes
    inputt.addEventListener('countrychange', function(e) {
        addressDropdow.value = it2.getSelectedCountryData().iso2;
    });

    // listen to the address dropdown for changes
    addressDropdow.addEventListener('change', function() {
        it2.setCountry(this.value);
    });
</script>


<!-- App js -->
<script src="{{asset('dashboard/js/app.js')}}"></script>

</body>

</html>
