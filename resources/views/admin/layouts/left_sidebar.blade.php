<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">
        <div class="user-details">
            <div class="d-flex">
                <div class="me-2">
                    <img src="{{asset('dashboard/images/users/avatar-4.jpg')}}" alt="" class="avatar-md rounded-circle">
                </div>
                <div class="user-info w-100">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Donald Johnson
                            <i class="mdi mdi-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)" class="dropdown-item"><i class="mdi mdi-account-circle text-muted me-2"></i>
                                    Profile<div class="ripple-wrapper me-2"></div>
                                </a></li>
                            <li><a href="javascript:void(0)" class="dropdown-item"><i class="mdi mdi-cog text-muted me-2"></i>
                                    Settings</a></li>
                            <li><a href="javascript:void(0)" class="dropdown-item"><i class="mdi mdi-lock-open-outline text-muted me-2"></i>
                                    Lock screen</a></li>
                            <li><a href="javascript:void(0)" class="dropdown-item"><i class="mdi mdi-power text-muted me-2"></i>
                                    Logout</a></li>
                        </ul>
                    </div>

                    <p class="text-white-50 m-0">{{__('Administrator')}}</p>
                </div>
            </div>
        </div>


        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">{{__('Main')}}</li>

                <li>
                    <a href="{{route('admin.home')}}" class="waves-effect">
                        <i class="mdi mdi-home"></i><!-- <span class="badge bg-primary float-end">0</span> -->
                        <span>{{__('Dashboard')}}</span>

                    </a>
                </li>

                <li>
                    <a href="{{route('static.content.edit')}}" class="waves-effect">
                        <i class="fa fa-globe"></i><!-- <span class="badge bg-primary float-end">0</span> -->
                        <span>{{__('Website Content')}}</span>

                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-flag"></i>
                        <span>{{__('Countries')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('country.country_list')}}">{{__('Countries List')}}</a></li>
                        <li><a href="{{route('country.create')}}">{{__('Create Country')}}</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-flag"></i>
                        <span>{{__('Cities')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('city.city_list')}}">{{__('Cities List')}}</a></li>
                        <li><a href="{{route('city.create')}}">{{__('Create City')}}</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-user"></i>
                        <span>{{__('Users')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('users.user_list')}}">{{__('Users List')}}</a></li>
                        <li><a href="{{route('users.create')}}">{{__('Create User')}}</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-image"></i>
                        <span>{{__('Promotional Banners')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('promotional.banner.banner_list')}}">{{__('Banners List')}}</a></li>
                        <li><a href="{{route('promotional.banner.create')}}">{{__('Create Banner')}}</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-list-alt"></i>
                        <span>{{__('Categories')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('service.service_list')}}">{{__('Test Categories List')}}</a></li>

                        {{--
                        <li><a href="{{route('doctor.category.doctor_category_list')}}">{{__('Doctors Categories List')}}</a></li>
                        --}}
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-list"></i>
                        <span>{{__('Test')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('test.test_list')}}">{{__('Tests List')}}</a></li>
                        <li><a href="{{route('test.create')}}">{{__('Create Test')}}</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-hospital"></i>
                        <span>{{__('Lab')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('clinic.clinic_list')}}">{{__('Labs List')}}</a></li>
                        <li><a href="{{route('clinic.create')}}">{{__('Create Lab')}}</a></li>
                    </ul>
                </li>

                {{--
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-user-md"></i>
                        <span>{{__('Doctors')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('doctor.doctor_list')}}">{{__('Doctors List')}}</a></li>
                        <li><a href="{{route('doctor.create')}}">{{__('Create Doctor')}}</a></li>
                    </ul>
                </li>
                --}}

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-calendar-alt"></i>
                        <span>{{__('Time Slots')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('appointment.appointment_list')}}">{{__('Labs Time Slots List')}}</a></li>

                        {{--
                        <li><a href="{{route('doctor.appointment.appointment_list')}}">{{__('Doctors Time Slots List')}}</a></li>
                        --}}
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-code"></i>
                        <span>{{__('Promo Code')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('promo.code.code_list')}}">{{__('Promo Codes List')}}</a></li>
                        <li><a href="{{route('promo.code.create')}}">{{__('Create Promo Code')}}</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-bell"></i>
                        <span>{{__('Notification')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('notification.notification_list')}}">{{__('Notifications List')}}</a></li>
                        <li><a href="{{route('notification.create')}}">{{__('Send Notification')}}</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-book-open"></i>
                        <span>{{__('Booking')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="test-booking-all-list"><a href="{{route('booking.booking_list')}}">{{__('Tests Bookings List')}}</a></li>
                        <li class="test-booking-rescheduled-list"><a href="{{route('booking.booking_list')}}?rescheduled=1">{{__('Rescheduled Tests Bookings')}}</a></li>

                        {{--
                        <li class="doctor-booking-all-list"><a href="{{route('doctor.booking.booking_list')}}">{{__('Doctors Bookings List')}}</a></li>
                        <li class="doctor-booking-rescheduled-list"><a href="{{route('doctor.booking.booking_list')}}?rescheduled=1">{{__('Rescheduled Doctors Bookings')}}</a></li>
                        --}}
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-stop"></i>
                        <span>{{__('Reason')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('reason.reason_list')}}">{{__('Reasons List')}}</a></li>
                        <li><a href="{{route('reason.create')}}">{{__('Create Reason')}}</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
