<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 text-center">
                <script>document.write(new Date().getFullYear())</script> © Health Lab <span
                    class="d-none d-sm-inline-block">- Crafted with <i class="mdi mdi-heart text-primary"></i> by <a href="https://start-tech.ae/">Start-Tech.</a></span>
            </div>

        </div>
    </div>
</footer>