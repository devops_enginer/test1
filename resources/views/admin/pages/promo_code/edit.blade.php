@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

        </style>
    @endpush

        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Edit Promo Code')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('promo.code.edit', $promoCode) }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Edit Promo Code</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('promo.code.update', $promoCode->id)}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf
                                        @method('PUT')
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="code">
                                                        Code
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('code') is-invalid @enderror"
                                                           id="code"
                                                           placeholder="Code"
                                                           name="code"
                                                           value="{{ $promoCode->code }}"
                                                           >
                                                    @error('code')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="value">
                                                        Value
                                                    </label>
                                                    <input type="number"
                                                           step="0.01"
                                                           class="form-control @error('value') is-invalid @enderror"
                                                           id="value"
                                                           placeholder="Value"
                                                           name="value"
                                                           value="{{ $promoCode->value }}"
                                                           >
                                                    @error('value')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="type">
                                                        Type
                                                    </label>
                                                    <select class="form-control
                                                    type-select @error('type') is-invalid @enderror"
                                                    name="type"
                                                    value="{{ $promoCode->type }}">
                                                        <option value="">Select Type</option>
                                                        <option
                                                        @if($promoCode->type == FIXED_CODE_TYPE)
                                                        selected
                                                        @endif
                                                        value="{{FIXED_CODE_TYPE}}">Fixed Amount</option>
                                                        <option
                                                        @if($promoCode->type == PERCENTAGE_CODE_TYPE)
                                                        selected
                                                        @endif
                                                        value="{{PERCENTAGE_CODE_TYPE}}">Percentage</option>
                                                    </select>
                                                    @error('type')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="limit">
                                                        Limit
                                                    </label>
                                                    <input type="number"
                                                           class="form-control @error('limit') is-invalid @enderror"
                                                           id="limit"
                                                           placeholder="Limit"
                                                           name="limit"
                                                           value="{{ $promoCode->limit }}"
                                                           >
                                                    @error('limit')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="expiry_date">
                                                        Expiry date
                                                    </label>
                                                    <input type="datetime-local"
                                                           class="form-control @error('expiry_date') is-invalid @enderror"
                                                           id="expiry_date"
                                                           placeholder="Expiry date"
                                                           name="expiry_date"
                                                           value="{{ $promoCode->expiry_date }}"
                                                           >
                                                    @error('expiry_date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->


    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <!--intlTelInput-->
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>
        
        <script>
            // var inputt = document.querySelector('#phone_number');
            var countryData = window.intlTelInputGlobals.getCountryData();
            var addressDropdow = document.querySelector("#addresss-country");
            // var it2 = window.intlTelInput(inputt, {
            //     utilScript: "js/utils"
            // });
            for (var i = 0; i < countryData.length; i++) {
                var country = countryData[i];
                var optionNode = document.createElement("option");
                optionNode.value = country.iso2;
                var textNode = document.createTextNode(country.name);
                optionNode.appendChild(textNode);
                // addressDropdow.appendChild(optionNode);
            }

            // addressDropdow.value = it2.getSelectedCountryData().iso2;

            // listen to the telephone input for changes
            // inputt.addEventListener('countrychange', function (e) {
                // addressDropdow.value = it2.getSelectedCountryData().iso2;
            // });

            // listen to the address dropdown for changes
            // addressDropdow.addEventListener('change', function () {
            //     it2.setCountry(this.value);
            // });
        </script>

        <script>
            $(document).ready(function() {
                $('.type-select').select2();
            });
        </script>
        <script>

            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
