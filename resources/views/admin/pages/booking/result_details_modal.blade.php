<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="viewModalLongTitle">{{ __('Details') }}</h5>
                <button type="button" class="close btn btn-primary text-white" data-bs-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="printDiv">
                <div class="row">
                    <div class="col-lg-12 mb-4">
                        <!-- Simple Tables -->
                        <div class="card">
                            <div
                                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                <h6 class="m-0 font-weight-bold text-primary">
                                    {{ __('Result Details') }}
                                </h6>
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>{{ __('Test') }}</th>
                                            <th>{{ __('Result') }}</th>
                                            <th>{{ __('Reference Range') }}</th>
                                            <th>{{ __('Methodology') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($parameters as $parameter)
                                            <tr>
                                                <td>{{ $parameter->test }}
                                                </td>
                                                <td>{{ $parameter->result }}</td>
                                                <td>{{ $parameter->reference_range }}</td>
                                                <td>{{ $parameter->methodology }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger me-2"
                    data-bs-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
