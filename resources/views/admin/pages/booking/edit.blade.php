@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

            .select2-container{
                width: 100% !important;
            }

        </style>
    @endpush

        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Edit Booking')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('booking.edit', $booking) }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Edit Booking</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('booking.update', $booking->id)}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf
                                        @method('PUT')
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="test_id">
                                                        Test
                                                    </label>
                                                    <select class="form-control
                                                    test-select @error('test_id') is-invalid @enderror"
                                                    name="test_id"
                                                    value="{{ old('test_id') }}">
                                                        <option value="">Select Test</option>
                                                        @foreach($tests as $key => $test)
                                                            <option
                                                                value="{{$test->id}}"
                                                                @if($booking->appointmentTime->appointment->test_id == $test->id)
                                                                selected
                                                                @endif
                                                                >{{$test->title}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('test_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="clinic_id">
                                                        Lab
                                                    </label>
                                                    <select class="form-control
                                                    clinic-select @error('clinic_id') is-invalid @enderror"
                                                    name="clinic_id"
                                                    value="{{ old('clinic_id') }}">
                                                        @foreach($clinics as $key => $clinic)
                                                            <option
                                                                value="{{$clinic->id}}"
                                                                @if($booking->appointmentTime->appointment->clinic_id == $clinic->id)
                                                                selected
                                                                @endif
                                                                >{{$clinic->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('clinic_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="appointment_id">
                                                        Date
                                                    </label>
                                                    <select class="form-control
                                                    appointment-select @error('appointment_id') is-invalid @enderror"
                                                    name="appointment_id"
                                                    value="{{ old('appointment_id') }}">
                                                        <option value="">Select Date</option>
                                                        @foreach($appointments as $key => $appointment)
                                                            <option
                                                                value="{{$appointment->id}}"
                                                                @if($booking->appointmentTime->appointment_id == $appointment->id)
                                                                selected
                                                                @endif
                                                                >{{$appointment->date}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('appointment_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="appointment_time_id">
                                                        Time
                                                    </label>
                                                    <select class="form-control
                                                    time-select @error('appointment_time_id') is-invalid @enderror"
                                                    name="appointment_time_id"
                                                    value="{{ old('appointment_time_id') }}">
                                                        @foreach($appointmentTimes as $key => $appointmentTime)
                                                            @if($booking->appointment_time_id == $appointmentTime->id)
                                                            <option selected value="{{$appointmentTime->id}}">{{$appointmentTime->time}}</option>
                                                            @else
                                                            <option value="{{$appointmentTime->id}}">{{$appointmentTime->time}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @error('appointment_time_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="user_id">
                                                        User
                                                    </label>
                                                    <select class="form-control
                                                    user-select @error('user_id') is-invalid @enderror"
                                                    name="user_id"
                                                    value="{{ old('user_id') }}">
                                                        @foreach($users as $key => $user)
                                                            @if($booking->user_id == $user->id)
                                                            <option selected value="{{$user->id}}">{{$user->full_name}}</option>
                                                            @else
                                                            <option value="{{$user->id}}">{{$user->full_name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @error('user_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="status">
                                                        Status
                                                    </label>
                                                    <select class="form-control
                                                    status-select @error('status') is-invalid @enderror"
                                                    name="status">
                                                        <option
                                                        @if($booking->status == PENDING_BOOKING_STATUS)
                                                        selected
                                                        @endif
                                                        value="{{PENDING_BOOKING_STATUS}}">Booked</option>
                                                        <option
                                                        @if($booking->status == COMPLETED_BOOKING_STATUS)
                                                        selected
                                                        @endif
                                                        value="{{COMPLETED_BOOKING_STATUS}}">Completed</option>
                                                        <option
                                                        @if($booking->status == CANCELED_BOOKING_STATUS)
                                                        selected
                                                        @endif
                                                        value="{{CANCELED_BOOKING_STATUS}}">Canceled</option>
                                                    </select>
                                                    @error('status')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <div class="row reason-row {{($booking->status != CANCELED_BOOKING_STATUS && $booking->is_rescheduled == 0) ? 'hidden' : ''}}">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="user_id">
                                                        Reason To Cancel or Reschedule
                                                    </label>
                                                    <select class="form-control
                                                    reason-select @error('reason_id') is-invalid @enderror"
                                                    name="reason_id"
                                                    value="{{ old('reason_id') }}">
                                                        <option value=""></option>
                                                        @foreach($reasons as $key => $reason)
                                                            <option
                                                                value="{{$reason->id}}"
                                                                @if($booking->reason_id == $reason->id)
                                                                selected
                                                                @endif
                                                                >{{$reason->text}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('reason_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6 reason-comment">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="user_id">
                                                        Reason Comment
                                                    </label>
                                                    <textarea
                                                        class="form-control @error('comment') is-invalid @enderror"
                                                        name="comment"
                                                        >{{$booking->comment}}</textarea>
                                                    @error('comment')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row" id="file_container">
                                            <div class="col-lg-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="file_select">
                                                        Result File
                                                    </label>
                                                    <input type="file"
                                                           class="form-control" id="file_select"
                                                           name="file">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->

        <div id="getAppointmentsUrl" data-url="{{route('clinic.test.appointments')}}"></div>
        <div id="getAppointmentTimesUrl" data-url="{{route('appointment.times')}}"></div>

    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <!--intlTelInput-->
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>
        
        <script>
            // var inputt = document.querySelector('#phone_number');
            var countryData = window.intlTelInputGlobals.getCountryData();
            var addressDropdow = document.querySelector("#addresss-country");
            // var it2 = window.intlTelInput(inputt, {
            //     utilScript: "js/utils"
            // });
            for (var i = 0; i < countryData.length; i++) {
                var country = countryData[i];
                var optionNode = document.createElement("option");
                optionNode.value = country.iso2;
                var textNode = document.createTextNode(country.name);
                optionNode.appendChild(textNode);
                // addressDropdow.appendChild(optionNode);
            }

            // addressDropdow.value = it2.getSelectedCountryData().iso2;

            // listen to the telephone input for changes
            // inputt.addEventListener('countrychange', function (e) {
                // addressDropdow.value = it2.getSelectedCountryData().iso2;
            // });

            // listen to the address dropdown for changes
            // addressDropdow.addEventListener('change', function () {
            //     it2.setCountry(this.value);
            // });
        </script>

        <script>
            $(document).ready(function() {
                $('.test-select').select2();

                $('.reason-select').select2({placeholder: 'Select Reason'});

                $('.appointment-select').select2();
                $('.user-select').select2();
                $('.clinic-select').select2();
                $('.status-select').select2();
                $('.time-select').select2();
            });
        </script>
        <script>

            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <script>
            function updateAppointmentSelect(appointments){
                var options = '';
                for (var i = appointments.length - 1; i >= 0; i--) {
                    options += '<option value="' + appointments[i].id + '">' + appointments[i].date + '</option>';
                }
                $('.appointment-select').html(options);
                $('.appointment-select').val('');
                $('.appointment-select').select2({placeholder: 'Select Date'});
            }

            function updateAppointmentTimeSelect(times){
                var options = '';
                for (var i = times.length - 1; i >= 0; i--) {
                    options += '<option value="' + times[i].id + '">' + times[i].time + '</option>';
                }
                $('.time-select').html(options);
                $('.time-select').select2({placeholder: 'Select Time'});
            }

            function getAppointments(testId, clinicId){
                if(!testId || !clinicId){
                    $('.appointment-select').html('');
                    $('.appointment-select').select2({placeholder: 'Select Date'});

                    $('.time-select').html('');
                    $('.time-select').select2({placeholder: 'Select Time'});
                    return;
                }

                var url = $('#getAppointmentsUrl').data('url');
                $.ajax({
                    url: url,
                    data:{
                        test_id: testId,
                        clinic_id: clinicId,
                    },
                    success: function(data){
                        updateAppointmentSelect(data.appointments);
                    },
                    error: function(_, __, ___){

                    }
                });
            }

            function getAppointmentTimes(appointmentId){
                if(!appointmentId){
                    $('.time-select').html('');
                    $('.time-select').select2({placeholder: 'Select Time'});
                    return;
                }

                var url = $('#getAppointmentTimesUrl').data('url');
                $.ajax({
                    url: url,
                    data:{
                        appointment_id: appointmentId,
                    },
                    success: function(data){
                        updateAppointmentTimeSelect(data.times);
                    },
                    error: function(_, __, ___){

                    }
                });
            }

            $(document).ready(function(){
                $('.test-select').on('change', function(){
                    var id = $(this).val();
                    var clinicId = $('.clinic-select').val();
                    getAppointments(id, clinicId);
                });

                $('.clinic-select').on('change', function(){
                    var id = $(this).val();
                    var testId = $('.test-select').val();
                    getAppointments(testId, id);
                });

                $('.appointment-select').on('change', function(){
                    var id = $(this).val();
                    getAppointmentTimes(id);
                });
            });
        </script>

        <script>
            var rescheduled = false;

            function showReasonRow(){
                $('.reason-row').removeClass('hidden');
            }

            function hideReasonRow(){
                $('.reason-row').addClass('hidden');
                $('.reason-select').val('');
                $('.reason-select').trigger('change.select2');;
                $('.reason-comment textarea').val('');
            }

            $(document).ready(function(){
                $('.appointment-select').on('change', function(){
                    rescheduled = true;
                    showReasonRow();
                });

                $('.time-select').on('change', function(){
                    rescheduled = true;
                    showReasonRow();
                });

                $('.status-select').on('change', function(){
                    var value = $(this).val();
                    if(value == "{{CANCELED_BOOKING_STATUS}}")
                        showReasonRow();
                    else if(rescheduled === false)
                        hideReasonRow();
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
