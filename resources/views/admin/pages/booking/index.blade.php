@extends('admin.index')

@section('content')
@push('css')
    <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- DataTables -->
    <link href="{{asset('dashboard/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}"
          rel="stylesheet" type="text/css" />
    <link href="{{asset('dashboard/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}"
          rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="{{asset('dashboard/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}"
          rel="stylesheet"
          type="text/css" />
    <!-- Plugin Css -->

    <style>
        .loading-modal.bd-example-modal-lg .modal-dialog{
            display: table;
            position: relative;
        }
        .bd-example-modal-lg .modal-dialog{
            margin: 0 auto;
            top: calc(50% - 24px);
        }
        .product-info-modal.bd-example-modal-lg .modal-dialog{
            top: 20px;
            max-width: calc(100% - 40px);
        }
        .bd-example-modal-lg .modal-dialog .modal-content{
            background-color: transparent;
            border: none;
        }
    </style>
@endpush
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <div class="page-title">
                            <h4 class="mb-0 font-size-18">{{__('Booking List')}}</h4>
                            <ol class="breadcrumb">
                                {{ Breadcrumbs::render('booking.list') }}
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- Start Page-content-Wrapper -->
            <div class="page-content-wrapper">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">{{__('Booking List')}}</h4>
                                <p class="card-title-desc">
                                	<a href="{{route('booking.create')}}" class="btn btn-success">{{__('Create Test Booking')}}</a>
                                </p>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="mb-3 form-group">
                                            <label class="form-label" for="clinic_id">
                                                Labs
                                            </label>
                                            <select class="form-control
                                            clinic-select @error('clinic_id') is-invalid @enderror"
                                            id="clinic-select"
                                            name="clinic_id">
                                                <option value=""></option>
                                                @foreach($clinics as $key => $clinic)
                                                    <option value="{{$clinic->id}}">{{$clinic->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('clinic_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <!-- End Col -->

                                    <div class="col-md-3">
                                        <div class="mb-3 form-group">
                                            <label class="form-label" for="country_id">
                                                Countries
                                            </label>
                                            <select class="form-control
                                            country-select @error('country_id') is-invalid @enderror"
                                            id="country-select"
                                            name="country_id">
                                                <option value=""></option>
                                                @foreach($countries as $key => $country)
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('country_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <!-- End Col -->

                                    <div class="col-md-3">
                                        <div class="mb-3 form-group">
                                            <label class="form-label" for="city_id">
                                                Cities
                                            </label>
                                            <select class="form-control
                                            city-select @error('city_id') is-invalid @enderror"
                                            id="city-select"
                                            name="city_id">
                                                <option value=""></option>
                                                
                                            </select>
                                            @error('city_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <!-- End Col -->
                                </div>

                                <table id="datatable"
                                       class="table table-striped table-bordered nowrap"
                                       style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th scope="col" class="desktop"></th>
                                            <th scope="col" class="desktop">{{__('User')}}</th>
                                            <th scope="col" class="desktop">{{__('Date')}}</th>
                                            <th scope="col" class="desktop">{{__('Time')}}</th>
                                            <th scope="col" class="desktop">{{__('Created At')}}</th>
                                            <th scope="col" class="desktop">{{__('Test')}}</th>
                                            <th scope="col" class="desktop">{{__('Lab')}}</th>
                                            <th scope="col" class="desktop">{{__('Country')}}</th>
                                            <th scope="col" class="desktop">{{__('City')}}</th>
                                            <th scope="col" class="desktop">{{__('Price')}}</th>
                                            <th scope="col" class="desktop">{{__('Status')}}</th>
                                            <th scope="col" class="desktop">{{__('Payment Status')}}</th>
                                            <th scope="col" class="desktop">{{__('Reason')}}</th>
                                            <th scope="col" class="desktop">{{__('Result')}}</th>
                                            <th scope="col" class="desktop">{{__('Action')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

            </div>
            <!-- End Page-content -->

        </div>
        <!-- Container-Fluid -->
    </div>
    <!-- End Page-content-wrapper -->

    <div class="modals">
        <div id="details-modal-wrapper"></div>

        <div class="modal fade bd-example-modal-lg loading-modal" tabindex="-1">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" style="width: 48px">
                    <span class="fa fa-spinner fa-spin fa-3x" style="color: white"></span>
                </div>
            </div>
        </div>
    </div>

    <div id="countryCities" data-url="{{route('country.cities')}}"></div>

    <div id="table-url" data-url="{{route('booking.booking_list')}}"></div>
    <div id="table-url" data-url="{{route('booking.delete_booking')}}"></div>
    @push('js')
        <!--  datatable js -->
        <script src="{{asset('dashboard/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Buttons examples -->
        <script src="{{asset('dashboard/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
        <!-- Responsive examples -->
        <script src="{{asset('dashboard/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
        <!-- Datatable init js -->
        <script src="{{asset('dashboard/js/pages/datatables.init.js')}}"></script>

        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>

        <!-- List js -->
        <script src="{{asset('dashboard/admin/js/booking/list.js')}}"></script>
        <!-- Delete js -->
        <script src="{{asset('dashboard/admin/js/booking/delete.js')}}"></script>

        <script>
            $(document).ready(function() {
                $('.clinic-select').select2({placeholder: 'Select Lab', allowClear: true});
                $('.country-select').select2({placeholder: 'Select Country', allowClear: true});
                $('.city-select').select2({placeholder: 'Select City', allowClear: true});
            });
        </script>

        <script>
            $(document).ready(function(){
                $('.clinic-select').on('change', function(){
                    var select = document.getElementById('clinic-select');
                    var text = select.options[select.selectedIndex].text;
                    var oTable = $('#datatable').dataTable();
                    oTable.fnFilter(text);

                    $('.country-select').val(null).select2({placeholder: 'Select Country', allowClear: true});
                    $('.city-select').val(null).select2({placeholder: 'Select City', allowClear: true});
                });

                $('.country-select').on('change', function(){
                    var select = document.getElementById('country-select');
                    var text = select.options[select.selectedIndex].text;
                    var oTable = $('#datatable').dataTable();
                    oTable.fnFilter(text);
                    $('.clinic-select').val(null).select2({placeholder: 'Select Lab', allowClear: true});
                });

                $('.city-select').on('change', function(){
                    var select = document.getElementById('city-select');
                    var text = select.options[select.selectedIndex].text;
                    var oTable = $('#datatable').dataTable();
                    oTable.fnFilter(text);
                    $('.clinic-select').val(null).select2({placeholder: 'Select Lab', allowClear: true});
                });
            });
        </script>

        <script>
            function updateCitiesSelect(cities){
                var options = '';
                for (var i = cities.length - 1; i >= 0; i--) {
                    options += '<option value="' + cities[i].id + '">' + cities[i].name + '</option>';
                }
                $('#city-select').html(options);
                $('#city-select').val(null);
                $('#city-select').select2({placeholder: 'Select City'});
            }

            function updateCities(countryId){
                if(!countryId){
                    $('#city-select').html('');
                    $('#city-select').select2({placeholder: 'Select City'});
                }

                var url = $('#countryCities').data('url');
                $.ajax({
                    url: url,
                    data: {
                        id: countryId
                    },
                    success: function(data){
                        updateCitiesSelect(data.cities);
                    },
                    error: function(_, __, ___){

                    }
                });
            }

            $(document).ready(function(){
                $('#country-select').on('change', function(){
                    var id = $(this).val();
                    updateCities(id);
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

        <script>
            $(document).ready(function(){
                var rescheduled = "{{$rescheduled}}" == true;
                if(rescheduled){
                    var li = $('.sub-menu .mm-active');
                    li.siblings('.test-booking-rescheduled-list').addClass('mm-active');
                    li.removeClass('mm-active');
                    li.find('a').removeClass('active');
                }
            });
        </script>

        <script>
            $(document).ready(function(){
                function showOrderDetails(data){
                    $('#details-modal-wrapper').html(data);
                    $('#details-modal-wrapper #detailsModal').modal('show');
                }

                function showLoadingModal(){
                    $('.loading-modal').modal('show');
                }

                function hideLoadingModal(){
                    $('.loading-modal').modal('hide');
                }

                function getResultDetails(url){
                    showLoadingModal();

                    $.ajax({
                        url: url,
                        success: function(data){
                            hideLoadingModal();
                            showOrderDetails(data.modal);
                        },
                        error: function(_, __, ___){
                            hideLoadingModal();
                        }
                    });
                }

                $(document).on('click', '.result-details', function(e){
                    e.preventDefault();
                    var url = $(this).attr('href');
                    getResultDetails(url);
                });
            });
        </script>

    @endpush
@endsection
