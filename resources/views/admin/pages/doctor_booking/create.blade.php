@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

        </style>
    @endpush
    
        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Create Booking')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('doctor.booking.create') }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Create a new Booking</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('doctor.booking.store')}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="doctor_id">
                                                        Doctor
                                                    </label>
                                                    <select class="form-control
                                                    doctor-select @error('doctor_id') is-invalid @enderror"
                                                    name="doctor_id"
                                                    value="{{ old('doctor_id') }}">
                                                        <option value="">Select doctor</option>
                                                        @foreach($doctors as $key => $doctor)
                                                            <option value="{{$doctor->id}}">{{$doctor->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('doctor_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="doctor_appointment_id">
                                                        Appointment
                                                    </label>
                                                    <select class="form-control
                                                    appointment-select @error('doctor_appointment_id') is-invalid @enderror"
                                                    name="doctor_appointment_id"
                                                    value="{{ old('doctor_appointment_id') }}">
                                                        <option value="">Select Appointment</option>
                                                        
                                                    </select>
                                                    @error('doctor_appointment_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="doctor_appointment_time_id">
                                                        Time
                                                    </label>
                                                    <select class="form-control
                                                    time-select @error('doctor_appointment_time_id') is-invalid @enderror"
                                                    name="doctor_appointment_time_id"
                                                    value="{{ old('doctor_appointment_time_id') }}">
                                                        <option value="">Select Time</option>
                                                        
                                                    </select>
                                                    @error('doctor_appointment_time_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="user_id">
                                                        User
                                                    </label>
                                                    <select class="form-control
                                                    user-select @error('user_id') is-invalid @enderror"
                                                    name="user_id"
                                                    value="{{ old('user_id') }}">
                                                        <option value="">Select User</option>
                                                        @foreach($users as $key => $user)
                                                            <option value="{{$user->id}}">{{$user->full_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('user_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="status">
                                                        Status
                                                    </label>
                                                    <select class="form-control
                                                    status-select @error('status') is-invalid @enderror"
                                                    name="status"
                                                    value="{{ old('status') }}">
                                                        <option value="{{PENDING_BOOKING_STATUS}}">Booked</option>
                                                        <option value="{{COMPLETED_BOOKING_STATUS}}">Completed</option>
                                                        <option value="{{CANCELED_BOOKING_STATUS}}">Canceled</option>
                                                    </select>
                                                    @error('status')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->

        <div id="getAppointmentsUrl" data-url="{{route('doctor.appointments')}}"></div>
        <div id="getAppointmentTimesUrl" data-url="{{route('doctor.appointment.times')}}"></div>

    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
        <!--intlTelInput-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>

        <script>
            $(document).ready(function() {
                $('.doctor-select').select2();
                $('.time-select').select2();
                $('.appointment-select').select2();
                $('.user-select').select2();
                $('.status-select').select2();
            });
        </script>
        <script>
            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            required: true,
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            required: "Please provide a phone number",
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <script>
            function updateAppointmentSelect(appointments){
                var options = '';
                for (var i = appointments.length - 1; i >= 0; i--) {
                    options += '<option value="' + appointments[i].id + '">' + appointments[i].date + '</option>';
                }
                $('.appointment-select').html(options);
                $('.appointment-select').val('');
                $('.appointment-select').select2({placeholder: 'Select Appointment'});
            }

            function updateAppointmentTimeSelect(times){
                var options = '';
                for (var i = times.length - 1; i >= 0; i--) {
                    options += '<option value="' + times[i].id + '">' + times[i].time + '</option>';
                }
                $('.time-select').html(options);
                $('.time-select').select2({placeholder: 'Select Time'});
            }

            function getAppointments(doctorId){
                if(!doctorId){
                    $('.appointment-select').html('');
                    $('.appointment-select').select2({placeholder: 'Select Appointment'});

                    $('.time-select').html('');
                    $('.time-select').select2({placeholder: 'Select Time'});
                    return;
                }

                var url = $('#getAppointmentsUrl').data('url');
                $.ajax({
                    url: url,
                    data:{
                        doctor_id: doctorId,
                    },
                    success: function(data){
                        updateAppointmentSelect(data.doctorAppointments);
                    },
                    error: function(_, __, ___){

                    }
                });
            }

            function getAppointmentTimes(appointmentId){
                if(!appointmentId){
                    $('.time-select').html('');
                    $('.time-select').select2({placeholder: 'Select Time'});
                    return;
                }

                var url = $('#getAppointmentTimesUrl').data('url');
                $.ajax({
                    url: url,
                    data:{
                        appointment_id: appointmentId,
                    },
                    success: function(data){
                        updateAppointmentTimeSelect(data.times);
                    },
                    error: function(_, __, ___){

                    }
                });
            }

            $(document).ready(function(){
                $('.doctor-select').on('change', function(){
                    var id = $(this).val();
                    getAppointments(id);
                });

                $('.appointment-select').on('change', function(){
                    var id = $(this).val();
                    getAppointmentTimes(id);
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
