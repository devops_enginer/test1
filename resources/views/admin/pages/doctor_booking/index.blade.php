@extends('admin.index')

@section('content')
@push('css')
    <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- DataTables -->
    <link href="{{asset('dashboard/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}"
          rel="stylesheet" type="text/css" />
    <link href="{{asset('dashboard/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}"
          rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="{{asset('dashboard/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}"
          rel="stylesheet"
          type="text/css" />
    <!-- Plugin Css -->
@endpush
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <div class="page-title">
                            <h4 class="mb-0 font-size-18">{{__('Booking List')}}</h4>
                            <ol class="breadcrumb">
                                {{ Breadcrumbs::render('doctor.booking.list') }}
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- Start Page-content-Wrapper -->
            <div class="page-content-wrapper">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">{{__('Booking List')}}</h4>
                                <p class="card-title-desc">
                                	<a href="{{route('doctor.booking.create')}}" class="btn btn-success">{{__('Create Doctor Booking')}}</a>
                                </p>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="mb-3 form-group">
                                            <label class="form-label" for="doctor_id">
                                                Doctors
                                            </label>
                                            <select class="form-control
                                            doctor-select @error('doctor_id') is-invalid @enderror"
                                            id="doctor-select"
                                            name="doctor_id">
                                                <option value="">Select Doctor</option>
                                                @foreach($doctors as $key => $doctor)
                                                    <option value="{{$doctor->id}}">{{$doctor->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('doctor_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <!-- End Col -->
                                </div>

                                <table id="datatable"
                                       class="table table-striped table-bordered nowrap"
                                       style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th scope="col" class="desktop"></th>
                                            <th scope="col" class="desktop">{{__('User')}}</th>
                                            <th scope="col" class="desktop">{{__('Date')}}</th>
                                            <th scope="col" class="desktop">{{__('Time')}}</th>
                                            <th scope="col" class="desktop">{{__('Created At')}}</th>
                                            <th scope="col" class="desktop">{{__('Doctor')}}</th>
                                            <th scope="col" class="desktop">{{__('Status')}}</th>
                                            <th scope="col" class="desktop">{{__('Reason')}}</th>
                                            <th scope="col" class="desktop">{{__('Prescription')}}</th>
                                            <th scope="col" class="desktop">{{__('Action')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

            </div>
            <!-- End Page-content -->

        </div>
        <!-- Container-Fluid -->
    </div>
    <!-- End Page-content-wrapper -->
    <div id="table-url" data-url="{{route('doctor.booking.booking_list')}}"></div>
    <div id="table-url" data-url="{{route('doctor.booking.delete_booking')}}"></div>
    @push('js')
        <!--  datatable js -->
        <script src="{{asset('dashboard/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Buttons examples -->
        <script src="{{asset('dashboard/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
        <!-- Responsive examples -->
        <script src="{{asset('dashboard/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
        <!-- Datatable init js -->
        <script src="{{asset('dashboard/js/pages/datatables.init.js')}}"></script>

        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>

        <!-- List js -->
        <script src="{{asset('dashboard/admin/js/doctor_booking/list.js')}}"></script>
        <!-- Delete js -->
        <script src="{{asset('dashboard/admin/js/doctor_booking/delete.js')}}"></script>

        <script>
            $(document).ready(function() {
                $('.doctor-select').select2();
            });
        </script>

        <script>
            $(document).ready(function(){
                $('.doctor-select').on('change', function(){
                    var select = document.getElementById('doctor-select');
                    var text = select.options[select.selectedIndex].text;
                    var oTable = $('#datatable').dataTable();
                    oTable.fnFilter(text);
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

        <script>
            $(document).ready(function(){
                var rescheduled = "{{$rescheduled}}" == true;
                if(rescheduled){
                    var li = $('.sub-menu .mm-active');
                    li.siblings('.doctor-booking-rescheduled-list').addClass('mm-active');
                    li.removeClass('mm-active');
                    li.find('a').removeClass('active');
                }
            });
        </script>

    @endpush
@endsection
