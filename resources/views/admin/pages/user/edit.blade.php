@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

        </style>
    @endpush

        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Edit User')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('user.edit',$user) }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Edit User</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('users.update',$user)}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf
                                        @method('PUT')
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="full_name">
                                                        User Name
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('full_name') is-invalid @enderror"
                                                           id="full_name"
                                                           placeholder="Full Name"
                                                           name="full_name"
                                                           value="{{ $user->full_name }}"
                                                           >
                                                    @error('full_name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="email">
                                                        Email
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('email') is-invalid @enderror"
                                                           id="email"
                                                           placeholder="Email"
                                                           name="email"
                                                           value="{{ $user->email }}"
                                                           >
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="password">
                                                        Password
                                                    </label>
                                                    <input type="password"
                                                           class="form-control @error('password') is-invalid @enderror"
                                                           id="password"
                                                           placeholder="Password"
                                                           name="password"
                                                           >
                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="password_confirmation">
                                                        Confirm Password
                                                    </label>
                                                    <input type="password"
                                                           class="form-control @error('password_confirmation') is-invalid @enderror"
                                                           id="password_confirmation"
                                                           placeholder="Confirm Password"
                                                           name="password_confirmation"
                                                           >
                                                    @error('password_confirmation')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="phone_number">Phone Number</label>
                                                    <input
                                                        class="form-control @error('phone_number') is-invalid @enderror"
                                                        type="tel"
                                                        pattern="[+]{0,1}[0-9]{8,13}" name="phone_number"
                                                        id="phone_number"
                                                        minlength="9" maxlength="13" value="{{$user->phone_number}}"
                                                        autocomplete="phone" autofocus
                                                        placeholder="Enter the phone" >
                                                    @error('phone_number')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="address_line_1">
                                                        Address line 1
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('address_line_1') is-invalid @enderror"
                                                           id="address_line_1"
                                                           placeholder="Full Name"
                                                           name="address_line_1"
                                                           value="{{ $user->address_line_1 }}"
                                                           >
                                                    @error('address_line_1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="address_line_2">
                                                        Address line 2
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('address_line_2') is-invalid @enderror"
                                                           id="address_line_2"
                                                           placeholder="Full Name"
                                                           name="address_line_2"
                                                           value="{{ $user->address_line_2 }}"
                                                           >
                                                    @error('address_line_2')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-4">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="country_id">
                                                        Country
                                                    </label>
                                                    <select id="countries-select" class="form-control
                                                    select2 @error('country_id') is-invalid @enderror"
                                                    name="country_id"
                                                    >
                                                        <option value=""></option>
                                                        @foreach($countries as $country)
                                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('country_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-4">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="city_id">
                                                        City
                                                    </label>
                                                    <select id="cities-select" class="form-control
                                                    select2 @error('city_id') is-invalid @enderror"
                                                    name="city_id"
                                                    >
                                                        <option value=""></option>
                                                        @foreach($cities as $city)
                                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('city_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-4">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="language">
                                                        Language
                                                    </label>
                                                    <select id="language-select" class="form-control
                                                    select2 @error('language') is-invalid @enderror"
                                                    name="language"
                                                    >
                                                        <option value=""></option>
                                                        <option
                                                            @if($user->language == 'en') selected @endif
                                                            value="en">English</option>
                                                        <option
                                                            @if($user->language == 'ar') selected @endif
                                                            value="ar">Arabic</option>
                                                    </select>
                                                    @error('language')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        {{--
                                        <div class="row" id="image_container">
                                            <div class="col-lg-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="image_select">
                                                        Photo (1:1)
                                                    </label>
                                                    <input type="file"
                                                           accept="image/png, image/jpeg, image/png, image/gif"
                                                           class="form-control" id="image_select"
                                                           name="photo">
                                                    <button style="margin-top: 5px" type="button"
                                                            id="remove_image_button"
                                                            class="btn btn-sm btn-link p-0 display-none">
                                                        Remove image
                                                    </button>
                                                </div>
                                            </div>
                                            @if ($user->real_photo)
                                                <img src="{{ $user->photo }}" id="image_preview" class="img-fluid col-md-6"
                                                     style="width: 132px;margin-top: 5px">
                                            @endif
                                        </div>
                                        <!-- End Row -->
                                        --}}

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->

        <div id="countryCities" data-url="{{route('country.cities')}}"></div>

    @push('js')
        <!-- jquery-validation -->
            <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
            <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
            <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
            <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
            <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
            <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

            <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
            <!-- validation init -->
            <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
            <!--intlTelInput-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"></script>
            <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>

            <!-- Upload image -->
            <script src="{{asset('dashboard/admin/js/user/upload_image.js')}}"></script>

            <script>
                var inputt = document.querySelector('#phone_number');
                var countryData = window.intlTelInputGlobals.getCountryData();
                var addressDropdow = document.querySelector("#addresss-country");
                var it2 = window.intlTelInput(inputt, {
                    utilScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js",
                });
                for (var i = 0; i < countryData.length; i++) {
                    var country = countryData[i];
                    var optionNode = document.createElement("option");
                    optionNode.value = country.iso2;
                    var textNode = document.createTextNode(country.name);
                    optionNode.appendChild(textNode);
                    // addressDropdow.appendChild(optionNode);
                }

                // addressDropdow.value = it2.getSelectedCountryData().iso2;

                // listen to the telephone input for changes
                inputt.addEventListener('countrychange', function (e) {
                    // addressDropdow.value = it2.getSelectedCountryData().iso2;
                });

                // listen to the address dropdown for changes
                // addressDropdow.addEventListener('change', function () {
                //     it2.setCountry(this.value);
                // });

                $('#phone_number').val("{{$user->phone_number}}");
            </script>
            
            <script>
                $(document).ready(function() {
                    $('.select2').select2({
                        width: '100%',
                        placeholder: "Select an Option",
                        allowClear: true,
                    });

                    // $('#language-select').select2('val', "{{$user->language}}");

                    $('#countries-select').select2('val', "{{$user->country_id}}");

                    $('#cities-select').select2('val', "{{$user->city_id}}");
                });
            </script>
            <script>
                $(function () {
                    $('#validation-form').validate({
                        rules: {
                            password: {
                                minlength: 6
                            },
                            password_confirmation: {
                                minlength: 6,
                                equalTo: "#password"
                            },
                            phone: {
                                minlength: 13
                            },
                        },
                        messages: {
                            phone: {
                                minlength: "Phone number must be at least 13 characters long"
                            },
                            password: {
                                minlength: "Your password must be at least 5 characters long"
                            },
                            password_confirmation: {
                                minlength: "Your password must be at least 5 characters long",
                                equalTo: "Password and Password Confirmation must be match"
                            }
                        },
                        errorElement: 'span',
                        errorPlacement: function (error, element) {
                            error.addClass('invalid-feedback');
                            element.closest('.form-group').append(error);
                        },
                        highlight: function (element, errorClass, validClass) {
                            $(element).addClass('is-invalid');
                        },
                        unhighlight: function (element, errorClass, validClass) {
                            $(element).removeClass('is-invalid');
                        }
                    });
                });
            </script>

            <script>
                function updateCitiesSelect(cities){
                    var options = '';
                    for (var i = cities.length - 1; i >= 0; i--) {
                        options += '<option value="' + cities[i].id + '">' + cities[i].name + '</option>';
                    }
                    $('#cities-select').html(options);
                    $('#cities-select').select2({placeholder: 'Select City'});
                }

                function updateCities(countryId){
                    if(!countryId){
                        $('#cities-select').html('');
                        $('#cities-select').select2({placeholder: 'Select City'});
                    }

                    var url = $('#countryCities').data('url');
                    $.ajax({
                        url: url,
                        data: {
                            id: countryId
                        },
                        success: function(data){
                            updateCitiesSelect(data.cities);
                        },
                        error: function(_, __, ___){

                        }
                    });
                }

                $(document).ready(function(){
                    $('#countries-select').on('change', function(){
                        var id = $(this).val();
                        updateCities(id);
                    });
                });
            </script>

            <!-- App js -->
            <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
