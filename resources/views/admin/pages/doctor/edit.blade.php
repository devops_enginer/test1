@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

            #map{
                height: 400px;
                width: 100%;
            }

        </style>
    @endpush

        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Edit Doctor')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('doctor.edit', $doctor) }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Edit Doctor</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('doctor.update', $doctor->id)}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf
                                        @method('PUT')

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="name">
                                                        Name
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('name') is-invalid @enderror"
                                                           id="name"
                                                           placeholder="Name"
                                                           name="name"
                                                           value="{{ $doctor->name }}"
                                                           >
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="experience">
                                                        Experience
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('experience') is-invalid @enderror"
                                                           id="experience"
                                                           placeholder="Experience"
                                                           name="experience"
                                                           value="{{ $doctor->experience }}"
                                                           >
                                                    @error('experience')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="rating">
                                                        Rating
                                                    </label>
                                                    <input type="number"
                                                           step="0.01"
                                                           class="form-control @error('rating') is-invalid @enderror"
                                                           id="rating"
                                                           placeholder="Rating"
                                                           name="rating"
                                                           value="{{ $doctor->rating }}"
                                                           >
                                                    @error('rating')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="category_id">
                                                        Category
                                                    </label>
                                                    <select id="categories-select" class="form-control
                                                    category-select @error('category_id') is-invalid @enderror"
                                                    name="category_id"
                                                    value="{{ old('category_id') }}" required>
                                                        @foreach($categories as $key => $category)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('category_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="description">
                                                        Description
                                                    </label>
                                                    <textarea class="form-control @error('description') is-invalid @enderror"
                                                        id="description"
                                                        placeholder="Description"
                                                        name="description"
                                                        required>{{ $doctor->description }}</textarea>
                                                    @error('description')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="country_id">
                                                        Country
                                                    </label>
                                                    <select id="countries-select" class="form-control
                                                    select2 @error('country_id') is-invalid @enderror"
                                                    name="country_id"
                                                    >
                                                        <option value=""></option>
                                                        @foreach($countries as $country)
                                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('country_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-4">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="city_id">
                                                        City
                                                    </label>
                                                    <select id="cities-select" class="form-control
                                                    select2 @error('city_id') is-invalid @enderror"
                                                    name="city_id"
                                                    >
                                                        <option value=""></option>
                                                        @foreach($cities as $city)
                                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('city_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row" id="image_container">
                                            <div class="col-lg-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="image_select">
                                                        Image (1:1)
                                                    </label>
                                                    <input type="file"
                                                           accept="image/png, image/jpeg, image/png, image/gif"
                                                           class="form-control" id="image_select"
                                                           name="image">
                                                    <button style="margin-top: 5px" type="button"
                                                            id="remove_image_button"
                                                            class="btn btn-sm btn-link p-0 display-none">
                                                        Remove image
                                                    </button>
                                                </div>
                                            </div>
                                            @if ($doctor->image)
                                                <img src="{{ $doctor->image }}" id="image_preview" class="img-fluid col-md-6"
                                                     style="width: 132px;margin-top: 5px">
                                            @endif
                                        </div>
                                        <!-- End Row -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="address">
                                                        Search Location
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('address') is-invalid @enderror"
                                                           id="address"
                                                           placeholder="Search Location"
                                                           name="address"
                                                           value="{{ $doctor->address }}"
                                                           >
                                                    @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                                <div id="map"></div>
                                                <div class="mb-3 form-group">
                                                    <input type="hidden" name="lat" id="latitude" value="{{$doctor->lat}}">
                                                    <input type="hidden" name="lng" id="longitude" value="{{$doctor->lng}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->

        <div id="countryCities" data-url="{{route('country.cities')}}"></div>

    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <!--intlTelInput-->
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>

        <!-- Upload image -->
        <script src="{{asset('dashboard/admin/js/doctor/upload_image.js')}}"></script>
        
        <script>
            // var inputt = document.querySelector('#phone_number');
            var countryData = window.intlTelInputGlobals.getCountryData();
            var addressDropdow = document.querySelector("#addresss-country");
            // var it2 = window.intlTelInput(inputt, {
            //     utilScript: "js/utils"
            // });
            for (var i = 0; i < countryData.length; i++) {
                var country = countryData[i];
                var optionNode = document.createElement("option");
                optionNode.value = country.iso2;
                var textNode = document.createTextNode(country.name);
                optionNode.appendChild(textNode);
                // addressDropdow.appendChild(optionNode);
            }

            // addressDropdow.value = it2.getSelectedCountryData().iso2;

            // listen to the telephone input for changes
            // inputt.addEventListener('countrychange', function (e) {
                // addressDropdow.value = it2.getSelectedCountryData().iso2;
            // });

            // listen to the address dropdown for changes
            // addressDropdow.addEventListener('change', function () {
            //     it2.setCountry(this.value);
            // });
        </script>

        <script>
            $(document).ready(function() {
                $('.test-select').select2();
                $('.test-select').select2("val", "{{ $doctor->test_id }}");

                $('#categories-select').select2();
                $('#categories-select').select2('val', "{{$doctor->category_id}}");
                $('#countries-select').select2({placeholder: 'Select Country'});
                $('#cities-select').select2({placeholder: 'Select City'});

                $('#countries-select').select2('val', "{{$doctor->country_id}}");

                $('#cities-select').select2('val', "{{$doctor->city_id}}");
            });
        </script>

        <script>
            function updateCitiesSelect(cities){
                var options = '';
                for (var i = cities.length - 1; i >= 0; i--) {
                    options += '<option value="' + cities[i].id + '">' + cities[i].name + '</option>';
                }
                $('#cities-select').html(options);
                $('#cities-select').select2({placeholder: 'Select City'});
            }

            function updateCities(countryId){
                if(!countryId){
                    $('#cities-select').html('');
                    $('#cities-select').select2({placeholder: 'Select City'});
                }

                var url = $('#countryCities').data('url');
                $.ajax({
                    url: url,
                    data: {
                        id: countryId
                    },
                    success: function(data){
                        updateCitiesSelect(data.cities);
                    },
                    error: function(_, __, ___){

                    }
                });
            }

            $(document).ready(function(){
                $('#countries-select').on('change', function(){
                    var id = $(this).val();
                    updateCities(id);
                });
            });
        </script>

        <script>

            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <script type="text/javascript">
            var map;
            
            function initMap() {
                var latitude = parseFloat($('#latitude').val());
                var longitude = parseFloat($('#longitude').val());
                
                var myLatLng = {lat: latitude, lng: longitude};
                
                map = new google.maps.Map(document.getElementById('map'), {
                  center: myLatLng,
                  zoom: 14                  
                });
                        
                var marker = new google.maps.Marker({
                  position: myLatLng,
                  map: map,
                  draggable: true,
                  title: 'Choose Location',
                  
                  // setting latitude & longitude as title of the marker
                  // title is shown when you hover over the marker
                  title: latitude + ', ' + longitude 
                });

                google.maps.event.addListener(marker, 'dragend', function(evt){
                    document.getElementById('latitude').value = evt.latLng.lat();
                    document.getElementById('longitude').value = evt.latLng.lng();
                });

                var searchBox = new google.maps.places.SearchBox(document.getElementById('address'));
                map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
                google.maps.event.addListener(searchBox, 'places_changed', function() {
                    searchBox.set('map', null);

                    marker.setMap(null);
                    var places = searchBox.getPlaces();

                    var bounds = new google.maps.LatLngBounds();
                    var i, place;
                    for (i = 0; place = places[i]; i++) {
                        (function(place) {
                            var newMarker = new google.maps.Marker({
                                position: place.geometry.location,
                                draggable: true,
                            });

                            google.maps.event.addListener(newMarker, 'dragend', function(evt){
                                document.getElementById('latitude').value = evt.latLng.lat();
                                document.getElementById('longitude').value = evt.latLng.lng();
                            });
                            
                            if(i == 0){
                                document.getElementById('latitude').value = place.geometry.location.lat();
                                document.getElementById('longitude').value = place.geometry.location.lng();
                            }

                            newMarker.bindTo('map', searchBox, 'map');
                            google.maps.event.addListener(newMarker, 'map_changed', function() {
                                if (!this.getMap()) {
                                    this.unbindAll();
                                }
                            });
                            bounds.extend(place.geometry.location);

                        }(place));

                    }

                    map.fitBounds(bounds);
                    searchBox.set('map', map);
                    map.setZoom(Math.min(map.getZoom(), 12));
                });
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBMorfiu77HWYZCJohBz1kluIZY1G1Ak_E&callback=initMap"
        async defer></script>
        
        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
