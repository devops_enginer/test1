@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

            #map{
                height: 400px;
                width: 100%;
            }

        </style>
    @endpush

        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Edit Website Content')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('static.content.edit', $staticContent) }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Edit Website Content</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('static.content.update')}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf
                                        @method('PUT')

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="phone_number">
                                                        Phone
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('phone_number') is-invalid @enderror"
                                                           id="phone_number"
                                                           placeholder="Phone"
                                                           name="phone_number"
                                                           value="{{ $staticContent->phone_number }}"
                                                           >
                                                    @error('phone_number')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="email">
                                                        Email
                                                    </label>
                                                    <input type="email"
                                                           class="form-control @error('email') is-invalid @enderror"
                                                           id="email"
                                                           placeholder="Email"
                                                           name="email"
                                                           value="{{ $staticContent->email }}"
                                                           >
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="facebook_link">
                                                        Facebook link
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('facebook_link') is-invalid @enderror"
                                                           id="facebook_link"
                                                           placeholder="Facebook link"
                                                           name="facebook_link"
                                                           value="{{ $staticContent->facebook_link }}"
                                                           >
                                                    @error('facebook_link')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="about">
                                                        About
                                                    </label>
                                                    <textarea
                                                           class="form-control @error('about') is-invalid @enderror"
                                                           id="about"
                                                           placeholder="About"
                                                           name="about"
                                                           >{{ $staticContent->about }}</textarea>
                                                    @error('about')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="privacy_policy">
                                                        Privacy policy
                                                    </label>
                                                    <textarea
                                                           class="form-control @error('privacy_policy') is-invalid @enderror"
                                                           id="privacy_policy"
                                                           placeholder="Privacy policy"
                                                           name="privacy_policy"
                                                           >{{ $staticContent->privacy_policy }}</textarea>
                                                    @error('privacy_policy')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="terms">
                                                        Terms
                                                    </label>
                                                    <textarea
                                                           class="form-control @error('terms') is-invalid @enderror"
                                                           id="terms"
                                                           placeholder="Terms"
                                                           name="terms"
                                                           >{{ $staticContent->terms }}</textarea>
                                                    @error('terms')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="address">
                                                        Address
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('address') is-invalid @enderror"
                                                           id="address"
                                                           placeholder="Address"
                                                           name="address"
                                                           value="{{ $staticContent->address }}"
                                                           >
                                                    @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                                <div id="map"></div>
                                                <div class="mb-3 form-group">
                                                    <input type="hidden" name="lat" id="latitude" value="{{$staticContent->lat}}">
                                                    <input type="hidden" name="lng" id="longitude" value="{{$staticContent->lng}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->


    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>

        <script src="{{asset('dashboard/libs/summernote/summernote.min.js')}}"></script>

        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <!--intlTelInput-->
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>

        <script>
            $(document).ready(function(){
                $('#about').summernote();
                $('#privacy_policy').summernote();
                $('#terms').summernote();
            });
        </script>
        
        <script>
            var inputt = document.querySelector('#phone_number');
            var countryData = window.intlTelInputGlobals.getCountryData();
            var addressDropdow = document.querySelector("#addresss-country");
            var it2 = window.intlTelInput(inputt, {
                utilScript: "js/utils"
            });
            for (var i = 0; i < countryData.length; i++) {
                var country = countryData[i];
                var optionNode = document.createElement("option");
                optionNode.value = country.iso2;
                var textNode = document.createTextNode(country.name);
                optionNode.appendChild(textNode);
                // addressDropdow.appendChild(optionNode);
            }

            // addressDropdow.value = it2.getSelectedCountryData().iso2;

            // listen to the telephone input for changes
            inputt.addEventListener('countrychange', function (e) {
                addressDropdow.value = it2.getSelectedCountryData().iso2;
            });

            // listen to the address dropdown for changes
            // addressDropdow.addEventListener('change', function () {
            //     it2.setCountry(this.value);
            // });
        </script>

        <script>

            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <script type="text/javascript">
            var map;
            
            function initMap() {
                var latitude = parseFloat($('#latitude').val());
                var longitude = parseFloat($('#longitude').val());
                
                var myLatLng = {lat: latitude, lng: longitude};
                
                map = new google.maps.Map(document.getElementById('map'), {
                  center: myLatLng,
                  zoom: 14                  
                });
                        
                var marker = new google.maps.Marker({
                  position: myLatLng,
                  map: map,
                  draggable: true,
                  title: 'Choose Location',
                  
                  // setting latitude & longitude as title of the marker
                  // title is shown when you hover over the marker
                  title: latitude + ', ' + longitude 
                });

                google.maps.event.addListener(marker, 'dragend', function(evt){
                    document.getElementById('latitude').value = evt.latLng.lat();
                    document.getElementById('longitude').value = evt.latLng.lng();
                });

                var searchBox = new google.maps.places.SearchBox(document.getElementById('address'));
                map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
                google.maps.event.addListener(searchBox, 'places_changed', function() {
                    searchBox.set('map', null);

                    marker.setMap(null);
                    var places = searchBox.getPlaces();

                    var bounds = new google.maps.LatLngBounds();
                    var i, place;
                    for (i = 0; place = places[i]; i++) {
                        (function(place) {
                            var newMarker = new google.maps.Marker({
                                position: place.geometry.location,
                                draggable: true,
                            });

                            google.maps.event.addListener(newMarker, 'dragend', function(evt){
                                document.getElementById('latitude').value = evt.latLng.lat();
                                document.getElementById('longitude').value = evt.latLng.lng();
                            });
                            
                            if(i == 0){
                                document.getElementById('latitude').value = place.geometry.location.lat();
                                document.getElementById('longitude').value = place.geometry.location.lng();
                            }

                            newMarker.bindTo('map', searchBox, 'map');
                            google.maps.event.addListener(newMarker, 'map_changed', function() {
                                if (!this.getMap()) {
                                    this.unbindAll();
                                }
                            });
                            bounds.extend(place.geometry.location);

                        }(place));

                    }

                    map.fitBounds(bounds);
                    searchBox.set('map', map);
                    map.setZoom(Math.min(map.getZoom(), 12));
                });
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBMorfiu77HWYZCJohBz1kluIZY1G1Ak_E&callback=initMap"
        async defer></script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
