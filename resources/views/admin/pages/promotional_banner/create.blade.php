@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

        </style>
    @endpush
    
        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Create Promotional Banner')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('promotional.banner.create') }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Create a new Promotional Banner</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('promotional.banner.store')}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf

                                        <div class="row" id="image_container">
                                            <div class="col-lg-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="image_select">
                                                        Image (16:9)
                                                    </label>
                                                    <input type="file"
                                                           accept="image/png, image/jpeg, image/png, image/gif"
                                                           class="form-control" id="image_select"
                                                           name="image" required>
                                                    <button style="margin-top: 5px" type="button"
                                                            id="remove_image_button"
                                                            class="btn btn-sm btn-link p-0 display-none">
                                                        Remove image
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="test_id">
                                                        Test
                                                    </label>
                                                    <select class="form-control
                                                    test-select @error('test_id') is-invalid @enderror"
                                                    name="test_id"
                                                    value="{{ old('test_id') }}">
                                                        <option value="">Select Test</option>
                                                        @foreach($tests as $key => $test)
                                                            <option value="{{$test->id}}">{{$test->title}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('test_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="order">
                                                        Order
                                                    </label>
                                                    <input type="number"
                                                           class="form-control @error('order') is-invalid @enderror"
                                                           id="order"
                                                           placeholder="Order"
                                                           name="order"
                                                           value="{{ old('order') }}"
                                                           required>
                                                    @error('order')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->

    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
        <!--intlTelInput-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>

        <!-- Upload image -->
        <script src="{{asset('dashboard/admin/js/promotional_banner/upload_image.js')}}"></script>

        <script>
            $(document).ready(function() {
                $('.test-select').select2();
            });
        </script>
        <script>
            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            required: true,
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            required: "Please provide a phone number",
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
