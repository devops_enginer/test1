@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

            button.add-time, button.remove-time, button.generate-times{
                margin-top: 25px;
                margin-left: 10px;
            }

        </style>
    @endpush
    
        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Create Time Slot')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('appointment.create') }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Create new Time Slot</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('appointment.store')}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="test_id">
                                                        Test
                                                    </label>
                                                    <select class="form-control
                                                    test-select @error('test_id') is-invalid @enderror"
                                                    name="test_id"
                                                    value="{{ old('test_id') }}">
                                                        <option value="">Select Test</option>
                                                        @foreach($tests as $key => $test)
                                                            <option value="{{$test->id}}">{{$test->title}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('test_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="clinic_id">
                                                        Lab
                                                    </label>
                                                    <select class="form-control
                                                    clinic-select @error('clinic_id') is-invalid @enderror"
                                                    name="clinic_id"
                                                    value="{{ old('clinic_id') }}">
                                                        <option value="">Select Lab</option>
                                                        @foreach($clinics as $key => $clinic)
                                                            <option value="{{$clinic->id}}">{{$clinic->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('clinic_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="date">
                                                        Date
                                                    </label>
                                                    <input type="date"
                                                           class="form-control @error('date') is-invalid @enderror"
                                                           id="date"
                                                           placeholder="Date"
                                                           name="date"
                                                           value="{{ old('date') }}"
                                                           required>
                                                    @error('date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <h4>Time Slots</h4>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="from_time">
                                                        From
                                                    </label>
                                                    <input type="time"
                                                           class="form-control @error('from_time') is-invalid @enderror"
                                                           id="from_time"
                                                           placeholder="From"
                                                           name="from_time"
                                                           value="{{ old('from_time') }}"
                                                           >
                                                    @error('from_time')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-5">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="to_time">
                                                        To
                                                    </label>
                                                    <input type="time"
                                                           class="form-control @error('to_time') is-invalid @enderror"
                                                           id="to_time"
                                                           placeholder="To"
                                                           name="to_time"
                                                           value="{{ old('to_time') }}"
                                                           >
                                                    @error('to_time')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-2 generate-time-button">
                                                <button type="button" class="btn btn-primary generate-times">Generate</button>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row times">
                                            <div class="col-md-5 first-time">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="times">
                                                        Time
                                                    </label>
                                                    <input type="time"
                                                           class="form-control @error('times') is-invalid @enderror"
                                                           id="times"
                                                           placeholder="Time"
                                                           name="times[]"
                                                           value="{{ old('times') }}"
                                                           required>
                                                    @error('times')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-2 times-buttons">
                                                <button type="button" class="btn btn-primary add-time"><i class="fa fa-plus"></i></button>

                                                <button type="button" class="btn btn-danger remove-time hidden"><i class="fa fa-minus"></i></button>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->

    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
        <!--intlTelInput-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>

        <script>
            $(document).ready(function() {
                $('.test-select').select2();
                $('.clinic-select').select2();
            });
        </script>
        <script>
            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            required: true,
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            required: "Please provide a phone number",
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <script>
            $(document).ready(function(){
                $(document).on('click', '.add-time', function(){
                    var el = $('.first-time').clone();
                    el.find('input').removeAttr('value');
                    $('.times-buttons').before('<div class="col-md-5 new-time">' + el.html() + '</div>');

                    $('.remove-time').removeClass('hidden');
                });

                $(document).on('click', '.remove-time', function(){
                    $('.new-time:last').remove();
                    if($('.new-time').length == 0) $(this).addClass('hidden');
                });
            });
        </script>

        <script>
            const monthsNum = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

            function addTimes(times){
                for (var i = 0; i < times.length; i++) {
                    if(!$('.first-time input[type="time"]').val() || ($('.first-time').hasClass('from-generated') && i == 0)){
                        $('.first-time input[type="time"]').val(times[i]);
                        $('.first-time').addClass('from-generated');
                        continue;
                    }

                    $('.add-time').trigger('click');
                    $('.new-time').last().find('input[type="time"]').val(times[i]);
                    $('.new-time').last().addClass('generated-time');
                }
            }

            function removeGeneratedTimes(){
                $('.generated-time').remove();
            }

            $(document).ready(function(){
                $('.generate-times').on('click', function(){
                    var fromTime = $('input[name="from_time"]').val();
                    var toTime = $('input[name="to_time"]').val();

                    if(!fromTime || !toTime) return;

                    var date = new Date();
                    date = date.getFullYear() + '-' + monthsNum[date.getMonth()] + '-' + date.getDate();

                    fromTime = new Date(date + ' ' + fromTime);
                    toTime = new Date(date + ' ' + toTime);

                    if(toTime < fromTime){
                        alert('To Time Must be after From Time');
                        return;
                    }

                    var times = [];

                    do{
                        times.push(fromTime.toLocaleTimeString('it-IT'));
                        fromTime = fromTime.setMinutes(fromTime.getMinutes() + 30);
                        fromTime = new Date(fromTime);
                    }while(fromTime < toTime)

                    times.push(toTime.toLocaleTimeString('it-IT'));

                    removeGeneratedTimes();
                    addTimes(times);
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
