@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

        </style>
    @endpush
    
        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Create Test')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('test.create') }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Create a new Test</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('test.store')}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="title">
                                                        Title
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('title') is-invalid @enderror"
                                                           id="title"
                                                           placeholder="Title"
                                                           name="title"
                                                           value="{{ old('title') }}"
                                                           required>
                                                    @error('title')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="description">
                                                        Description
                                                    </label>
                                                    <textarea 
                                                           class="form-control @error('description') is-invalid @enderror"
                                                           id="description"
                                                           placeholder="Description"
                                                           name="description"
                                                           required>{{ old('description') }}</textarea>
                                                    @error('description')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="col-md-6">
                                            <div class="mb-3 form-group">
                                                <input class="form-check-input" type="checkbox" id="is_featured" name="is_featured">
                                                <label class="form-check-label ms-1" for="is_featured">
                                                    Is Featured
                                                </label>
                                                @error('is_featured')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <!-- End Col -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="service_id">
                                                        Category
                                                    </label>
                                                    <select class="form-control
                                                    service-select @error('service_id') is-invalid @enderror"
                                                    name="service_id"
                                                    value="{{ old('service_id') }}">
                                                        <option value="">Select Category</option>
                                                        @foreach($services as $key => $service)
                                                            <option value="{{$service->id}}">{{$service->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('service_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="clinics[]">
                                                        Labs
                                                    </label>
                                                    <select id="clinic-select" class="form-control
                                                    clinic-select @error('clinics[]') is-invalid @enderror"
                                                    name="clinics[]"
                                                    multiple 
                                                    value="{{ old('clinics[]') }}">
                                                        @foreach($clinics as $key => $clinic)
                                                            <option value="{{$clinic->id}}">{{$clinic->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('clinics[]')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <div class="prices-row">
                                            
                                        </div>

                                        <div class="row" id="image_container">
                                            <div class="col-lg-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="image_select">
                                                        Image (1:1)
                                                    </label>
                                                    <input type="file"
                                                           accept="image/png, image/jpeg, image/png, image/gif"
                                                           class="form-control" id="image_select"
                                                           name="image" required>
                                                    <button style="margin-top: 5px" type="button"
                                                            id="remove_image_button"
                                                            class="btn btn-sm btn-link p-0 display-none">
                                                        Remove image
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->

                                        <div class="row" id="thumbnail_container">
                                            <div class="col-lg-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="thumbnail_select">
                                                        Thumbnail (1:1)
                                                    </label>
                                                    <input type="file"
                                                           accept="image/png, image/jpeg, image/png, image/gif"
                                                           class="form-control" id="thumbnail_select"
                                                           name="thumbnail" required>
                                                    <button style="margin-top: 5px" type="button"
                                                            id="remove_thumbnail_button"
                                                            class="btn btn-sm btn-link p-0 display-none">
                                                        Remove image
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->

        <div id="price-row-template" class="hidden">
            <div class="row">
                <div class="col-md-4">
                    <input type="hidden" name="clinic_id">
                    <div class="mb-3 form-group clinic">
                        <label class="form-label" for="clinic">
                            Lab
                        </label>
                        
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="mb-3 form-group">
                        <label class="form-label" for="sale_price">
                            Sale Price
                        </label>
                        <input type="number"
                               step="0.01" 
                               class="form-control"
                               id="sale_price"
                               placeholder="Sale Price"
                               name="sale_price"
                               >
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="mb-3 form-group">
                        <label class="form-label" for="test_price">
                            Test Price
                        </label>
                        <input type="number"
                               step="0.01"
                               class="form-control"
                               id="test_price"
                               placeholder="Test Price"
                               name="test_price"
                               value=""
                               >
                    </div>
                </div>
            </div>
        </div>

    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>

        <script src="{{asset('dashboard/libs/summernote/summernote.min.js')}}"></script>

        <!--intlTelInput-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>

        <!-- Upload image -->
        <script src="{{asset('dashboard/admin/js/test/upload_image.js')}}"></script>

        <script>
            $(document).ready(function() {
                $('.service-select').select2();
                $('.clinic-select').select2();

                $('#description').summernote();
            });
        </script>
        <script>
            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            required: true,
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            required: "Please provide a phone number",
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <script>
            function removePricesRows(values){
                $('.prices-row .row').each(function(i, obj){
                    var clinicId = $(obj).data('key');
                    for (var i = values.length - 1; i >= 0; i--) {
                        if(values[i] == clinicId) return 0;
                    }
                    $(obj).remove();
                });
            }

            function getNewValue(lastValues, values){
                let intersection = values.filter(x => !lastValues.includes(x));

                return intersection[0];
            }

            function addNewClinicPrices(newClinicId, text){
                var el = $('#price-row-template').clone();
                var clinicNameInput = '<input type="text" class="form-control clinic" placeholder="Lab" name="clinic" value="' + text + '" readonly>';

                el.find('.clinic').append(clinicNameInput);
                el.find('.row').attr('data-key', newClinicId);
                var clinicInput = el.find('input[name="clinic_id"]');
                clinicInput.val(newClinicId);
                clinicInput.attr('name', 'prices[' + newClinicId + '][clinic_id]');

                var salePriceInput = el.find('input[name="sale_price"]');
                salePriceInput.attr('name', 'prices[' + newClinicId + '][sale_price]');
                var testPriceInput = el.find('input[name="test_price"]');
                testPriceInput.attr('name', 'prices[' + newClinicId + '][test_price]');
                $('.prices-row').append(el.html());
            }

            function getSelectedText(newClinicId) {
                var text;
                $('#clinic-select option').each(function(i, obj){
                    if($(obj).is(':selected') && $(obj).val() == newClinicId){
                        text = $(obj).text();
                    }
                });
                return text;
            }

            $(document).ready(function(){
                var lastValues = [];
                $('.clinic-select').on('select2:select', function(){
                    var values = $(this).val();
                    var newClinicId = getNewValue(lastValues, values);
                    var text = getSelectedText(newClinicId);
                    addNewClinicPrices(newClinicId, text);
                    lastValues = values;
                });

                $('.clinic-select').on('select2:unselect', function(){
                    var values = $(this).val();
                    removePricesRows(values);
                    lastValues = values;
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
