@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

        </style>
    @endpush

        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Edit Test')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('test.edit', $test) }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Edit Test</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('test.update', $test->id)}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf
                                        @method('PUT')
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="title">
                                                        Title
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('title') is-invalid @enderror"
                                                           id="title"
                                                           placeholder="Title"
                                                           name="title"
                                                           value="{{ $test->title }}"
                                                           >
                                                    @error('title')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="description">
                                                        Description
                                                    </label>
                                                    <textarea 
                                                           class="form-control @error('description') is-invalid @enderror"
                                                           id="description"
                                                           placeholder="Description"
                                                           name="description"
                                                           >{{ $test->description }}</textarea>
                                                    @error('description')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="col-md-6">
                                            <div class="mb-3 form-group">
                                                <input
                                                    @if($test->is_featured)
                                                    checked
                                                    @endif
                                                    class="form-check-input"
                                                    type="checkbox"
                                                    id="is_featured"
                                                    name="is_featured">
                                                <label class="form-check-label ms-1" for="is_featured">
                                                    Is Featured
                                                </label>
                                                @error('is_featured')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <!-- End Col -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="service_id">
                                                        Category
                                                    </label>
                                                    <select class="form-control
                                                    service-select @error('service_id') is-invalid @enderror"
                                                    name="service_id">
                                                        <option value="">Select Category</option>
                                                        @foreach($services as $key => $service)
                                                            @if($service->id == $test->service_id)
                                                            <option selected value="{{$service->id}}">{{$service->name}}</option>
                                                            @else
                                                            <option value="{{$service->id}}">{{$service->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @error('service_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="clinics[]">
                                                        Labs
                                                    </label>
                                                    <select class="form-control
                                                    clinic-select @error('clinics[]') is-invalid @enderror"
                                                    name="clinics[]"
                                                    multiple 
                                                    value="{{ old('clinics[]') }}">
                                                        @foreach($clinics as $key => $clinic)
                                                            @if(in_array($clinic->id, $test->clinics->pluck('id')->toArray()))
                                                            <option selected value="{{$clinic->id}}">{{$clinic->name}}</option>
                                                            @else
                                                            <option value="{{$clinic->id}}">{{$clinic->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @error('clinics[]')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <div class="prices-row">
                                            @foreach($test->clinics as $key => $clinic)
                                            <div class="row" data-key="{{$clinic->id}}">
                                                <div class="col-md-4">
                                                    <input type="hidden" name="prices[{{$clinic->id}}][clinic_id]" value="{{$clinic->id}}">
                                                    <div class="mb-3 form-group clinic">
                                                        <label class="form-label" for="clinic">
                                                            Lab
                                                        </label>
                                                        <input type="text"
                                                            class="form-control clinic" placeholder="Lab"
                                                            name="clinic"
                                                            value="{{$clinic->name}}"
                                                            readonly>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="mb-3 form-group">
                                                        <label class="form-label" for="sale_price">
                                                            Sale Price
                                                        </label>
                                                        <input type="number"
                                                               step="0.01"
                                                               class="form-control"
                                                               id="sale_price"
                                                               placeholder="Sale Price"
                                                               name="prices[{{$clinic->id}}][sale_price]"
                                                               value="{{$clinic->pivot->sale_price}}"
                                                               required 
                                                               >
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="mb-3 form-group">
                                                        <label class="form-label" for="test_price">
                                                            Test Price
                                                        </label>
                                                        <input type="number"
                                                               step="0.01"
                                                               class="form-control"
                                                               id="test_price"
                                                               placeholder="Test Price"
                                                               name="prices[{{$clinic->id}}][test_price]"
                                                               value="{{$clinic->pivot->test_price}}"
                                                               required
                                                               >
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>

                                        <div class="row" id="image_container">
                                            <div class="col-lg-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="image_select">
                                                        Image (1:1)
                                                    </label>
                                                    <input type="file"
                                                           accept="image/png, image/jpeg, image/png, image/gif"
                                                           class="form-control" id="image_select"
                                                           name="image" >
                                                    <button style="margin-top: 5px" type="button"
                                                            id="remove_image_button"
                                                            class="btn btn-sm btn-link p-0 display-none">
                                                        Remove image
                                                    </button>
                                                </div>
                                            </div>
                                            @if ($test->image)
                                                <img src="{{ $test->image }}" id="image_preview" class="img-fluid col-md-6" style="width: 132px;margin-top: 5px">
                                            @endif
                                        </div>
                                        <!-- End Row -->

                                        <div class="row" id="thumbnail_container">
                                            <div class="col-lg-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="thumbnail_select">
                                                        Thumbnail (1:1)
                                                    </label>
                                                    <input type="file"
                                                           accept="image/png, image/jpeg, image/png, image/gif"
                                                           class="form-control" id="thumbnail_select"
                                                           name="thumbnail" >
                                                    <button style="margin-top: 5px" type="button"
                                                            id="remove_thumbnail_button"
                                                            class="btn btn-sm btn-link p-0 display-none">
                                                        Remove image
                                                    </button>
                                                </div>
                                            </div>
                                            @if ($test->thumbnail)
                                                <img src="{{ $test->thumbnail }}" id="thumbnail_preview" class="img-fluid col-md-6" style="width: 132px;margin-top: 5px">
                                            @endif
                                        </div>
                                        <!-- End Row -->

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->

        <div id="price-row-template" class="hidden">
            <div class="row">
                <div class="col-md-4">
                    <input type="hidden" name="clinic_id">
                    <div class="mb-3 form-group clinic">
                        <label class="form-label" for="clinic">
                            Lab
                        </label>
                        
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="mb-3 form-group">
                        <label class="form-label" for="sale_price">
                            Sale Price
                        </label>
                        <input type="number"
                               step="0.01"
                               class="form-control"
                               id="sale_price"
                               placeholder="Sale Price"
                               name="sale_price"
                               required
                               >
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="mb-3 form-group">
                        <label class="form-label" for="test_price">
                            Test Price
                        </label>
                        <input type="number"
                               step="0.01"
                               class="form-control"
                               id="test_price"
                               placeholder="Test Price"
                               name="test_price"
                               required
                               >
                    </div>
                </div>
            </div>
        </div>

    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

        <script src="{{asset('dashboard/libs/summernote/summernote.min.js')}}"></script>

        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <!--intlTelInput-->
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>

        <!-- Upload image -->
        <script src="{{asset('dashboard/admin/js/test/upload_image.js')}}"></script>
        
        <script>
            // var inputt = document.querySelector('#phone_number');
            var countryData = window.intlTelInputGlobals.getCountryData();
            var addressDropdow = document.querySelector("#addresss-country");
            // var it2 = window.intlTelInput(inputt, {
            //     utilScript: "js/utils"
            // });
            for (var i = 0; i < countryData.length; i++) {
                var country = countryData[i];
                var optionNode = document.createElement("option");
                optionNode.value = country.iso2;
                var textNode = document.createTextNode(country.name);
                optionNode.appendChild(textNode);
                // addressDropdow.appendChild(optionNode);
            }

            // addressDropdow.value = it2.getSelectedCountryData().iso2;

            // listen to the telephone input for changes
            // inputt.addEventListener('countrychange', function (e) {
                // addressDropdow.value = it2.getSelectedCountryData().iso2;
            // });

            // listen to the address dropdown for changes
            // addressDropdow.addEventListener('change', function () {
            //     it2.setCountry(this.value);
            // });
        </script>

        <script>
            $(document).ready(function() {
                $('.service-select').select2({"val": "{{ $test->service_id }}"});
                $('.clinic-select').select2();

                $('#description').summernote();
            });
        </script>
        <script>

            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <script>
            function removePricesRows(values){
                $('.prices-row .row').each(function(i, obj){
                    var clinicId = $(obj).data('key');
                    for (var i = values.length - 1; i >= 0; i--) {
                        if(values[i] == clinicId) return 0;
                    }
                    $(obj).remove();
                });
            }

            function getNewValue(lastValues, values){
                let intersection = values.filter(x => !lastValues.includes(x));

                return intersection[0];
            }

            function addNewClinicPrices(newClinicId, text){
                var el = $('#price-row-template').clone();
                var clinicNameInput = '<input type="text" class="form-control clinic" placeholder="Lab" name="clinic" value="' + text + '" readonly>';

                el.find('.clinic').append(clinicNameInput);
                el.find('.row').attr('data-key', newClinicId);
                var clinicInput = el.find('input[name="clinic_id"]');
                clinicInput.val(newClinicId);
                clinicInput.attr('name', 'prices[' + newClinicId + '][clinic_id]');

                var salePriceInput = el.find('input[name="sale_price"]');
                salePriceInput.attr('name', 'prices[' + newClinicId + '][sale_price]');
                var testPriceInput = el.find('input[name="test_price"]');
                testPriceInput.attr('name', 'prices[' + newClinicId + '][test_price]');
                $('.prices-row').append(el.html());
            }

            function getSelectedText(newClinicId) {
                var text;
                $('#clinic-select option').each(function(i, obj){
                    if($(obj).is(':selected') && $(obj).val() == newClinicId){
                        text = $(obj).text();
                    }
                });
                return text;
            }

            $(document).ready(function(){
                var lastValues = $('.clinic-select').val();
                $('.clinic-select').on('select2:select', function(){
                    var values = $(this).val();
                    var newClinicId = getNewValue(lastValues, values);
                    var text = getSelectedText(newClinicId);
                    addNewClinicPrices(newClinicId, text);
                    lastValues = values;
                });

                $('.clinic-select').on('select2:unselect', function(){
                    var values = $(this).val();
                    removePricesRows(values);
                    lastValues = values;
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
