@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

            button.add-time, button.remove-time{
                margin-top: 25px;
                margin-left: 10px;
            }

        </style>
    @endpush
    
        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Create Time Slot')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('doctor.appointment.create') }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Create new Time Slot</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('doctor.appointment.store')}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="date">
                                                        Date
                                                    </label>
                                                    <input type="date"
                                                           class="form-control @error('date') is-invalid @enderror"
                                                           id="date"
                                                           placeholder="Date"
                                                           name="date"
                                                           value="{{ old('date') }}"
                                                           required>
                                                    @error('date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <h4>Time Slots</h4>
                                        <div class="row times">
                                            <div class="col-md-10 first-time">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="mb-3 form-group">
                                                            <label class="form-label" for="times[]">
                                                                Time
                                                            </label>
                                                            <input type="time"
                                                                   class="form-control @error('times') is-invalid @enderror"
                                                                   id="times[0][time]"
                                                                   placeholder="Time"
                                                                   name="times[0][time]"
                                                                   value="{{ old('times[0][time]') }}"
                                                                   required>
                                                            @error('times[0][time]')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="mb-3 form-group">
                                                            <label class="form-label" for="times[]">
                                                                Price
                                                            </label>
                                                            <input type="number"
                                                                   step="0.01"
                                                                   min="1.00"
                                                                   class="form-control @error('times') is-invalid @enderror"
                                                                   id="times[0][price]"
                                                                   placeholder="Price"
                                                                   name="times[0][price]"
                                                                   value="{{ old('times[0][price]') }}"
                                                                   required>
                                                            @error('times[0][price]')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-2 times-buttons">
                                                <button type="button" class="btn btn-primary add-time"><i class="fa fa-plus"></i></button>

                                                <button type="button" class="btn btn-danger remove-time hidden"><i class="fa fa-minus"></i></button>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="doctor_id">
                                                        Doctor
                                                    </label>
                                                    <select class="form-control
                                                    doctor-select @error('doctor_id') is-invalid @enderror"
                                                    name="doctor_id"
                                                    value="{{ old('doctor_id') }}">
                                                        <option value="">Select Doctor</option>
                                                        @foreach($doctors as $key => $doctor)
                                                            <option value="{{$doctor->id}}">{{$doctor->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('doctor_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->

    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
        <!--intlTelInput-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>

        <script>
            $(document).ready(function() {
                $('.doctor-select').select2();
            });
        </script>
        <script>
            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            required: true,
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            required: "Please provide a phone number",
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <script>
            $(document).ready(function(){
                var lastKey = 0;
                $(document).on('click', '.add-time', function(){
                    lastKey++;
                    var el = $('.first-time').clone();
                    el.find('input').removeAttr('value');
                    el.find('input[type="time"]').attr('name', 'times[' + lastKey + '][time]');
                    el.find('input[type="time"]').attr('id', 'times[' + lastKey + '][time]');
                    el.find('input[type="number"]').attr('name', 'times[' + lastKey + '][price]');
                    el.find('input[type="number"]').attr('id', 'times[' + lastKey + '][price]');
                    
                    $('.times-buttons').before('<div class="col-md-10 new-time">' + el.html() + '</div>');

                    $('.remove-time').removeClass('hidden');
                });

                $(document).on('click', '.remove-time', function(){
                    if($('.new-time').length > 0){
                        $('.new-time:last').remove();
                        lastKey--;
                    }

                    if($('.new-time').length == 0){
                        $(this).addClass('hidden');
                    }
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
