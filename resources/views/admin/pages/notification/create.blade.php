@extends('admin.index')

@section('content')

    @push('css')

        <link href="{{asset('dashboard/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('dashboard/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet"/>
        <!-- intlTelInput -->
        <link href="{{asset('dashboard/css/intlTelInput.css')}}" id="app-style" rel="stylesheet" type="text/css">
        <style>
            .iti--allow-dropdown input, .iti {
                width: 100% !important;
            }

            .iti--allow-dropdown input {
                border: 1px solid #ced4da;
                padding-top: 5px;
                padding-bottom: 5px;
                border-radius: 5px;
            }

            .checkbox-input{
                padding-top: 32px;
            }

        </style>
    @endpush
    
        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <div class="page-title">
                                <h4 class="mb-0 font-size-18">{{__('Send Notification')}}</h4>
                                <ol class="breadcrumb">
                                    {{ Breadcrumbs::render('notification.create') }}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <!-- Start Page-content-Wrapper -->
                <div class="page-content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Send a new Notification</h4>
                                    @include('admin.layouts.message')
                                    <form class="needs-validation"
                                          id="validation-form"
                                          action="{{route('notification.store')}}"
                                          method="post"
                                          enctype="multipart/form-data"
                                          novalidate>
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="title">
                                                        Title
                                                    </label>
                                                    <input type="text"
                                                           class="form-control @error('title') is-invalid @enderror"
                                                           id="title"
                                                           placeholder="Title"
                                                           name="title"
                                                           value="{{ old('title') }}"
                                                           required>
                                                    @error('title')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="body">
                                                        Body
                                                    </label>
                                                    <textarea 
                                                           class="form-control @error('body') is-invalid @enderror"
                                                           id="body"
                                                           placeholder="Body"
                                                           name="body"
                                                           required>{{ old('body') }}</textarea>
                                                    @error('body')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="country_id">
                                                        Country
                                                    </label>
                                                    <select id="countries-select" class="form-control
                                                    select2 @error('country_id') is-invalid @enderror"
                                                    name="country_id"
                                                    required>
                                                        <option value=""></option>
                                                        @foreach($countries as $country)
                                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('country_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                            <div class="col-md-4">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="city_id">
                                                        City
                                                    </label>
                                                    <select id="cities-select" class="form-control
                                                    select2 @error('city_id') is-invalid @enderror"
                                                    name="city_id"
                                                    required>
                                                        <option value=""></option>
                                                        
                                                    </select>
                                                    @error('city_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row user-select-row">
                                                    <div class="col-md-9">
                                                        <div class="mb-3 form-group">
                                                            <label class="form-label" for="user_id">
                                                                Users
                                                            </label>
                                                            <select class="form-control
                                                            user-select @error('user_id') is-invalid @enderror"
                                                            name="user_id[]"
                                                            multiple>
                                                                <option value="">Select User</option>
                                                                @foreach($users as $key => $user)
                                                                    <option value="{{$user->id}}">{{$user->full_name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('user_id[]')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <!-- End Col -->

                                                    <div class="col-md-3 checkbox-input">
                                                        <div class="mb-3 form-group">
                                                            <input class="form-check-input" type="checkbox" id="all_users" name="all_users">
                                                            <label class="form-check-label ms-1" for="all_users">
                                                                All
                                                            </label>
                                                            @error('all_users')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <!-- End Col -->
                                                </div>
                                            </div>
                                            <!-- End Col -->

                                            <div class="col-md-6">
                                                <div class="mb-3 form-group">
                                                    <label class="form-label" for="test_id">
                                                        Test (optional)
                                                    </label>
                                                    <select class="form-control
                                                    test-select @error('test_id') is-invalid @enderror"
                                                    name="test_id"
                                                    value="{{ old('test_id') }}">
                                                        <option value="">Select Test</option>
                                                        @foreach($tests as $key => $test)
                                                            <option value="{{$test->id}}">{{$test->title}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('test_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- End Row -->

                                        <div id="users-ids"></div>

                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                        <!-- End Col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content-Wrapper -->

            </div>
            <!-- Container-fluid -->
        </div>
        <!-- End Page-content -->

        <div id="countryCities" data-url="{{route('country.cities')}}"></div>
        
        <div id="countryUsers" data-url="{{route('country.users')}}"></div>
        <div id="cityUsers" data-url="{{route('city.users')}}"></div>
        <div id="allUsers" data-url="{{route('users.index')}}"></div>
        

    @push('js')
        <!-- jquery-validation -->
        <script src="{{asset('dashboard/libs/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
        <script src="{{asset('dashboard/libs/parsleyjs/parsley.min.js')}}"></script>
        <!--intlTelInput-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"></script>
        <!-- validation init -->
        <script src="{{asset('dashboard/js/pages/form-validation.init.js')}}"></script>
        <script src="{{asset('dashboard/js/pages/intlTelInput.js')}}"></script>

        <script>
            $(document).ready(function() {
                $('.test-select').select2();
                $('.user-select').select2({placeholder: 'Select Users'});
                $('#countries-select').select2({placeholder: 'Select Country', allowClear: true});
                $('#cities-select').select2({placeholder: 'Select City', allowClear: true});
            });
        </script>
        <script>
            $(function () {
                $('#validation-form').validate({
                    rules: {
                        phone_number: {
                            required: true,
                            minlength: 13
                        },
                    },
                    messages: {
                        phone_number: {
                            required: "Please provide a phone number",
                            minlength: "Phone number must be at least 13 characters long"
                        },
                    },
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
            });
        </script>

        <script>
            $(document).ready(function(){
                $('.checkbox-input').on('input', 'input[type="checkbox"]', function(){
                    if($(this).is(':checked')){
                        $('.user-select').select2("enable", false);
                        var usersIds = $('.user-select option');
                        for (var i = usersIds.length - 1; i >= 0; i--) {
                            if(!$(usersIds[i]).attr('value')) continue;
                            $('#users-ids').append('<input type="hidden" name="user_id[]" value="' + $(usersIds[i]).attr('value')  + '" />');
                        }
                    }
                    else{
                        $('.user-select').select2({placeholder: 'Select Users'}).prop('disabled', false);
                        $('#users-ids').html('');
                        $('.user-select').val('');
                    }
                });
            });
        </script>

        <script>
            function updateCitiesSelect(cities){
                var options = '';
                for (var i = cities.length - 1; i >= 0; i--) {
                    options += '<option value="' + cities[i].id + '">' + cities[i].name + '</option>';
                }
                $('#cities-select').html(options);
                $('#cities-select').val(null);
                $('#cities-select').select2({placeholder: 'Select City', allowClear: true});
            }

            function updateCities(countryId){
                if(!countryId){
                    $('#cities-select').html('');
                    $('#cities-select').select2({placeholder: 'Select City'});
                }

                var url = $('#countryCities').data('url');
                $.ajax({
                    url: url,
                    data: {
                        id: countryId
                    },
                    success: function(data){
                        updateCitiesSelect(data.cities);
                    },
                    error: function(_, __, ___){

                    }
                });
            }

            function updateUsersSelect(users){
                var options = '';
                for (var i = users.length - 1; i >= 0; i--) {
                    options += '<option value="' + users[i].id + '">' + users[i].full_name + '</option>';
                }
                $('.user-select').html(options);
                $('.user-select').select2({placeholder: 'Select Users'});
            }

            function updateUsers(id, url){
                $.ajax({
                    url: url,
                    data: {
                        id: id
                    },
                    success: function(data){
                        updateUsersSelect(data.users);
                    },
                    error: function(_, __, ___){

                    }
                });
            }

            $(document).ready(function(){
                $('#countries-select').on('change', function(){
                    var id = $(this).val();
                    updateCities(id);
                    var url = id ? $('#countryUsers').data('url') : $('#allUsers').data('url');
                    updateUsers(id, url);
                });

                $('#cities-select').on('change', function(){
                    var id = $(this).val();
                    var url = id ? $('#cityUsers').data('url') : $('#allUsers').data('url');
                    updateUsers(id, url);
                });
            });
        </script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/app.js')}}"></script>

    @endpush

@endsection
