@extends('admin.index')

@section('content')

  <div class="page-content">
      <div class="container-fluid">
          <!-- start page title -->
          <div class="row">
              <div class="col-12">
                  <div class="page-title-box d-flex align-items-center justify-content-between">
                      <div class="page-title">
                          <h4 class="mb-0 font-size-18">Dashboard</h4>
                          <ol class="breadcrumb">
                              <li class="breadcrumb-item active">Welcome to Health LAb Dashboard</li>
                          </ol>
                      </div>
                  </div>
              </div>
          </div>
          <!-- end page title -->

          <!-- Start page content-wrapper -->
          <div class="page-content-wrapper">
              

          </div>
          <!-- end page-content-wrapper-->
      </div>
      <!-- Container-fluid -->
  </div>
  <!-- End Page-content -->

@push('js')

<!-- Peity JS -->
<script src="{{asset('dashboard/libs/peity/jquery.peity.min.js')}}"></script>

<script src="{{asset('dashboard/libs/morris.js/morris.min.js')}}"></script>

<script src="{{asset('dashboard/libs/raphael/raphael.min.js')}}"></script>

<!-- Dashboard init JS -->
<script src="{{asset('dashboard/js/pages/dashboard.init.js')}}"></script>

<!-- App js -->
<script src="{{asset('dashboard/js/app.js')}}"></script>

@endpush

@endsection
