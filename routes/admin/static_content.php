<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/static-content','middleware'=>'admin'],function (){
	Route::controller(StaticContentController::class)->group(function(){
        Route::get('/edit','edit')->name('static.content.edit');
        Route::get('/show','show')->name('static.content.show');
        Route::put('/update','update')->name('static.content.update');
	});
});
