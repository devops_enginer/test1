<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/service','middleware'=>'admin'],function (){
	Route::controller(ServiceController::class)->group(function(){
		Route::get('/index', 'index')->name('service.list');
		Route::get('/create', 'create')->name('service.create');
        Route::post('/store', 'store')->name('service.store');
        Route::get('/edit/{service}','edit')->name('service.edit');
        Route::get('/show/{service}','show')->name('service.show');
        Route::put('/update/{service}','update')->name('service.update');
        Route::post('/delete_service','delete_service')->name('service.delete_service');
        Route::get('/service_list', 'service_list')->name('service.service_list');
	});
});
