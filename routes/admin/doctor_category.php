<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/doctor-category', 'middleware' => 'admin'],function (){
    Route::controller(DoctorCategoryController::class)->group(function(){
        Route::get('/index', 'index')->name('doctor.category.list');
        Route::get('/create', 'create')->name('doctor.category.create');
        Route::post('/store', 'store')->name('doctor.category.store');
        Route::get('/edit/{doctorCategory}','edit')->name('doctor.category.edit');
        Route::get('/show/{doctorCategory}','show')->name('doctor.category.show');
        Route::put('/update/{doctorCategory}','update')->name('doctor.category.update');
        Route::post('/delete_doctor_category','delete_doctor_category')->name('doctor.category.delete_doctor_category');
        Route::get('/doctor_category_list', 'doctor_category_list')->name('doctor.category.doctor_category_list');

        Route::get('/category-doctors', 'getCategoryDoctors')->name('doctor.category.doctors');
    });
});
