<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/doctor-appointment','middleware'=>'admin'],function (){
	Route::controller(DoctorAppointmentController::class)->group(function(){
		Route::get('/index', 'index')->name('doctor.appointment.list');
		Route::get('/create', 'create')->name('doctor.appointment.create');
        Route::post('/store', 'store')->name('doctor.appointment.store');
        Route::get('/edit/{appointment}','edit')->name('doctor.appointment.edit');
        Route::get('/show/{appointment}','show')->name('doctor.appointment.show');
        Route::put('/update/{appointment}','update')->name('doctor.appointment.update');
        Route::post('/delete_appointment','delete_appointment')->name('doctor.appointment.delete_appointment');
        Route::get('/appointment_list', 'appointment_list')->name('doctor.appointment.appointment_list');


        Route::get('/doctor-appointments', 'doctorAppointments')->name('doctor.appointments');

        Route::get('/appointment-times', 'appointmentTimes')->name('doctor.appointment.times');
	});
});
