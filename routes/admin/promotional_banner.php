<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/promotional-banner','middleware'=>'admin'],function (){
	Route::controller(PromotionalBannerController::class)->group(function(){
		Route::get('/index', 'index')->name('promotional.banner.list');
		Route::get('/create', 'create')->name('promotional.banner.create');
        Route::post('/store', 'store')->name('promotional.banner.store');
        Route::get('/edit/{promotionalBanner}','edit')->name('promotional.banner.edit');
        Route::get('/show/{promotionalBanner}','show')->name('promotional.banner.show');
        Route::put('/update/{promotionalBanner}','update')->name('promotional.banner.update');
        Route::post('/delete_banner','delete_banner')->name('promotional.banner.delete_banner');
        Route::get('/banner_list', 'banner_list')->name('promotional.banner.banner_list');
	});
});
