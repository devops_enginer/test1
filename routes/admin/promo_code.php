<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/promo-code','middleware'=>'admin'],function (){
	Route::controller(PromoCodeController::class)->group(function(){
		Route::get('/index', 'index')->name('promo.code.list');
		Route::get('/create', 'create')->name('promo.code.create');
        Route::post('/store', 'store')->name('promo.code.store');
        Route::get('/edit/{promoCode}','edit')->name('promo.code.edit');
        Route::get('/show/{promoCode}','show')->name('promo.code.show');
        Route::put('/update/{promoCode}','update')->name('promo.code.update');
        Route::post('/delete_code','delete_code')->name('promo.code.delete_code');
        Route::get('/code_list', 'code_list')->name('promo.code.code_list');
	});
});
