<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/booking','middleware'=>'admin'],function (){
	Route::controller(BookingController::class)->group(function(){
		Route::get('/index', 'index')->name('booking.list');
		Route::get('/create', 'create')->name('booking.create');
        Route::post('/store', 'store')->name('booking.store');
        Route::get('/edit/{booking}','edit')->name('booking.edit');
        Route::get('/show/{booking}','show')->name('booking.show');
        Route::put('/update/{booking}','update')->name('booking.update');
        Route::post('/delete_booking','delete_booking')->name('booking.delete_booking');
        Route::get('/booking_list', 'booking_list')->name('booking.booking_list');

        Route::get('/booking-result-details', 'resultDetails')->name('booking.result.details');
	});
});
