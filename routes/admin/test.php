<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/test','middleware'=>'admin'],function (){
	Route::controller(TestController::class)->group(function(){
		Route::get('/index', 'index')->name('test.list');
		Route::get('/create', 'create')->name('test.create');
        Route::post('/store', 'store')->name('test.store');
        Route::get('/edit/{test}','edit')->name('test.edit');
        Route::get('/show/{test}','show')->name('test.show');
        Route::put('/update/{test}','update')->name('test.update');
        Route::post('/delete_test','delete_test')->name('test.delete_test');
        Route::get('/test_list', 'test_list')->name('test.test_list');        
	});
});
