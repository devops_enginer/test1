<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/doctor-booking','middleware'=>'admin'],function (){
	Route::controller(DoctorBookingController::class)->group(function(){
		Route::get('/index', 'index')->name('doctor.booking.list');
		Route::get('/create', 'create')->name('doctor.booking.create');
        Route::post('/store', 'store')->name('doctor.booking.store');
        Route::get('/edit/{booking}','edit')->name('doctor.booking.edit');
        Route::get('/show/{booking}','show')->name('doctor.booking.show');
        Route::put('/update/{booking}','update')->name('doctor.booking.update');
        Route::post('/delete_booking','delete_booking')->name('doctor.booking.delete_booking');
        Route::get('/booking_list', 'booking_list')->name('doctor.booking.booking_list');
	});
});
