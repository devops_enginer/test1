<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/reason','middleware'=>'admin'],function (){
	Route::controller(ReasonController::class)->group(function(){
		Route::get('/index', 'index')->name('reason.list');
		Route::get('/create', 'create')->name('reason.create');
        Route::post('/store', 'store')->name('reason.store');
        Route::get('/edit/{reason}','edit')->name('reason.edit');
        Route::get('/show/{reason}','show')->name('reason.show');
        Route::put('/update/{reason}','update')->name('reason.update');
        Route::post('/delete_reason','delete_reason')->name('reason.delete_reason');
        Route::get('/reason_list', 'reason_list')->name('reason.reason_list');
	});
});
