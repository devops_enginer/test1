<?php

use Illuminate\Support\Facades\Route;

Route::get('/admin/home', [\App\Http\Controllers\Admin\HomeController::class, 'index'])->name('admin.home')->middleware('admin');
