<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/clinic','middleware'=>'admin'],function (){
	Route::controller(ClinicController::class)->group(function(){
		Route::get('/index', 'index')->name('clinic.list');
		Route::get('/create', 'create')->name('clinic.create');
        Route::post('/store', 'store')->name('clinic.store');
        Route::get('/edit/{clinic}','edit')->name('clinic.edit');
        Route::get('/show/{clinic}','show')->name('clinic.show');
        Route::put('/update/{clinic}','update')->name('clinic.update');
        Route::post('/delete_clinic','delete_clinic')->name('clinic.delete_clinic');
        Route::get('/clinic_list', 'clinic_list')->name('clinic.clinic_list');
	});
});
