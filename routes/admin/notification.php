<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/notification','middleware'=>'admin'],function (){
	Route::controller(NotificationController::class)->group(function(){
		Route::get('/index', 'index')->name('notification.list');
		Route::get('/create', 'create')->name('notification.create');
        Route::post('/store', 'store')->name('notification.store');
        Route::get('/show/{notification}','show')->name('notification.show');
        Route::post('/delete_notification','delete_notification')->name('notification.delete_notification');
        Route::get('/notification_list', 'notification_list')->name('notification.notification_list');
	});
});
