<?php
use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/user/','middleware'=>'admin'],function (){
    Route::resource('/users', 'UserController');
    Route::controller(UserController::class)->group(function(){
        Route::post('/delete_user', 'delete_user')->name('users.delete_user');
        Route::get('/user_list', 'user_list')->name('users.user_list');
    });
});
