<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/appointment','middleware'=>'admin'],function (){
	Route::controller(AppointmentController::class)->group(function(){
		Route::get('/index', 'index')->name('appointment.list');
		Route::get('/create', 'create')->name('appointment.create');
        Route::post('/store', 'store')->name('appointment.store');
        Route::get('/edit/{appointment}','edit')->name('appointment.edit');
        Route::get('/show/{appointment}','show')->name('appointment.show');
        Route::put('/update/{appointment}','update')->name('appointment.update');
        Route::post('/delete_appointment','delete_appointment')->name('appointment.delete_appointment');
        Route::get('/appointment_list', 'appointment_list')->name('appointment.appointment_list');


        Route::get('/test-clinic-appointments', 'testClinicAppointments')->name('clinic.test.appointments');

        Route::get('/appointment-times', 'appointmentTimes')->name('appointment.times');
	});
});
