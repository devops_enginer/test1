<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/country', 'middleware' => 'admin'],function (){
    Route::controller(CountryController::class)->group(function(){
        Route::get('/index', 'index')->name('country.list');
        Route::get('/create', 'create')->name('country.create');
        Route::post('/store', 'store')->name('country.store');
        Route::get('/edit/{country}','edit')->name('country.edit');
        Route::get('/show/{country}','show')->name('country.show');
        Route::put('/update/{country}','update')->name('country.update');
        Route::post('/delete_country','delete_country')->name('country.delete_country');
        Route::get('/country_list', 'country_list')->name('country.country_list');

        Route::get('/country-cities', 'getCountryCities')->name('country.cities');
        Route::get('/country-users', 'getCountryUsers')->name('country.users');
    });
});
