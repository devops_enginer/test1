<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/city','middleware'=>'admin'],function (){
	Route::controller(CityController::class)->group(function(){
		Route::get('/index', 'index')->name('city.list');
		Route::get('/create', 'create')->name('city.create');
        Route::post('/store', 'store')->name('city.store');
        Route::get('/edit/{city}','edit')->name('city.edit');
        Route::get('/show/{city}','show')->name('city.show');
        Route::put('/update/{city}','update')->name('city.update');
        Route::post('/delete_city','delete_city')->name('city.delete_city');
        Route::get('/city_list', 'city_list')->name('city.city_list');

        Route::get('/city-users', 'getCityUsers')->name('city.users');
	});
});
