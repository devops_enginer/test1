<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'/admin/doctor', 'middleware' => 'admin'],function (){
    Route::controller(DoctorController::class)->group(function(){
        Route::get('/index', 'index')->name('doctor.list');
        Route::get('/create', 'create')->name('doctor.create');
        Route::post('/store', 'store')->name('doctor.store');
        Route::get('/edit/{doctor}','edit')->name('doctor.edit');
        Route::get('/show/{doctor}','show')->name('doctor.show');
        Route::put('/update/{doctor}','update')->name('doctor.update');
        Route::post('/delete_doctor','delete_doctor')->name('doctor.delete_doctor');
        Route::get('/doctor_list', 'doctor_list')->name('doctor.doctor_list');
    });
});
