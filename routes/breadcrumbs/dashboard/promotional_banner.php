<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Promotional Banner
Breadcrumbs::for('promotional.banner.list', function ($trail) {
    $trail->push(__('Promotional Banners List'), route('promotional.banner.banner_list'));
});
// Home > Promotional Banner > Create
Breadcrumbs::for('promotional.banner.create', function ($trail) {
    $trail->parent('promotional.banner.list');
    $trail->push(__('Create New Promotional Banner'), route('promotional.banner.create'));
});
// Home > Promotional Banner > Edit
Breadcrumbs::for('promotional.banner.edit', function ($trail, $promotionalBanner) {
    $trail->parent('promotional.banner.list');
    $trail->push("Edit Promotional Banner", route('promotional.banner.edit', $promotionalBanner));
});

// Home > Promotional Banner > Show
Breadcrumbs::for('promotional.banner.show', function ($trail, $promotionalBanner) {
    $trail->parent('promotional.banner.list');
    $trail->push("Show Promotional Banner", route('promotional.banner.show', $promotionalBanner));
});
