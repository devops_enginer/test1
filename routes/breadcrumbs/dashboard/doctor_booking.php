<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Doctor Booking
Breadcrumbs::for('doctor.booking.list', function ($trail) {
    $trail->push(__('Doctor Bookings List'), route('doctor.booking.booking_list'));
});
// Home > Doctor Booking > Create
Breadcrumbs::for('doctor.booking.create', function ($trail) {
    $trail->parent('doctor.booking.list');
    $trail->push(__('Create New Doctor Booking'), route('doctor.booking.create'));
});
// Home > Doctor Booking > Edit
Breadcrumbs::for('doctor.booking.edit', function ($trail, $doctorBooking) {
    $trail->parent('doctor.booking.list');
    $trail->push("Edit Doctor Booking", route('doctor.booking.edit', $doctorBooking));
});

// Home > Doctor Booking > Show
Breadcrumbs::for('doctor.booking.show', function ($trail, $doctorBooking) {
    $trail->parent('doctor.booking.list');
    $trail->push("Show Doctor Booking", route('doctor.booking.show', $doctorBooking));
});
