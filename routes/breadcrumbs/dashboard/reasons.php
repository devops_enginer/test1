<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Reason
Breadcrumbs::for('reason.list', function ($trail) {
    $trail->push(__('Cities List'), route('reason.reason_list'));
});
// Home > Reason > Create
Breadcrumbs::for('reason.create', function ($trail) {
    $trail->parent('reason.list');
    $trail->push(__('Create New Reason'), route('reason.create'));
});
// Home > Reason > Edit
Breadcrumbs::for('reason.edit', function ($trail, $reason) {
    $trail->parent('reason.list');
    $trail->push("Edit Reason", route('reason.edit', $reason));
});

// Home > Reason > Show
Breadcrumbs::for('reason.show', function ($trail, $reason) {
    $trail->parent('reason.list');
    $trail->push("Show Reason", route('reason.show', $reason));
});
