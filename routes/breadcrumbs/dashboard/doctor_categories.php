<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Doctor Category
Breadcrumbs::for('doctor.category.list', function ($trail) {
    $trail->push(__('Doctor Categories List'), route('doctor.category.doctor_category_list'));
});
// Home > Doctor Category > Create
Breadcrumbs::for('doctor.category.create', function ($trail) {
    $trail->parent('doctor.category.list');
    $trail->push(__('Create New Doctor Category'), route('doctor.category.create'));
});
// Home > Doctor Category > Edit
Breadcrumbs::for('doctor.category.edit', function ($trail, $doctorCategory) {
    $trail->parent('doctor.category.list');
    $trail->push("Edit Doctor Category", route('doctor.category.edit', $doctorCategory));
});

// Home > Doctor Category > Show
Breadcrumbs::for('doctor.category.show', function ($trail, $doctorCategory) {
    $trail->parent('doctor.category.list');
    $trail->push("Show Doctor Category", route('doctor.category.show', $doctorCategory));
});
