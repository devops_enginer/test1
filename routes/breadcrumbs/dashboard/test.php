<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Test
Breadcrumbs::for('test.list', function ($trail) {
    $trail->push(__('Tests List'), route('test.test_list'));
});
// Home > Test > Create
Breadcrumbs::for('test.create', function ($trail) {
    $trail->parent('test.list');
    $trail->push(__('Create New Test'), route('test.create'));
});
// Home > Test > Edit
Breadcrumbs::for('test.edit', function ($trail, $test) {
    $trail->parent('test.list');
    $trail->push("Edit Test", route('test.edit', $test));
});

// Home > Test > Show
Breadcrumbs::for('test.show', function ($trail, $test) {
    $trail->parent('test.list');
    $trail->push("Show Test", route('test.show', $test));
});
