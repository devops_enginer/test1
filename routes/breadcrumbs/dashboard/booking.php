<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Booking
Breadcrumbs::for('booking.list', function ($trail) {
    $trail->push(__('Bookings List'), route('booking.booking_list'));
});
// Home > Booking > Create
Breadcrumbs::for('booking.create', function ($trail) {
    $trail->parent('booking.list');
    $trail->push(__('Create New Booking'), route('booking.create'));
});
// Home > Booking > Edit
Breadcrumbs::for('booking.edit', function ($trail, $booking) {
    $trail->parent('booking.list');
    $trail->push("Edit Booking", route('booking.edit', $booking));
});

// Home > Booking > Show
Breadcrumbs::for('booking.show', function ($trail, $booking) {
    $trail->parent('booking.list');
    $trail->push("Show Booking", route('booking.show', $booking));
});
