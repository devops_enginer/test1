<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Appointment
Breadcrumbs::for('appointment.list', function ($trail) {
    $trail->push(__('Appointments List'), route('appointment.appointment_list'));
});
// Home > Appointment > Create
Breadcrumbs::for('appointment.create', function ($trail) {
    $trail->parent('appointment.list');
    $trail->push(__('Create New Appointment'), route('appointment.create'));
});
// Home > Appointment > Edit
Breadcrumbs::for('appointment.edit', function ($trail, $appointment) {
    $trail->parent('appointment.list');
    $trail->push("Edit Appointment", route('appointment.edit', $appointment));
});

// Home > Appointment > Show
Breadcrumbs::for('appointment.show', function ($trail, $appointment) {
    $trail->parent('appointment.list');
    $trail->push("Show Appointment", route('appointment.show', $appointment));
});
