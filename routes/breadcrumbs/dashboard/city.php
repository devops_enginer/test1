<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > City
Breadcrumbs::for('city.list', function ($trail) {
    $trail->push(__('Cities List'), route('city.city_list'));
});
// Home > City > Create
Breadcrumbs::for('city.create', function ($trail) {
    $trail->parent('city.list');
    $trail->push(__('Create New City'), route('city.create'));
});
// Home > City > Edit
Breadcrumbs::for('city.edit', function ($trail, $city) {
    $trail->parent('city.list');
    $trail->push("Edit City", route('city.edit', $city));
});

// Home > City > Show
Breadcrumbs::for('city.show', function ($trail, $city) {
    $trail->parent('city.list');
    $trail->push("Show City", route('city.show', $city));
});
