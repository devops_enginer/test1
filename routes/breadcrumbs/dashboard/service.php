<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Service
Breadcrumbs::for('service.list', function ($trail) {
    $trail->push(__('Services List'), route('service.service_list'));
});
// Home > Service > Create
Breadcrumbs::for('service.create', function ($trail) {
    $trail->parent('service.list');
    $trail->push(__('Create New Service'), route('service.create'));
});
// Home > Service > Edit
Breadcrumbs::for('service.edit', function ($trail, $service) {
    $trail->parent('service.list');
    $trail->push("Edit Service", route('service.edit', $service));
});

// Home > Service > Show
Breadcrumbs::for('service.show', function ($trail, $service) {
    $trail->parent('service.list');
    $trail->push("Show Service", route('service.show', $service));
});
