<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Doctor
Breadcrumbs::for('doctor.list', function ($trail) {
    $trail->push(__('Doctors List'), route('doctor.doctor_list'));
});
// Home > Doctor > Create
Breadcrumbs::for('doctor.create', function ($trail) {
    $trail->parent('doctor.list');
    $trail->push(__('Create New Doctor'), route('doctor.create'));
});
// Home > Doctor > Edit
Breadcrumbs::for('doctor.edit', function ($trail, $doctor) {
    $trail->parent('doctor.list');
    $trail->push("Edit Doctor", route('doctor.edit', $doctor));
});

// Home > Doctor > Show
Breadcrumbs::for('doctor.show', function ($trail, $doctor) {
    $trail->parent('doctor.list');
    $trail->push("Show Doctor", route('doctor.show', $doctor));
});
