<?php
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
//Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push(__('home'), route('home'));
});
require __DIR__.'/user.php';
require __DIR__.'/country.php';
require __DIR__.'/city.php';
require __DIR__.'/promotional_banner.php';
require __DIR__.'/service.php';
require __DIR__.'/test.php';
require __DIR__.'/clinic.php';
require __DIR__.'/appointment.php';
require __DIR__.'/doctor_appointment.php';
require __DIR__.'/booking.php';
require __DIR__.'/doctor_booking.php';
require __DIR__.'/promo_code.php';
require __DIR__.'/static_content.php';
require __DIR__.'/notification.php';
require __DIR__.'/reasons.php';
require __DIR__.'/doctors.php';
require __DIR__.'/doctor_categories.php';
