<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Notification
Breadcrumbs::for('notification.list', function ($trail) {
    $trail->push(__('Notifications List'), route('notification.notification_list'));
});
// Home > Notification > Create
Breadcrumbs::for('notification.create', function ($trail) {
    $trail->parent('notification.list');
    $trail->push(__('Send New Notification'), route('notification.create'));
});

// Home > Notification > Show
Breadcrumbs::for('notification.show', function ($trail, $notification) {
    $trail->parent('notification.list');
    $trail->push("Show Notification", route('notification.show', $notification));
});
