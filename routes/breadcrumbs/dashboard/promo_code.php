<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Promo Code
Breadcrumbs::for('promo.code.list', function ($trail) {
    $trail->push(__('Promo Codes List'), route('promo.code.code_list'));
});
// Home > Promo Code > Create
Breadcrumbs::for('promo.code.create', function ($trail) {
    $trail->parent('promo.code.list');
    $trail->push(__('Create New Promo Code'), route('promo.code.create'));
});
// Home > Promo Code > Edit
Breadcrumbs::for('promo.code.edit', function ($trail, $promoCode) {
    $trail->parent('promo.code.list');
    $trail->push("Edit Promo Code", route('promo.code.edit', $promoCode));
});

// Home > Promo Code > Show
Breadcrumbs::for('promo.code.show', function ($trail, $promoCode) {
    $trail->parent('promo.code.list');
    $trail->push("Show Promo Code", route('promo.code.show', $promoCode));
});
