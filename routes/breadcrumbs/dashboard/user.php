<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > User
Breadcrumbs::for('user.list', function ($trail) {
    $trail->push(__('Users List'), route('users.user_list'));
});
// Home > User > Create
Breadcrumbs::for('user.create', function ($trail) {
    $trail->parent('user.list');
    $trail->push(__('Create New User'), route('users.create'));
});
// Home > User > Edit
Breadcrumbs::for('user.edit', function ($trail, $user) {
    $trail->parent('user.list');
    $trail->push("Edit User", route('users.edit',$user));
});

// Home > User > Show
Breadcrumbs::for('user.show', function ($trail, $user) {
    $trail->parent('user.list');
    $trail->push("Show User", route('users.show',$user));
});
