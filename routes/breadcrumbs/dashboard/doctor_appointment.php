<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Doctor Appointment
Breadcrumbs::for('doctor.appointment.list', function ($trail) {
    $trail->push(__('Doctor Appointments List'), route('doctor.appointment.appointment_list'));
});
// Home > Doctor Appointment > Create
Breadcrumbs::for('doctor.appointment.create', function ($trail) {
    $trail->parent('doctor.appointment.list');
    $trail->push(__('Create New Doctor Appointment'), route('doctor.appointment.create'));
});
// Home > Doctor Appointment > Edit
Breadcrumbs::for('doctor.appointment.edit', function ($trail, $doctorAppointment) {
    $trail->parent('doctor.appointment.list');
    $trail->push("Edit Doctor Appointment", route('doctor.appointment.edit', $doctorAppointment));
});

// Home > Doctor Appointment > Show
Breadcrumbs::for('doctor.appointment.show', function ($trail, $doctorAppointment) {
    $trail->parent('doctor.appointment.list');
    $trail->push("Show Doctor Appointment", route('doctor.appointment.show', $doctorAppointment));
});
