<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Clinic
Breadcrumbs::for('clinic.list', function ($trail) {
    $trail->push(__('Clinics List'), route('clinic.clinic_list'));
});
// Home > Clinic > Create
Breadcrumbs::for('clinic.create', function ($trail) {
    $trail->parent('clinic.list');
    $trail->push(__('Create New Clinic'), route('clinic.create'));
});
// Home > Clinic > Edit
Breadcrumbs::for('clinic.edit', function ($trail, $clinic) {
    $trail->parent('clinic.list');
    $trail->push("Edit Clinic", route('clinic.edit', $clinic));
});

// Home > Clinic > Show
Breadcrumbs::for('clinic.show', function ($trail, $clinic) {
    $trail->parent('clinic.list');
    $trail->push("Show Clinic", route('clinic.show', $clinic));
});
