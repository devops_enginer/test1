<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Country
Breadcrumbs::for('country.list', function ($trail) {
    $trail->push(__('Countrys List'), route('country.country_list'));
});
// Home > Country > Create
Breadcrumbs::for('country.create', function ($trail) {
    $trail->parent('country.list');
    $trail->push(__('Create New Country'), route('country.create'));
});
// Home > Country > Edit
Breadcrumbs::for('country.edit', function ($trail, $country) {
    $trail->parent('country.list');
    $trail->push("Edit Country", route('country.edit', $country));
});

// Home > Country > Show
Breadcrumbs::for('country.show', function ($trail, $country) {
    $trail->parent('country.list');
    $trail->push("Show Country", route('country.show', $country));
});
