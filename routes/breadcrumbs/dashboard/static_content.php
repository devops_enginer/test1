<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Home > Promotional Banner
Breadcrumbs::for('static.content.list', function ($trail) {
    $trail->push(__('Static Content List'), route('static.content.edit'));
});

// Home > Website Content > Edit
Breadcrumbs::for('static.content.edit', function ($trail, $staticContent) {
    $trail->parent('static.content.list');
    $trail->push("Edit Website Content", route('static.content.edit', $staticContent));
});

// Home > Website Content > Show
Breadcrumbs::for('static.content.show', function ($trail, $staticContent) {
    $trail->parent('static.content.list');
    $trail->push("Show Website Content", route('static.content.show', $staticContent));
});
