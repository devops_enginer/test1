<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect('login');
});

Auth::routes();

// Route::get('/test','\App\Http\Controllers\Api\V1\AuthApiController@testSocial');

// Route::post('/users/auth/apple/callback','\App\Http\Controllers\Api\V1\AuthApiController@testSocialCallback');