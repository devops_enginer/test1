<?php
   
use Illuminate\Support\Facades\Route;
  
// use App\Http\Controllers\Web\StripePaymentController;
  
Route::group(['prefix' => 'payment', 'namespace' => 'Web'], function(){
    Route::controller(StripePaymentController::class)->group(function(){
        Route::get('stripe-add-payment-method', 'createPaymentMethod')->name('payment.stripe.method');
        Route::post('stripe-add-payment-method', 'createPaymentMethodPost')->name('payment.stripe.method.post');

        Route::get('stripe-pay', 'stripePay')->name('payment.stripe.pay');
        Route::post('stripe-pay', 'stripePayPost')->name('payment.stripe.pay.post');
    });
});