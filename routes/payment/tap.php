<?php
   
use Illuminate\Support\Facades\Route;
  
// use App\Http\Controllers\Web\TapPaymentController;
  
Route::group(['prefix' => 'payment', 'namespace' => 'Web'], function(){
    Route::controller(TapPaymentController::class)->group(function(){
        Route::get('tap-add-payment-method', 'createPaymentMethod')->name('payment.tap.method');
        Route::post('tap-add-payment-method', 'createPaymentMethodPost')->name('payment.tap.method.post');
    });
});