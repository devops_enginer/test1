<?php

Route::group(['prefix'=>'/stripe', 'middleware' => ['auth:sanctum']], function(){
	Route::get('/payment-methods','StripePaymentApiController@paymentMethods');
	Route::post('/new-payment-method','StripePaymentApiController@newPaymentMethod');
	Route::post('/pay-test','StripePaymentApiController@payTest');
	Route::post('/pay-doctor-appointment','StripePaymentApiController@payDoctorAppointment');
	Route::post('/remove-method','StripePaymentApiController@removePaymentMethod');
});