<?php

Route::group(['prefix'=>'/doctor-appointments', 'middleware' => ['auth:sanctum']], function(){
	Route::get('/doctor','DoctorAppointmentApiController@doctorAppointments');
	Route::post('/book-appointment','DoctorAppointmentApiController@bookAppointment');
	Route::post('/cancel-booking','DoctorAppointmentApiController@cancelBooking');
	Route::post('/reschedule-booking','DoctorAppointmentApiController@rescheduleBooking');
});