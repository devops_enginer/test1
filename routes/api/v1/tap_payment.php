<?php

Route::get('/tap/authorize/redirect','TapPaymentApiController@authRedirect')->name('tap.authorize.redirect');
Route::get('/tap/payment/redirect','TapPaymentApiController@payRedirect')->name('tap.payment.redirect');

Route::group(['prefix'=>'/tap', 'middleware' => ['auth:sanctum']], function(){
	Route::get('/payment-methods','TapPaymentApiController@paymentMethods');

	Route::post('/new-payment-method','TapPaymentApiController@newPaymentMethod');
	Route::post('/pay-test','TapPaymentApiController@payTest');
	Route::post('/pay-doctor-appointment','TapPaymentApiController@payDoctorAppointment');
	Route::post('/remove-method','TapPaymentApiController@removePaymentMethod');
});