<?php

Route::group(['prefix'=>'/users', 'middleware' => ['auth:sanctum']], function(){
	Route::get('/profile','UserApiController@profile');
	Route::get('/notifications','UserApiController@notifications');
	Route::get('/results','UserApiController@results');
	Route::get('/bookings','UserApiController@bookings');
	Route::get('/doctor-bookings','UserApiController@doctorBookings');
	Route::post('/update','UserApiController@update');
});