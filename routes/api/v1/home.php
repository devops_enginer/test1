<?php

Route::group(['prefix'=>'/', 'middleware' => ['auth:sanctum']], function(){
	Route::get('/home','HomeApiController@home');
	Route::get('/static-content','HomeApiController@staticContent');
});