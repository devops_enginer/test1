<?php

Route::group(['prefix'=>'/services', 'middleware' => ['auth:sanctum']], function(){
	Route::get('/','ServiceApiController@index');
	Route::get('/service-test','ServiceApiController@categoryTests');
});