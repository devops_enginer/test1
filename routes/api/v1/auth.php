<?php

Route::group(['prefix'=>'/users/auth'], function(){
	Route::post('/register','AuthApiController@register');
	Route::post('/login','AuthApiController@login');
	Route::post('/reset-password-request','AuthApiController@resetPasswordRequest');
	Route::post('/reset-password','AuthApiController@resetPassword');
	Route::post('/social-signin','AuthApiController@socialLogin');

	Route::group(['middleware' => 'auth:sanctum'], function(){
		Route::post('/logout','AuthApiController@logout');
		Route::post('/change-password','AuthApiController@changePassword');
	});
});