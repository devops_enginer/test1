<?php

Route::group(['prefix'=>'/appointments', 'middleware' => ['auth:sanctum']], function(){
	Route::get('/test-appointments','AppointmentApiController@testAppointments');
	Route::get('/clinic-appointments','AppointmentApiController@clinicAppointments');
	Route::get('/test-clinic-appointments','AppointmentApiController@clinicTestAppointments');
	Route::post('/book-appointment','AppointmentApiController@bookAppointment');
	Route::post('/cancel-booking','AppointmentApiController@cancelBooking');
	Route::post('/reschedule-booking','AppointmentApiController@rescheduleBooking');
});