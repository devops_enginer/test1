<?php

Route::group(['prefix'=>'/clinics', 'middleware' => ['auth:sanctum']], function(){
	Route::get('/default-clinic','ClinicApiController@defaultClinic');
	Route::get('/location','ClinicApiController@location');
});