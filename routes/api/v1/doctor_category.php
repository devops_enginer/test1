<?php

Route::group(['prefix'=>'/doctors-categories', 'middleware' => ['auth:sanctum']], function(){
	Route::get('/','DoctorCategoryApiController@index');
	Route::get('/doctor','DoctorCategoryApiController@categoryDoctors');
});