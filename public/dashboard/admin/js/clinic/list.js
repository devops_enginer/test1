(function ($) {
    "use strict";
    let table = $('#datatable').DataTable({
        serverSide: true,
        scrollY: 500,
        scrollX: true,
        scrollCollapse: true,
        processing: true,
        "ordering": false,
        pageLength: 10,
        ajax:{
            url:$('#table-url').data("url"),
        },
        order:[0,'asc'],
        autoWidth: false,
        language: {
            paginate: {
                next: 'Next &#8250;',
                previous: '&#8249; Previous'
            }
        },
        columns: [
            {"data": 'checkbox', "name": 'checkbox', "orderable": false, "searchable": false},
            {"data": "name","name": 'name', "searchable": false},
            {"data": "image","name": 'image'},
            {"data": "address","name": 'address'},
            {"data": "city","name": 'city'},
            {"data": "country","name": 'country'},
            {"data": "rating","name": 'rating'},
            {"data": "number_of_ratings","name": 'number_of_ratings'},
            {"data": "working_hours","name": 'working_hours'},
            {"data": "is_default","name": 'is_default'},
            {"data": "action",'name':'action'}
        ]
    });
    $('.filter-select').change(function(){
        table.draw();
    });
})(jQuery)

