(function ($) {
    "use strict";
    let table = $('#datatable').DataTable({
        serverSide: true,
        scrollY: 500,
        scrollX: true,
        scrollCollapse: true,
        processing: true,
        "ordering": false,
        pageLength: 10,
        ajax:{
            url:$('#table-url').data("url"),
            data: {
                rescheduled: getUrlParameter('rescheduled')
            }
        },
        order:[0,'asc'],
        autoWidth: false,
        language: {
            paginate: {
                next: 'Next &#8250;',
                previous: '&#8249; Previous'
            }
        },
        columns: [
            {"data": 'checkbox', "name": 'checkbox', "orderable": false, "searchable": false},
            {"data": "user","name": 'user', "searchable": false},
            {"data": "date","name": 'date', "searchable": false},
            {"data": "time","name": 'time', "searchable": false},
            {"data": "created_at","name": 'created_at', "searchable": false},
            {"data": "test","name": 'test', "searchable": false},
            {"data": "clinic","name": 'clinic', "searchable": false},
            {"data": "country","name": 'country', "searchable": false},
            {"data": "city","name": 'city', "searchable": false},
            {"data": "price","name": 'price', "searchable": false},
            {"data": "status","name": 'status', "searchable": false},
            {"data": "payment_status","name": 'payment_status', "searchable": false},
            {"data": "reason","name": 'reason', "searchable": false},
            {"data": "result","name": 'result', "searchable": false},
            {"data": "action",'name':'action', "searchable": false}
        ]
    });

    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return null;
    }
    
    $('.filter-select').change(function(){
        table.draw();
    });
})(jQuery)

