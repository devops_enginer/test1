/**
 * Handle posts national_id icon preview
 */

document.getElementById('icon_select').addEventListener('change', function() {
    // Create the icon preview
    var container = document.getElementById('icon_container');
    var icon = document.createElement('img');
    icon.className = 'img-fluid "col-md-6';
    icon.style='width:132px';
    icon.setAttribute('id', 'icon_preview');

    var file = document.getElementById('icon_select').files[0];
    var reader = new FileReader();

    reader.addEventListener("load", function () {
        icon.src = reader.result;
        container.appendChild(icon);
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }

    // Check if a icon is selected
    if (document.getElementById('icon_select').files.length === 1) {
        document.getElementById('remove_icon_button').classList.remove('display-none');
        document.getElementById('remove_icon_button').classList.add('display-block');
        document.getElementById('icon_select').classList.remove('display-block');
        document.getElementById('icon_select').classList.add('display-none');

        // Remove the 'removed' input if a icon is selected
        if (document.getElementById('removed')) {
            var elem = document.getElementById('removed');
            elem.parentNode.removeChild(elem);
        }
    }

});

document.getElementById('remove_icon_button').addEventListener('click', function() {
    document.getElementById('icon_preview').outerHTML = '';

    document.getElementById('remove_icon_button').classList.remove('display-block');
    document.getElementById('remove_icon_button').classList.add('display-none');

    document.getElementById('icon_select').classList.remove('display-none');
    document.getElementById('icon_select').classList.add('display-block');

    document.getElementById('icon_select').value = '';

    // Create a hidden input if the icon is removed
    var input = document.createElement("input");
    input.setAttribute('type', 'hidden');
    input.setAttribute('id', 'removed');
    input.setAttribute('name', 'removed');
    document.getElementById('icon_container').appendChild(input);
});
