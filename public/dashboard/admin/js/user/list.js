(function ($) {
    "use strict";
    let table = $('#datatable').DataTable({
        serverSide: true,
        scrollY: 500,
        scrollX: true,
        scrollCollapse: true,
        processing: true,
        "ordering": false,
        pageLength: 10,
        ajax:{
            url:$('#table-url').data("url"),
        },
        order:[0,'asc'],
        autoWidth: false,
        language: {
            paginate: {
                next: 'Next &#8250;',
                previous: '&#8249; Previous'
            }
        },
        columns: [
            {"data": 'checkbox', "name": 'checkbox', "searchable": false},
            {"data": "full_name","name": 'full_name', "searchable": false},
            {"data": "email","name": 'email', "searchable": false},
            {"data": "phone_number",'name':'phone_number', "searchable": false},
            {"data": "account_type",'name':'account_type', "searchable": false},
            {"data": "address_line_1",'name':'address_line_1', "searchable": false},
            {"data": "address_line_2",'name':'address_line_2', "searchable": false},
            {"data": "language",'name':'language', "searchable": false},
            {"data": "city",'name':'city', "searchable": false},
            {"data": "country",'name':'country', "searchable": false},
            {"data": "action",'name':'action'}
        ]
    });
    $('.filter-select').change(function(){
        table.draw();
    });
})(jQuery)

