(function ($) {
    "use strict";
    let table = $('#datatable').DataTable({
        serverSide: true,
        scrollY: 500,
        scrollX: true,
        scrollCollapse: true,
        processing: true,
        "ordering": false,
        pageLength: 10,
        ajax:{
            url:$('#table-url').data("url"),
        },
        order:[0,'asc'],
        autoWidth: false,
        language: {
            paginate: {
                next: 'Next &#8250;',
                previous: '&#8249; Previous'
            }
        },
        columns: [
            {"data": 'checkbox', "name": 'checkbox', "orderable": false, "searchable": false},
            {"data": "title", "name": 'title'},
            {"data": "description", "name": 'description'},
            {"data": "thumbnail","name": 'thumbnail', "searchable": false},
            {"data": "image","name": 'image', "searchable": false},
            {"data": "is_featured","name": 'is_featured', "searchable": false},
            {"data": "service", "name": 'service', "searchable": false},
            {"data": "clinics", "name": 'clinics', "searchable": false},
            {"data": "action",'name':'action'}
        ]
    });
    $('.filter-select').change(function(){
        table.draw();
    });
})(jQuery)

