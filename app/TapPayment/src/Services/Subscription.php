<?php

namespace VMdevelopment\TapPayment\Services;

use Illuminate\Support\Facades\Validator;
use VMdevelopment\TapPayment\Abstracts\AbstractService;
use Propaganistas\LaravelPhone\PhoneNumber;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

use Carbon\Carbon;

class Subscription extends AbstractService
{
	protected $endpoint = 'subscription/v1/';
	protected $method = 'post';
	protected $attributes = [
		'term' => [
	        'interval' => '',
	        'period' => 0,
	        'from' => '',
	        'due' => 0,
	        'auto_renew' => true,
	        'timezone' => 'Asia/Dubai'
	    ],
	    'trial' => [
	        'days' => 0,
	        'amount' => 0.0
	    ],
	    'charge' => [
	        'amount' => 0,
	        'currency' => '',
	        'description' => 'The Digital Hotelier Subscription',
	        'statement_descriptor' => 'The Digital Hotelier Subscription',
	        'metadata' => '',
	        'receipt' => [
	            'email' => true,
	            'sms' => true
	        ],
	        'customer' => [
	            'id' => ''
	        ],
	        'source' => [
	            'id' => ''
	        ],
	        'post' => [
	            'url' => ''
	        ]
	    ]
	];


	public function __construct( $id = null )
	{
		if ( $id ) {
			$this->attributes['id'] = $id;
			$this->setEndpoint( $id );
		}
		parent::__construct();
	}


	protected function setEndpoint( $endpoint )
	{
		$this->endpoint .= $endpoint;
	}

	public function setInterval( $interval )
	{
		$this->attributes['term']['interval'] = $interval;
	}

	public function setPeriod( $period )
	{
		$this->attributes['term']['period'] = $period;
	}

	public function setFrom( $from )
	{
		$from = $from . 'T12:00:00';
		$this->attributes['term']['from'] = $from;
	}

	public function setAmount( $amount )
	{
		$this->attributes['charge']['amount'] = $amount;
	}

	public function setCurrency( $currency )
	{
		$this->attributes['charge']['currency'] = $currency;
	}

	public function setDescription( $description )
	{
		$this->attributes['charge']['description'] = $description;
		$this->attributes['charge']['statement_descriptor'] = $description;
	}

	public function setCustomerId( $id )
	{
		$this->attributes['charge']['customer']['id'] = $id;
	}

	public function setCardId( $id )
	{
		$this->attributes['charge']['source']['id'] = $id;
	}

	public function setPostUrl( $url )
	{
		$this->attributes['charge']['post']['url'] = $url;
	}

	public function setMetaData( array $meta )
	{
		$this->attributes['metadata'] = $meta;
	}

	public function setRawAttributes( array $attributes )
	{
		$this->attributes = $attributes;
	}

	public function getData()
	{
		return $this->attributes;
	}

	protected function setMethod( $method )
	{
		$this->method = $method;
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function find()
	{
		$this->setMethod( 'get' );
		if ($this->validateAttributes(['id' => 'required']))
			return $this->send();
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function cancel()
	{
		$this->setMethod( 'delete' );
		if ($this->validateAttributes(['id' => 'required']))
			return $this->send();
	}

	/**
	 * @param $rules
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function validateAttributes( array $rules, $messages = [] )
	{
		$validator = Validator::make( $this->attributes, $rules, $messages );

		if ( $validator->fails() )
			throw new \Exception( $validator->errors()->first() );

		return true;
	}


	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	protected function send()
	{
		$client = new Client([
			'headers' => ['Authorization' => 'Bearer ' . config( 'tap-payment.auth.api_key' )],
			'Accept' => 'application/json',
		]);

		try {
			$response = $client->request($this->method, $this->getPath(), ['json' => $this->attributes]);

			return json_decode( $response->getBody()->getContents(), true );
		}
		catch ( \Throwable $exception ) {
			throw new \Exception($exception->getResponse()->getBody()->getContents());
		}
	}


	/**
	 * @return Invoice
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function create()
	{
		$rules = [
			'term.interval' => 'required',
			'term.period' => 'required',
			'term.from' => 'required',
			'charge.amount' => 'required',
			'charge.currency' => 'required',
			'charge.description' => 'required',
			'charge.statement_descriptor' => 'required',
			'charge.customer.id' => 'required',
			'charge.source.id' => 'required',
		];

		if ($this->validateAttributes($rules))
		
		return $this->send();
	}
}