<?php

namespace VMdevelopment\TapPayment\Services;

use Illuminate\Support\Facades\Validator;
use VMdevelopment\TapPayment\Abstracts\AbstractService;
use VMdevelopment\TapPayment\Resources\Invoice;

class Card extends AbstractService
{
	protected $endpoint = 'card/';
	protected $method = 'post';
	protected $attributes = [];


	public function __construct( $id = null )
	{
		if ( $id ) {
			$this->attributes['id'] = $id;
			$this->setEndpoint( $id );
		}
		parent::__construct();
	}


	protected function setEndpoint( $endpoint )
	{
		$this->endpoint .= $endpoint;
	}


	public function setToken( $token )
	{
		$this->attributes['source'] = $token;
	}

	public function setRawAttributes( array $attributes )
	{
		$this->attributes = $attributes;
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function find()
	{
		$this->setMethod( 'get' );
		if ($this->validateAttributes(['id' => 'required']))
			return $this->send();
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function saveCard($token)
	{
		$this->setMethod( 'post' );
		$this->setToken($token);
		return $this->send();
	}

	protected function setMethod( $method )
	{
		$this->method = $method;
	}

	/**
	 * @param $rules
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function validateAttributes( array $rules, $messages = [] )
	{
		$validator = Validator::make( $this->attributes, $rules, $messages );

		if ( $validator->fails() )
			throw new \Exception( $validator->errors()->first() );

		return true;
	}


	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	protected function send()
	{
		try {
			$response = $this->client->request(
				$this->method,
				$this->getPath(),
				[
					'form_params' => $this->attributes,
					'headers'     => [
						'Authorization' => 'Bearer ' . config( 'tap-payment.auth.api_key' ),
						'Accept'        => 'application/json',
					]
				]
			);

			return json_decode( $response->getBody()->getContents(), true );
		}
		catch ( \Throwable $exception ) {
			throw new \Exception( $exception->getMessage() );
		}
	}
}