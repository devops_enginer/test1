<?php

namespace VMdevelopment\TapPayment\Services;

use Illuminate\Support\Facades\Validator;
use VMdevelopment\TapPayment\Abstracts\AbstractService;
use Propaganistas\LaravelPhone\PhoneNumber;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Business extends AbstractService
{
	protected $endpoint = 'business/';
	protected $method = 'post';
	protected $attributes = [
		'name' => [
	        'en' => ''
	    ],
	    'type' => 'corp',
	    'entity' => [
	        'legal_name' => [
	            'en' => ''
	        ],
	        'license' => [
	            'type' => 'Commercial Registration',
	            'number' => ''
	        ],
	        'not_for_profit' => false,
	        'country' => 'AE',
	        'tax_number' => '',
	        'documents' => [
	            [
	                'type' => 'Commercial Registration',
	                'number' => '',
	                'issuing_country' => 'AE',
	                'issuing_date' => '',
	                'expiry_date' => '',
	                'files' => [
	                    ''
	                ]
	            ]
	        ],
	        'bank_account' => [
	            'iban' => '',
	            'swift_code' => '',
	            'account_number' => ''
	        ],
	        'billing_address' => [
	            'recipient_name' => '',
	            'address_1' => '',
	            'address_2' => '',
	            'po_box' => '',
	            'district' => 'Dubai',
	            'city' => 'Dubai',
	            'state' => 'Dubai',
	            'zip_code' => '',
	            'country' => 'AE'
	        ]
	    ],
	    'contact_person' => [
	        'name' => [
	            'title' => 'Mr',
	            'first' => '',
	            'middle' => '',
	            'last' => ''
	        ],
	        'contact_info' => [
	            'primary' => [
	                'email' => '',
	                'phone' => [
	                    'country_code' => '',
	                    'number' => ''
	                ]
	            ]
	        ],
	        'nationality' => '',
	        'date_of_birth' => '',
	        'is_authorized' => true,
	        'identification' => [
	            [
	                'type' => '',
	                'issuing_country' => 'AE',
	                'issuing_date' => '',
	                'expiry_date' => '',
	                'files' => [
	                    '',
	                ]
	            ]
	        ]
	    ],
	    'brands' => [
	        [
	            'name' => [
	                'en' => ''
	            ],
	            'sector' => [
	                'Web Dev/Design'
	            ],
	            'website' => '',
	            'social' => [],
	            'logo' => '',
	            'content' => [
	                'tag_line' => [
	                    'en' => ''
	                ],
	                'about' => [
	                    'en' => ''
	                ]
	            ]
	        ]
	    ],
	    'post' => [
	        'url' => ''
	    ],
	    'metadata' => [
	        'mtd' => ''
	    ]
	];


	public function __construct()
	{
		parent::__construct();
	}


	protected function setEndpoint( $endpoint )
	{
		$this->endpoint .= $endpoint;
	}


	public function setName( $name )
	{
		$this->attributes['name']['en'] = $name;
		$this->attributes['entity']['legal_name']['en'] = $name;
		$this->attributes['brands'][0]['name']['en'] = $name;
	}

	public function setWebsite( $website )
	{
		$this->attributes['brands'][0]['website'] = $website;
	}

	public function setLogo( $logo )
	{
		$this->attributes['brands'][0]['logo'] = $logo;
	}

	public function setLicenseNumber( $number )
	{
		$this->attributes['entity']['license']['number'] = $number;
	}

	public function setTaxNumber( $number )
	{
		$this->attributes['entity']['tax_number'] = $number;
	}

	public function setDocuments($number, $issuingDate, $expiryDate, $file)
	{
		$this->attributes['entity']['documents'][0]['number'] = $number;
		$this->attributes['entity']['documents'][0]['issuing_date'] = $issuingDate;
		$this->attributes['entity']['documents'][0]['expiry_date'] = $expiryDate;
		$this->attributes['entity']['documents'][0]['files'] = [$file];
	}

	public function setBankAccount($iban, $swiftCode, $accountNumber)
	{
		$this->attributes['entity']['bank_account']['iban'] = $iban;
		$this->attributes['entity']['bank_account']['swift_code'] = $swiftCode;
		$this->attributes['entity']['bank_account']['account_number'] = $accountNumber;
	}

	public function setNationality($nationality)
	{
		$this->attributes['contact_person']['nationality'] = $nationality;
	}

	public function setDateOfBirth($date)
	{
		$this->attributes['contact_person']['date_of_birth'] = $date;
	}

	public function setIdentification($type, $issuingDate, $expiryDate, $files)
	{
		$this->attributes['contact_person']['identification'][0]['type'] = $type;
		$this->attributes['contact_person']['identification'][0]['issuing_date'] = $issuingDate;
		$this->attributes['contact_person']['identification'][0]['expiry_date'] = $expiryDate;
		$this->attributes['contact_person']['identification'][0]['files'] = $files;
	}

	public function setContactPerson($name, $email, $phone)
	{
		$phoneNumber =  PhoneNumber::make($phone)->getPhoneNumberInstance();

		$nNumber = $phoneNumber->getNationalNumber();
		$cCode = '+' . $phoneNumber->getCountryCode();

		$nameArr = explode(' ', $name);
		$firstName = $nameArr[0] ?? '';
		$middleName = $nameArr[1] ?? '';
		$lastName = $nameArr[2] ?? ($nameArr[1] ?? '');

		$this->attributes['contact_person']['name']['first'] = $firstName;
		$this->attributes['contact_person']['name']['middle'] = $middleName;
		$this->attributes['contact_person']['name']['last'] = $lastName;

		$this->attributes['contact_person']['contact_info']['primary']['email'] = $email;
		$this->attributes['contact_person']['contact_info']['primary']['phone']['country_code'] = $cCode;
		$this->attributes['contact_person']['contact_info']['primary']['phone']['number'] = $nNumber;
	}

	public function setBillingAddress($name, $address_1, $city, $state, $zipCode, $country)
	{
		$this->attributes['entity']['billing_address']['recipient_name'] = $name;
		$this->attributes['entity']['billing_address']['address_1'] = $address_1;
		$this->attributes['entity']['billing_address']['city'] = $city;
		$this->attributes['entity']['billing_address']['district'] = $city;
		$this->attributes['entity']['billing_address']['state'] = $state;
		$this->attributes['entity']['billing_address']['zip_code'] = $zipCode;
		$this->attributes['entity']['billing_address']['country'] = $country;
	}

	public function setPostUrl( $url )
	{
		$this->attributes['post']['url'] = $url;
	}

	public function setMetaData( array $meta )
	{
		$this->attributes['metadata'] = $meta;
	}

	public function setRawAttributes( array $attributes )
	{
		$this->attributes = $attributes;
	}

	public function getData()
	{
		return $this->attributes;
	}

	protected function setMethod( $method )
	{
		$this->method = $method;
	}


	/**
	 * @param $rules
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function validateAttributes( array $rules, $messages = [] )
	{
		$validator = Validator::make( $this->attributes, $rules, $messages );

		if ( $validator->fails() )
			throw new \Exception( $validator->errors()->first() );

		return true;
	}


	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	protected function send()
	{
		$client = new Client([
			'headers' => ['Authorization' => 'Bearer ' . config( 'tap-payment.auth.business_api_key' )],
			'Accept' => 'application/json',
		]);

		try {
			$response = $client->request($this->method, $this->getPath(), ['json' => $this->attributes]);

			return json_decode( $response->getBody()->getContents(), true );
		}
		catch ( \Throwable $exception ) {
			throw new \Exception($exception->getResponse()->getBody()->getContents());
		}
	}


	/**
	 * @return Invoice
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function create()
	{
		$rules = [
			'name.en' => 'required',
			'entity.legal_name.en' => 'required',
			'entity.license.number' => 'required',
			'entity.tax_number'    => 'nullable',
			'entity.documents.*.number' => 'required',
			'entity.documents.*.issuing_date' => 'required',
			'entity.documents.*.expiry_date' => 'required',
			'entity.documents.*.files' => 'required',
			'entity.bank_account.iban' => 'required',
			'entity.bank_account.swift_code' => 'required',
			'entity.bank_account.account_number' => 'required',
			'entity.billing_address.recipient_name' => 'required',
			'entity.billing_address.address_1' => 'required',
			'entity.billing_address.city' => 'required',
			'entity.billing_address.district' => 'required',
			'entity.billing_address.state' => 'required',
			'entity.billing_address.zip_code' => 'required',
			'entity.billing_address.country' => 'required',
			'contact_person.name.first' => 'required',
			'contact_person.name.last' => 'required',
			'contact_person.contact_info.primary.email' => 'required',
			'contact_person.contact_info.primary.phone.country_code' => 'required',
			'contact_person.contact_info.primary.phone.number' => 'required',
		];

		if ($this->validateAttributes($rules))
		
		return $this->send();
	}
}