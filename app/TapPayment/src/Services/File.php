<?php

namespace VMdevelopment\TapPayment\Services;

use Illuminate\Support\Facades\Validator;
use VMdevelopment\TapPayment\Abstracts\AbstractService;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class File extends AbstractService
{
	protected $endpoint = 'files/';
	protected $method = 'post';
	protected $attributes = [
		[
			'name' => 'file',
			'contents' => '',
			'filename' => ''
		],
		[
			'name' => 'purpose',
			'contents' => '',
		],
		[
			'name' => 'title',
			'contents' => '',
		],
		[
			'name' => 'expires_at',
			'contents' => '1913743462',
		],
		[
			'name' => 'file_link_create',
			'contents' => true,
		],
	];

	public function __construct()
	{
		parent::__construct();
	}


	protected function setEndpoint( $endpoint )
	{
		$this->endpoint .= $endpoint;
	}


	public function setFile( $file )
	{
		$filenameWithExt = $file->getClientOriginalName();
		$this->addValueToAttributes('file', $file->getContent());
		$this->addValueToAttributes('file', $filenameWithExt, 'filename');
	}

	public function setPurpose( $purpose )
	{
		$this->addValueToAttributes('purpose', $purpose);
	}

	public function setTitle( $title )
	{
		$this->addValueToAttributes('title', $title);
	}

	public function setPostUrl( $url )
	{
		$this->attributes['post']['url'] = $url;
	}

	public function setMetaData( array $meta )
	{
		$this->attributes['metadata'] = $meta;
	}


	public function setRawAttributes( array $attributes )
	{
		$this->attributes = $attributes;
	}

	protected function setMethod( $method )
	{
		$this->method = $method;
	}

	private function addValueToAttributes($keyVal, $value, $valueKey = 'contents'){
		foreach ($this->attributes as $key => $attribute) {
			if($attribute['name'] == $keyVal){
				$this->attributes[$key][$valueKey] = $value;
				return;
			}
		}
	}

	private function getAttrValue($keyVal){
		foreach ($this->attributes as $key => $attribute) {
			if($attribute['name'] == $keyVal){
				return $attribute['contents'];
			}
		}
	}

	/**
	 * @param $rules
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function validateAttributes( array $rules, $messages = [] )
	{
		$validator = Validator::make( $this->attributes, $rules, $messages );

		if ( $validator->fails() )
			throw new \Exception( $validator->errors()->first() );

		return true;
	}


	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	protected function send()
	{
		$client = new Client([
			'headers' => ['Authorization' => 'Bearer ' . config( 'tap-payment.auth.business_api_key' )],
			'Accept' => '*/*',
            'Content-Type' => 'multipart/form-data',
		]);

		try {
			$response = $client->request($this->method, $this->getPath(), ['multipart' => $this->attributes]);

			return json_decode( $response->getBody()->getContents(), true );
		}
		catch ( \Throwable $exception ) {
			throw new \Exception( $exception->getMessage() );
		}
	}


	/**
	 * @return Invoice
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function create()
	{
		$rules = [
			
		];

		if ($this->validateAttributes($rules))
		
		return $this->send();
	}
}