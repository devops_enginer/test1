<?php

namespace VMdevelopment\TapPayment\Services;

use Illuminate\Support\Facades\Validator;
use VMdevelopment\TapPayment\Abstracts\AbstractService;
use Propaganistas\LaravelPhone\PhoneNumber;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Authorize extends AbstractService
{
	protected $endpoint = 'authorize/';
	protected $method = 'post';
	protected $attributes = [
		'amount' => 1,
	    'currency' => '',
	    'save_card' => true,
	    'description' => 'The Health Lab Subscription',
	    'statement_descriptor' => 'The Health Lab Subscription',
	    'metadata' => [],
	    'receipt' => [
	        'email' => true,
	        'sms' => true
	    ],
	    'customer' => [
	        'first_name' => '',
	        'middle_name' => '',
	        'last_name' => '',
	        'email' => '',
	        'phone' => [
	            'country_code' => '',
	            'number' => ''
	        ]
	    ],
	    'source' => [
	        'id' => 'src_card'
	    ],
	    'auto' => [
	        'type' => 'CAPTURE',
	        'time' => 100
	    ],
	    'post' => [
	        'url' => ''
	    ],
	    'redirect' => [
	        'url' => ''
	    ]
	];


	public function __construct( $id = null )
	{
		if ( $id ) {
			$this->attributes['id'] = $id;
			$this->setEndpoint( $id );
		}
		parent::__construct();
	}


	protected function setEndpoint( $endpoint )
	{
		$this->endpoint .= $endpoint;
	}

	public function setAmount( $amount )
	{
		$this->attributes['amount'] = $amount;
	}

	public function setCurrency( $currency )
	{
		$this->attributes['currency'] = $currency;
	}

	public function setDescription( $description )
	{
		$this->attributes['description'] = $description;
		$this->attributes['statement_descriptor'] = $description;
	}

	public function setName( $name )
	{
		$nameArr = explode(' ', $name);
		$firstName = $nameArr[0] ?? '';
		$middleName = $nameArr[1] ?? '';
		$lastName = $nameArr[2] ?? ($nameArr[1] ?? '');

		$this->attributes['customer']['first_name'] = $firstName;
		$this->attributes['customer']['middle_name'] = $middleName;
		$this->attributes['customer']['last_name'] = $lastName;
	}

	public function setEmail( $email )
	{
		$this->attributes['customer']['email'] = $email;
	}

	public function setCustomerPhone($phone)
	{
		$phoneNumber =  PhoneNumber::make($phone)->getPhoneNumberInstance();

		$nNumber = $phoneNumber->getNationalNumber();
		$cCode = '+' . $phoneNumber->getCountryCode();

		$this->attributes['customer']['phone']['country_code'] = $cCode;
		$this->attributes['customer']['phone']['number'] = $nNumber;
	}

	public function setReturnUrl( $url )
	{
		$this->attributes['redirect']['url'] = $url;
	}

	public function setPostUrl( $url )
	{
		$this->attributes['post']['url'] = $url;
	}

	public function setMetaData( array $meta )
	{
		$this->attributes['metadata'] = $meta;
	}

	public function setRawAttributes( array $attributes )
	{
		$this->attributes = $attributes;
	}

	public function getData()
	{
		return $this->attributes;
	}

	protected function setMethod( $method )
	{
		$this->method = $method;
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function find()
	{
		$this->setMethod( 'get' );
		if ($this->validateAttributes(['id' => 'required']))
			return $this->send();
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function void()
	{
		$this->setEndpoint('/void');
		$this->setMethod( 'post' );
		if ($this->validateAttributes(['id' => 'required']))
			return $this->send();
	}

	/**
	 * @param $rules
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function validateAttributes( array $rules, $messages = [] )
	{
		$validator = Validator::make( $this->attributes, $rules, $messages );

		if ( $validator->fails() )
			throw new \Exception( $validator->errors()->first() );

		return true;
	}


	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	protected function send()
	{
		$client = new Client([
			'headers' => ['Authorization' => 'Bearer ' . config( 'tap-payment.auth.api_key' )],
			'Accept' => 'application/json',
		]);

		try {
			$response = $client->request($this->method, $this->getPath(), ['json' => $this->attributes]);

			return json_decode( $response->getBody()->getContents(), true );
		}
		catch ( \Throwable $exception ) {
			throw new \Exception($exception->getResponse()->getBody()->getContents());
		}
	}


	/**
	 * @return Invoice
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function create()
	{
		$rules = [
			'amount' => 'required',
			'currency' => 'required',
			'description' => 'required',
			'statement_descriptor' => 'required',
			'customer.first_name' => 'required',
			'customer.email' => 'required',
			'customer.phone.country_code' => 'required',
			'customer.phone.number' => 'required',
		];

		if ($this->validateAttributes($rules))
		
		return $this->send();
	}
}