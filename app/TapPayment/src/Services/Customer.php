<?php

namespace VMdevelopment\TapPayment\Services;

use Illuminate\Support\Facades\Validator;
use VMdevelopment\TapPayment\Abstracts\AbstractService;
use Propaganistas\LaravelPhone\PhoneNumber;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

use Carbon\Carbon;

class Customer extends AbstractService
{
	protected $endpoint = 'customers/';
	protected $method = 'post';
	protected $attributes = [];


	public function __construct( $id = null )
	{
		if ( $id ) {
			$this->attributes['id'] = $id;
			$this->setEndpoint( $id );
		}
		parent::__construct();
	}


	protected function setEndpoint( $endpoint )
	{
		$this->endpoint .= $endpoint;
	}

	public function setName( $fullName )
	{
		$nameArr = explode(' ', $fullName);
		$firstName = $nameArr[0] ?? '';
		$middleName = $nameArr[1] ?? '';
		$lastName = $nameArr[2] ?? ($nameArr[1] ?? '');

		$this->attributes['first_name'] = $firstName;
		$this->attributes['middle_name'] = $middleName;
		$this->attributes['last_name'] = $lastName;
	}

	public function setEmail( $email )
	{
		$this->attributes['email'] = $email;
	}

	public function setDescription( $description )
	{
		$this->attributes['description'] = $description;
	}

	public function setCurrency( $currency )
	{
		$this->attributes['currency'] = $currency;
	}

	public function setPhoneNumber($phone)
	{
		$phoneNumber =  PhoneNumber::make($phone)->getPhoneNumberInstance();

		$nNumber = $phoneNumber->getNationalNumber();
		$cCode = '+' . $phoneNumber->getCountryCode();

		$this->attributes['phone']['country_code'] = $cCode;
		$this->attributes['phone']['number'] = $nNumber;
	}

	public function setMetaData( array $meta )
	{
		$this->attributes['metadata'] = $meta;
	}

	public function setRawAttributes( array $attributes )
	{
		$this->attributes = $attributes;
	}

	public function getData()
	{
		return $this->attributes;
	}

	protected function setMethod( $method )
	{
		$this->method = $method;
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function find()
	{
		$this->setMethod( 'get' );
		if ($this->validateAttributes(['id' => 'required']))
			return $this->send();
	}

	/**
	 * @param $rules
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function validateAttributes( array $rules, $messages = [] )
	{
		$validator = Validator::make( $this->attributes, $rules, $messages );

		if ( $validator->fails() )
			throw new \Exception( $validator->errors()->first() );

		return true;
	}


	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	protected function send()
	{
		$client = new Client([
			'headers' => ['Authorization' => 'Bearer ' . config( 'tap-payment.auth.api_key' )],
			'Accept' => 'application/json',
		]);

		try {
			$response = $client->request($this->method, $this->getPath(), ['json' => $this->attributes]);

			return json_decode( $response->getBody()->getContents(), true );
		}
		catch ( \Throwable $exception ) {
			throw new \Exception($exception->getResponse()->getBody()->getContents());
		}
	}


	/**
	 * @return Invoice
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function create()
	{
		$rules = [
			'first_name' => 'required',
			'middle_name' => 'nullable',
			'last_name' => 'required',
			'email' => 'required',
			'phone.country_code' => 'required',
			'phone.number' => 'required',
			'description' => 'nullable',
			'currency' => 'nullable',
		];

		if ($this->validateAttributes($rules))
		
		return $this->send();
	}
}