<?php

namespace VMdevelopment\TapPayment\Services;

use Illuminate\Support\Facades\Validator;
use VMdevelopment\TapPayment\Abstracts\AbstractService;
use VMdevelopment\TapPayment\Resources\Invoice;

class Token extends AbstractService
{
	protected $endpoint = 'tokens/';
	protected $method = 'post';
	protected $attributes = [];


	public function __construct( $id = null )
	{
		if ( $id ) {
			$this->attributes['id'] = $id;
			$this->setEndpoint( $id );
		}
		parent::__construct();
	}


	protected function setEndpoint( $endpoint )
	{
		$this->endpoint .= $endpoint;
	}


	public function setToken( $token )
	{
		$this->attributes['source'] = $token;
	}

	public function setName( $name )
	{
		$this->attributes['card']['name'] = $name;
	}

	public function setNumber( $number )
	{
		$this->attributes['card']['number'] = $number;
	}

	public function setExpMonth( $month )
	{
		$this->attributes['card']['exp_month'] = $month;
	}

	public function setExpYear( $year )
	{
		$this->attributes['card']['exp_year'] = $year;
	}

	public function setCVC( $cvc )
	{
		$this->attributes['card']['cvc'] = $cvc;
	}

	public function setSavedCardId( $id )
	{
		$this->attributes['saved_card']['card_id'] = $id;
	}

	public function setSavedCardCustomerId( $id )
	{
		$this->attributes['saved_card']['customer_id'] = $id;
	}

	public function setRedirectUrl( $url )
	{
		$this->attributes['redirect']['url'] = $url;
	}


	public function setPostUrl( $url )
	{
		$this->attributes['post']['url'] = $url;
	}


	public function setSource( $source )
	{
		$this->attributes['source']['id'] = $source;
	}


	public function setMetaData( array $meta )
	{
		$this->attributes['metadata'] = $meta;
	}


	public function setRawAttributes( array $attributes )
	{
		$this->attributes = $attributes;
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function create()
	{
		$rules = [
			'card.name' => 'required',
			'card.number' => 'required',
			'card.exp_year' => 'required',
			'card.exp_month' => 'required',
			'card.cvc' => 'required',
		];

		if ($this->validateAttributes($rules))
			return $this->send();
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function find()
	{
		$this->setMethod( 'get' );
		if ($this->validateAttributes(['id' => 'required']))
			return $this->send();
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function createFromSavedCard($customerId, $cardId)
	{
		$this->setMethod( 'post' );
		$this->setSavedCardId($cardId);
		$this->setSavedCardCustomerId($customerId);
		return $this->send();
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function save()
	{
		$this->setMethod( 'post' );
		if ($this->validateAttributes(['id' => 'required']))
			return $this->send();
	}

	protected function setMethod( $method )
	{
		$this->method = $method;
	}

	/**
	 * @param $rules
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function validateAttributes( array $rules, $messages = [] )
	{
		$validator = Validator::make( $this->attributes, $rules, $messages );

		if ( $validator->fails() )
			throw new \Exception( $validator->errors()->first() );

		return true;
	}


	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	protected function send()
	{
		try {
			$response = $this->client->request(
				$this->method,
				$this->getPath(),
				[
					'form_params' => $this->attributes,
					'headers'     => [
						'Authorization' => 'Bearer ' . config( 'tap-payment.auth.api_key' ),
						'Accept'        => 'application/json',
					]
				]
			);

			return new Invoice( json_decode( $response->getBody()->getContents(), true ) );
		}
		catch ( \Throwable $exception ) {
			throw new \Exception( $exception->getMessage() );
		}
	}


	/**
	 * @return Invoice
	 * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
	 */
	public function pay()
	{
		$rules = [
			'token'         => 'required',
		];
		
		if ($this->validateAttributes($rules))
			return $this->send();
	}
}