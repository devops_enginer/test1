<?php

namespace VMdevelopment\TapPayment;

use VMdevelopment\TapPayment\Services\Charge;
use VMdevelopment\TapPayment\Services\Business;
use VMdevelopment\TapPayment\Services\File;
use VMdevelopment\TapPayment\Services\Authorize;
use VMdevelopment\TapPayment\Services\Subscription;
use VMdevelopment\TapPayment\Services\Token;
use VMdevelopment\TapPayment\Services\Card;
use VMdevelopment\TapPayment\Services\Customer;

class TapService
{
	/**
	 * @return \VMdevelopment\TapPayment\Services\Charge
	 */
	public static function createCharge()
	{
		return new Charge();
	}


	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function findCharge( $id )
	{
		$charge = new Charge( $id );

		return $charge->find();
	}

	/**
	 * @return \VMdevelopment\TapPayment\Services\Business
	 */
	public static function createBusiness()
	{
		return new Business();
	}

	/**
	 * @return \VMdevelopment\TapPayment\Services\File
	 */
	public static function createFile()
	{
		return new File();
	}

	/**
	 * @return \VMdevelopment\TapPayment\Services\Authorize
	 */
	public static function createAuthorize()
	{
		return new Authorize();
	}

	/**
	 * @return \VMdevelopment\TapPayment\Services\Token
	 */
	public static function createToken()
	{
		return new Token();
	}

	/**
	 * @return \VMdevelopment\TapPayment\Services\Card
	 */
	public static function createCard()
	{
		return new Card();
	}

	/**
	 * @return \VMdevelopment\TapPayment\Services\Card
	 */
	public static function saveCard($customerId, $token)
	{
		$card = new Card($customerId);
		return $card->saveCard($token);
	}

	/**
	 * @return \VMdevelopment\TapPayment\Services\Customer
	 */
	public static function createCustomer()
	{
		return new Customer();
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function findToken($id)
	{
		$token = new Token( $id );

		return $token->find();
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function createFromSavedCard($customerId, $cardId)
	{
		$token = new Token();

		return $token->createFromSavedCard($customerId, $cardId);
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function findAuthorize( $id )
	{
		$authorize = new Authorize( $id );

		return $authorize->find();
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function voidAuthorize( $id )
	{
		$authorize = new Authorize( $id );

		return $authorize->void();
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function saveToken( $customerId, $token )
	{
		$token = new Token( $customerId );

		$token->setToken($token);

		return $token->save();
	}

	/**
	 * @return \VMdevelopment\TapPayment\Services\Subscription
	 */
	public static function createSubscription()
	{
		return new Subscription();
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function findSubscription( $id )
	{
		$subscription = new Subscription( $id );

		return $subscription->find();
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function cancelSubscription( $id )
	{
		$subscription = new Subscription( $id );

		return $subscription->cancel();
	}
}