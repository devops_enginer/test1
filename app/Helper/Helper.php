<?php

namespace App\Helper;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\PendingBookingPayment;

class Helper{

    public static function createSuccessResponse($data = null, $message = null, $isLogin = false)
    {
        $response = ['isSuccessful' => true];
        $response['hasContent'] = $data ? true:false;
        $response['code'] = 200;
        $response['message'] = $message;
        $response['detailed_error'] = null;
        $response['data'] = $data;

        return $response;
    }

    public static function createErrorResponse($message, $detailed_error = null, $error_code = 400, $isLogin = false){
        $response = ['isSuccessful'=>false];
        $response['code'] = $error_code;
        $response['hasContent'] = false;
        $response['message'] = $message;
        $response['detailed_error'] = $detailed_error;
        $response['data'] = null;
        return $response;
    }

    public static function getUserAuthToken(){
        $userToken = str_replace('Bearer ', '', request()->header('Authorization'));
        return $userToken;
    }

    public static function getUserFromToken($token){
        [$id, $userToken] = explode('|', $token, 2);
        $tokenData = DB::table('personal_access_tokens')->where('token', hash('sha256', $userToken))->first();
        if($tokenData == null) abort(404);
        $userId = $tokenData->tokenable_id;
        $user = User::findOrFail($userId);
        return $user;
    }

    public static function getStripePrice($price){
        $price = number_format($price / 3.65, 2) * 100;
        return $price;
    }

    public static function storeBookingInPendingPayment($appointmentTimeId, $userId){
        PendingBookingPayment::where('user_id', $userId)->delete();

        PendingBookingPayment::updateOrCreate([
            'user_id' => $userId,
            'appointment_time_id' => $appointmentTimeId,
        ], [
            'user_id' => $userId,
            'appointment_time_id' => $appointmentTimeId,
            'payment_status' => UNPAID_BOOKING_PAYMENT_STATUS,
        ]);
    }

    public static function makePendingBookingPaid($userId){
        PendingBookingPayment::where([['user_id', $userId], ['payment_status', UNPAID_BOOKING_PAYMENT_STATUS]])->update(['payment_status' => PAID_BOOKING_PAYMENT_STATUS]);
    }

    public static function getPendingBookingPaymentStatus($userId, $appointmentTimeId){
        $pendingBookingPayment = PendingBookingPayment::where([['user_id', $userId], ['appointment_time_id', $appointmentTimeId]])->first();

        $status = $pendingBookingPayment->payment_status ?? UNPAID_BOOKING_PAYMENT_STATUS;
        if($pendingBookingPayment) $pendingBookingPayment->delete();
        return $status;
    }
}