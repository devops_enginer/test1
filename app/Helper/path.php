<?php

const BANNER_IMG_PATH = 'uploaded_files/images/promotional_banner';
const SERVICE_ICON_PATH = 'uploaded_files/images/service';
const COUNTRY_FLAG_PATH = 'uploaded_files/images/country';
const TEST_IMG_PATH = 'uploaded_files/images/test';
const TEST_THUMBNAIL_PATH = 'uploaded_files/images/test/thumbnail';
const CLINIC_IMG_PATH = 'uploaded_files/images/clinic';
const DOCTOR_CATEGORY_ICON_PATH = 'uploaded_files/images/doctor_category';
const DOCTOR_IMG_PATH = 'uploaded_files/images/doctor';
const DOCTOR_BOOKING_PRESCRIPTION_PATH = 'uploaded_files/files/doctor/prescription';
const RESULT_FILE_PATH = 'uploaded_files/file/result';

const USER_IMG_PATH = 'uploaded_files/images/users';