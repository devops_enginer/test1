<?php

// Booking
const PENDING_BOOKING_STATUS = 'booked';
const COMPLETED_BOOKING_STATUS = 'completed';
const CANCELED_BOOKING_STATUS = 'canceled';

const PAID_BOOKING_PAYMENT_STATUS = 'paid';
const UNPAID_BOOKING_PAYMENT_STATUS = 'unpaid';

// Promo Code
const FIXED_CODE_TYPE = 'fixed_amount';
const PERCENTAGE_CODE_TYPE = 'percentage';
