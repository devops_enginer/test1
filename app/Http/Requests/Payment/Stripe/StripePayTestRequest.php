<?php

namespace App\Http\Requests\Payment\Stripe;

use Illuminate\Foundation\Http\FormRequest;
use App\Exceptions\RequestException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;

class StripePayTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = Auth::id();
        
        return [
            'test_id' => 'required|integer|exists:tests,id',
            'clinic_id' => 'required|integer|exists:clinics,id',
            'payment_method' => 'required|string|exists:stripe_payment_methods,method_id,user_id,' . $userId,
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new RequestException($validator->errors()->first(), null, 422);
    }
}
