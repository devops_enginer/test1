<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Exceptions\RequestException;
use Illuminate\Contracts\Validation\Validator;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $email = 'required|email';
        if($this->account_type == 'normal'){
            $email .= '|unique:users,email,null,id,account_type,normal';
        }

        return [
            'full_name' => 'string|required',
            'email' => $email,
            'phone_number' => 'nullable|string|unique:users,phone_number',
            'password' => 'required_without:token|string',
            'address_line_1' => 'required|string',
            'address_line_2' => 'required|string',
            'language' => 'required|string|in:en,ar',
            'city_id' => 'required|integer|exists:cities,id',
            'country_id' => 'required|integer|exists:countries,id',

            'account_type' => 'required|string|in:normal,facebook,google,apple',
            'token' => 'nullable|string',
            'fcm_token' => 'nullable|string',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new RequestException($validator->errors()->first(), null, 422);
    }
}
