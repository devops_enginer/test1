<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Exceptions\RequestException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = Auth::id();

        return [
            'full_name' => 'nullable|string',
            'email' => 'nullable|email|unique:users,email,' . $userId . ',id,account_type,normal',
            'phone_number' => 'nullable|string|unique:users,phone_number,' . $userId . ',id',
            'address_line_1' => 'nullable|string',
            'address_line_2' => 'nullable|string',
            'language' => 'nullable|string|in:en,ar',
            'city_id' => 'nullable|integer|exists:cities,id',
            'country_id' => 'nullable|integer|exists:countries,id',

            'photo' => 'nullable|image',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new RequestException($validator->errors()->first(), null, 422);
    }
}
