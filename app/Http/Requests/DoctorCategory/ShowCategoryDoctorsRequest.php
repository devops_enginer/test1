<?php

namespace App\Http\Requests\DoctorCategory;

use Illuminate\Foundation\Http\FormRequest;
use App\Exceptions\RequestException;
use Illuminate\Contracts\Validation\Validator;

class ShowCategoryDoctorsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'nullable|integer|exists:doctor_categories,id',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new RequestException($validator->errors()->first(), null, 422);
    }
}
