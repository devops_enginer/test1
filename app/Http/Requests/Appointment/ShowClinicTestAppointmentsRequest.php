<?php

namespace App\Http\Requests\Appointment;

use Illuminate\Foundation\Http\FormRequest;
use App\Exceptions\RequestException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;

class ShowClinicTestAppointmentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'test_id' => 'required|integer|exists:tests,id',
            'clinic_id' => 'required|integer|exists:clinics,id',
            'date' => 'required|date_format:Y-m-d',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new RequestException($validator->errors()->first(), null, 422);
    }
}
