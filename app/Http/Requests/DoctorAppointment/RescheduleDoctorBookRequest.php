<?php

namespace App\Http\Requests\DoctorAppointment;

use Illuminate\Foundation\Http\FormRequest;
use App\Exceptions\RequestException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;

class RescheduleDoctorBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'book_id' => 'required|integer|exists:doctor_bookings,id,user_id,' . Auth::id(),
            'new_time_id' => 'required|integer|exists:doctor_appointment_times,id,user_id,NULL',
            'reason_id' => 'required|integer|exists:reasons,id',
            'comment' => 'nullable|string'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new RequestException($validator->errors()->first(), null, 422);
    }
}
