<?php

namespace App\Http\Requests\Dashboard\Test;

use App\Exceptions\GeneralException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CreateTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'image' => ['required', 'image'],
            'thumbnail' => ['required', 'image'],
            'title' => ['required', 'string'],
            'description' => ['required', 'string'],
            'is_featured' => ['nullable', 'string'],
            'service_id' => ['required', 'integer', 'exists:services,id'],

            'clinics' => ['nullable', 'array'],
            'clinics.*' => ['required', 'integer', 'exists:clinics,id'],

            'prices' => ['required_with:clinics', 'nullable', 'array'],
            'prices.*.clinic_id' => ['required', 'integer', 'exists:clinics,id'],
            'prices.*.sale_price' => ['required', 'numeric'],
            'prices.*.test_price' => ['required', 'numeric'],
        ];
    }


    public function failedValidation(Validator $validator)
    {
        throw new GeneralException($validator->errors()->first());
    }
}
