<?php

namespace App\Http\Requests\Dashboard\User;

use App\Exceptions\GeneralException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'full_name' => ['required','string'],
            'email' => ['required','email'],
            'password' => ['required','string','min:6','confirmed'],
            'phone_number' => ['required','string'],
            'photo' => ['nullable','image'],
            'account_type' => ['nullable', 'string', 'in:normal'],
            'address_line_1' => ['nullable', 'string'],
            'address_line_2' => ['nullable', 'string'],
            'language' => ['required', 'string'],
            'city_id' => ['required', 'integer', Rule::exists('cities','id')],
            'country_id' => ['required', 'integer', Rule::exists('countries','id')],
        ];
    }


    public function failedValidation(Validator $validator)
    {
        throw new GeneralException($validator->errors()->first());
    }
}
