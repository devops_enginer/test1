<?php

namespace App\Http\Requests\Dashboard\User;

use App\Exceptions\GeneralException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'full_name' => ['nullable','string'],
            'email' => ['nullable','email'],
            'password' => ['nullable','string','min:6','confirmed'],
            'phone_number' => ['nullable','string'],
            'photo' => ['nullable','image'],
            'account_type' => ['nullable', 'string', 'in:normal'],
            'address_line_1' => ['nullable', 'string'],
            'address_line_2' => ['nullable', 'string'],
            'language' => ['nullable', 'string'],
            'city_id' => ['nullable', 'integer', Rule::exists('cities','id')],
            'country_id' => ['nullable', 'integer', Rule::exists('countries','id')],
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new GeneralException($validator->errors()->first());
    }
}
