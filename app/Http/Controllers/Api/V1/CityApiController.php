<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use Exception;

use App\Models\City;

class CityApiController extends Controller
{
    public function index()
    {
        $cities = City::with('country')->get();

        $response = Helper::createSuccessResponse($cities);
        return response()->json($response);
    }
}
