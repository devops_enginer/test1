<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use App\Jobs\ResetPasswordJob;
use Exception;
use App\Helper\Helper;
use Socialite;

use App\Http\Requests\User\LoginUserRequest;
use App\Http\Requests\User\RegisterUserRequest;
use App\Http\Requests\User\ChangePasswordRequest;
use App\Http\Requests\User\ResetPasswordRequest;
use App\Http\Requests\User\UserEmailRequest;
use App\Http\Requests\User\SocialSigninRequest;

use App\Models\User;
use App\Models\DeviceToken;

class AuthApiController extends Controller
{
    public function login(LoginUserRequest $loginUserRequest)
    {
        $request = $loginUserRequest->validated();
        $message = 'Incorrect Information';
        $status = false;

        $user = User::where('account_type', 'normal')
                    ->when(isset($request['email']), function($query) use ($request){
                        return $query->where('email', $request['email']);
                    })
                    ->when(isset($request['phone_number']), function($query) use ($request){
                        return $query->where('phone_number', $request['phone_number']);
                    })
                    ->first();

        if($user){
            if(Hash::check($request['password'], $user->password)){
                $data = $user;
                $status = true;
                $message = 'Correct Information';
            }
            else $message = 'Wrong Password';
        }

        if($status === false){
            $response = Helper::createErrorResponse($message, null, 400);
            return response()->json($response);
        }

        if($loginUserRequest->fcm_token){
            $this->storeFcmToken($user->id, $loginUserRequest->fcm_token);
        }

        $user['token'] = $this->generateToken($user);
        $data = $user;

        $response = Helper::createSuccessResponse($data);
        return response()->json($response);
    }

    public function register(RegisterUserRequest $registerUserRequest)
    {
        $requestData = $registerUserRequest->validated();

        if($requestData['account_type'] != 'normal' && isset($requestData['token']) || !isset($requestData['password'])){
            return $this->socialSignIn($requestData);
        }

        $requestData['password'] = Hash::make($requestData['password']);
        $requestData['account_type'] = 'normal';

        $user = User::create($requestData);

        if($registerUserRequest->fcm_token){
            $this->storeFcmToken($user->id, $registerUserRequest->fcm_token);
        }

        $user['token'] = $this->generateToken($user);
        $data = $user;

        $response = Helper::createSuccessResponse($data);
        return response()->json($response);
    }

    public function socialLogin(SocialSigninRequest $socialSigninRequest){
        return $this->socialSignIn($socialSigninRequest->validated());
    }

    public function changePassword(ChangePasswordRequest $changePasswordRequest){
        $userId = Auth::id();
        $data = $changePasswordRequest->validated();

        $data['password'] = Hash::make($data['new_password']);

        $user = User::find($userId);
        $result = $user->update($data);

        $response = Helper::createSuccessResponse($result);
        return response()->json($response);
    }

    public function resetPasswordRequest(UserEmailRequest $userEamilRequest){
        $requestData = $userEamilRequest->validated();
        $token = Str::random(5);
        $user = User::where([['email', $requestData['email']], ['account_type', 'normal']])->first();

        $user->update([
            'reset_token' => $token,
        ]);

        $mailData = [
            'token' => $token,
            'email' => $requestData['email'],
        ];
        dispatch(new ResetPasswordJob($mailData));

        $response = Helper::createSuccessResponse('Email has been sent');
        return response()->json($response);
    }

    public function resetPassword(ResetPasswordRequest $resetPasswordRequest){
        $requestData = $resetPasswordRequest->validated();
        $token = $requestData['token'];
        $user = User::where('reset_token', $token)->first();
        $password = Hash::make($requestData['password']);

        $user->update(['password' => $password]);

        $response = Helper::createSuccessResponse('Password has been changed');
        return response()->json($response);
    }

    public function logout(){
        $user = Auth::user();
        $user->currentAccessToken()->delete();
        $response = Helper::createSuccessResponse('User has been successfully Logged Out');
        return response()->json($response);
    }

    private function socialSignIn($requestArray){
        $provider = $requestArray['account_type'];
        $requestProvider = $requestArray['account_type'];

        if($provider == 'apple') $provider = 'sign-in-with-apple';

        $providers = ['google', 'facebook', 'sign-in-with-apple'];
        if(!in_array($provider, $providers)){
            $response = Helper::createErrorResponse('Unsupported Provider', null, 400);
            return response()->json($response);
        }

        $providerKey = $requestArray['account_type'] . '_id';
        try{
            $driver = Socialite::driver($provider);
            $socialUserObject = $driver->userFromToken($requestArray['token']);
            $userInfo = $this->userDataFromSocialite($socialUserObject, $providerKey);
            $requestArray[$providerKey] = $userInfo['id'];
            $requestArray['password'] = $userInfo['password'];

            $user = $this->loginUserWithSocial($requestArray, $providerKey);

            if(isset($requestArray['fcm_token'])){
                $this->storeFcmToken($user->id, $requestArray['fcm_token']);
            }

            $user['token'] = $this->generateToken($user);
            $data = $user;

            $response = Helper::createSuccessResponse($data);
            return response()->json($response);
        }catch(Exception $e){
            $response = Helper::createErrorResponse('Incorrect Information', null, 400);
            return response()->json($response);
        }
    }

    private function loginUserWithSocial($userData, $providerKey){
        $user = User::updateOrCreate([
            $providerKey => $userData[$providerKey],
        ], $userData);

        return $user;
    }

    private function userDataFromSocialite($userSocial, $provider){
        return [
            'name' => $userSocial->getName() ?? strtoupper($provider) . ' User',
            'email' => $userSocial->getEmail() ?? null,
            'password' => Hash::make(Str::random(10)),
            'photo' => $userSocial->getAvatar() ?? null,
            'id' => $userSocial->getId() ?? null,
        ];
    }

    private function generateToken($user){
        $token = $user->createToken('authToken')->plainTextToken;
        return $token;
    }

    private function storeFcmToken($userId, $fcmToken){
        $badToken = DeviceToken::where('token', $fcmToken)->where('user_id', '<>', $userId)->delete();

        $deviceToken = DeviceToken::updateOrCreate([
            'token' => $fcmToken,
            'user_id' => $userId,
        ], [
            'token' => $fcmToken,
            'user_id' => $userId,
        ]);

        return $deviceToken;

        
    }
}
