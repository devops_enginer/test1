<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;

use App\Exceptions\RequestException;
use Exception;

use App\Http\Requests\Payment\Tap\AddNewTapPaymentMethodRequest;
use App\Http\Requests\Payment\Tap\TapPayTestRequest;
use App\Http\Requests\Payment\Tap\TapPayDoctorAppointmentRequest;
use App\Http\Requests\Payment\Tap\RemovePaymentMethodRequest;

use App\Models\TapPaymentMethod;
use App\Models\ClinicTest;
use App\Models\DoctorAppointmentTime;
use App\Models\Booking;

use App\Repositories\Frontend\TapChargeRepository;
use App\Repositories\Frontend\TapPaymentMethodRepository;

use VMdevelopment\TapPayment\TapService;

class TapPaymentApiController extends Controller
{
    private $tapChargeRepository;
    private $tapPaymentMethodRepository;

    public function __construct(TapChargeRepository $tapChargeRepository){
        $this->tapChargeRepository = $tapChargeRepository;
        $this->tapPaymentMethodRepository = new TapPaymentMethodRepository;
    }

    public function paymentMethods()
    {
        $user = Auth::user();
        $paymentMethods = TapPaymentMethod::where('user_id', $user->id)->get();

        $response = Helper::createSuccessResponse($paymentMethods);
        return response()->json($response);
    }

    public function newPaymentMethod(Request $request)
    {
        $user = Auth::user();
        $token = Helper::getUserAuthToken();
        $link = route('payment.tap.method', ['token' => $token]);
        $data = ['link' => $link];

        $response = Helper::createSuccessResponse($data);
        return response()->json($response);
    }

    public function removePaymentMethod(RemovePaymentMethodRequest $request)
    {
        $method = TapPaymentMethod::where('id', $request->payment_method)->delete();

        $response = Helper::createSuccessResponse(true, 'Deleted Successfully!');
        return response()->json($response);
    }

    public function payTest(TapPayTestRequest $request)
    {
        $clinicTest = ClinicTest::where([['test_id', $request->test_id], ['clinic_id', $request->clinic_id]])->with('clinic.country')->first();

        if($clinicTest == null){
            throw new RequestException('Clinic does not have such test', null, 422);
        }

        $user = Auth::user();
        Helper::storeBookingInPendingPayment($request->appointment_time_id, $user->id);

        return $this->pay($user, $clinicTest->test_price, 'Midical Test');
    }

    public function payDoctorAppointment(TapPayDoctorAppointmentRequest $request)
    {
        $appointmentTime = DoctorAppointmentTime::find($request->appointment_time_id);

        $user = Auth::user();
        return $this->pay($user, $appointmentTime->price, 'Midical Appointment');
    }

    public function payRedirect(Request $request){
        if(!$request->tap_id || !$request->token){
            return view('frontend.payment.tap.result', [
                'header' => 'Sorry!',
                'message' => 'Something went Wrong',
            ]);
        }

        $invoice = TapService::findCharge($request->tap_id); 
        $invoice = $invoice->getData(); 
        $code = $invoice['response']['code'];

        if($code != '000'){
            return view('frontend.payment.tap.result', [
                'header' => 'Sorry!',
                'message' => $invoice['response']['message'],
            ]);
        }

        $user = Helper::getUserFromToken($request->token);

        Helper::makePendingBookingPaid($user->id);

        return view('frontend.payment.tap.result', [
            'header' => 'Thank You',
            'message' => 'Payment Completed',
        ]);
    }

    private function payOld($user, $paymentMethod, $price, $description){
        try {
            $token = TapService::createFromSavedCard($paymentMethod->customer_id, $paymentMethod->card_id)->getData(); 

            $paymentMethod = $this->tapPaymentMethodRepository->update($paymentMethod, [
                'token' => $token['id']
            ]);

            $payment = TapService::createCharge(); 
            $payment->setCustomerName($paymentMethod->customer_name);
            $payment->setCustomerEmail($paymentMethod->customer_email);
            $payment->setCustomerPhone('971', $paymentMethod->customer_phone);
            $payment->setDescription($description);
            $payment->setCustomerId($paymentMethod->customer_id);
            $payment->setAmount($price);
            $payment->setCurrency($user->country->currency);
            $payment->setThreeDSecure(true);
            $payment->setSource($paymentMethod->token);
            $payment->setRedirectUrl(route('tap.payment.redirect'));

            $invoice = $payment->pay();
            $data = $invoice->getData();
            $data['user_id'] = $user->id;
            $data['payment_method_id'] = $paymentMethod->id;
            $this->tapChargeRepository->createFromTapResponse($data);

            $data = ['3ds_url' => ($data['transaction']['url'] ?? null)];

            $response = Helper::createSuccessResponse($data, '3DS Required');
            return response()->json($response);
        } catch (Exception $exception) {
            throw new RequestException($exception->getMessage(), null, 500);
        }
    }

    private function pay($user, $price, $description){
        $token = $user->createToken('authToken')->plainTextToken;

        try {
            $payment = TapService::createCharge(); 
            $payment->setCustomerName($user->full_name);
            $payment->setCustomerEmail($user->email);
            $payment->setCustomerPhone('971', $user->phone_number);
            $payment->setDescription($description);
            $payment->setAmount($price);
            $payment->setCurrency($user->country->currency);
            $payment->setThreeDSecure(true);
            $payment->setSource('src_all');
            $payment->setRedirectUrl(route('tap.payment.redirect', ['token' => $token]));

            $invoice = $payment->pay();
            $data = $invoice->getData();
            $data['user_id'] = $user->id;
            $this->tapChargeRepository->createFromTapResponse($data);

            $data = ['3ds_url' => ($data['transaction']['url'] ?? null)];

            $response = Helper::createSuccessResponse($data, '3DS Required');
            return response()->json($response);
        } catch (Exception $exception) {
            throw new RequestException($exception->getMessage(), null, 500);
        }
    }
}
