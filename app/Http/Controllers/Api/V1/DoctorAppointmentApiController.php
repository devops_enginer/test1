<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use Exception;

use App\Http\Requests\DoctorAppointment\ShowDoctorAppointmentsRequest;
use App\Http\Requests\DoctorAppointment\BookDoctorAppointmentRequest;
use App\Http\Requests\DoctorAppointment\CancelDoctorBookRequest;
use App\Http\Requests\DoctorAppointment\RescheduleDoctorBookRequest;

use App\Models\DoctorAppointment;
use App\Models\DoctorAppointmentTime;
use App\Models\DoctorBooking;

class DoctorAppointmentApiController extends Controller
{
    public function doctorAppointments(ShowDoctorAppointmentsRequest $request)
    {
        $appointments = DoctorAppointment::with('doctor', 'times')
            ->when($request->id, function($query) use ($request){
                return $query->where('doctor_id', $request->id);
            })
            ->when($request->date, function($query) use ($request){
                return $query->whereDate('date', $request->date);
            })->get();

        $response = Helper::createSuccessResponse($appointments);
        return response()->json($response);
    }

    public function bookAppointment(BookDoctorAppointmentRequest $request)
    {
        $userId = Auth::id();
        $time = DoctorAppointmentTime::with('appointment.doctor')->find($request->time_id);
        $time->update([
            'user_id' => $userId,
        ]);

        $booking = DoctorBooking::updateOrCreate([
            'user_id' => $userId,
            'doctor_appointment_time_id' => $request->time_id,
        ], [
            'status' => PENDING_BOOKING_STATUS,
            'user_id' => $userId,
            'doctor_appointment_time_id' => $request->time_id,
        ]);

        $response = Helper::createSuccessResponse($time);
        return response()->json($response);
    }

    public function cancelBooking(CancelDoctorBookRequest $request)
    {
        $userId = Auth::id();
        $booking = DoctorBooking::find($request->book_id);

        $booking->update([
            'status' => CANCELED_BOOKING_STATUS,
            'reason_id' => $request->reason_id,
            'comment' => $request->comment,
        ]);

        DoctorAppointmentTime::where('id', $booking->doctor_appointment_time_id)->update([
            'user_id' => null
        ]);

        $response = Helper::createSuccessResponse($booking);
        return response()->json($response);
    }

    public function rescheduleBooking(RescheduleDoctorBookRequest $request)
    {
        $userId = Auth::id();
        $booking = DoctorBooking::find($request->book_id);
        
        DoctorAppointmentTime::where('id', $booking->doctor_appointment_time_id)->update([
            'user_id' => null
        ]);

        $booking->update([
            'doctor_appointment_time_id' => $request->new_time_id,
            'reason_id' => $request->reason_id,
            'comment' => $request->comment,
            'is_rescheduled' => 1,
        ]);

        DoctorAppointmentTime::where('id', $booking->doctor_appointment_time_id)->update([
            'user_id' => $userId
        ]);
        
        $response = Helper::createSuccessResponse($booking);
        return response()->json($response);
    }
}
