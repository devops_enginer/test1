<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use Exception;

use App\Models\Reason;

class ReasonApiController extends Controller
{
    public function index()
    {
        $reasons = Reason::all();

        $response = Helper::createSuccessResponse($reasons);
        return response()->json($response);
    }
}
