<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use Exception;

use App\Models\PromotionalBanner;
use App\Models\Service;
use App\Models\Test;
use App\Models\ClinicTest;
use App\Models\Admin\StaticContent;

use App\Repositories\Dashboard\ClinicRepository;
use App\Repositories\Dashboard\StaticContentRepository;

class HomeApiController extends Controller
{
    public function home()
    {
        $banners = PromotionalBanner::orderBy('order', 'ASC')->where('test_id', null)->get();
        $services = Service::all();
        $defaultClinic = (new ClinicRepository)->getDefaultClinic();
        $testsIds = ClinicTest::where('clinic_id', $defaultClinic->id)->select('test_id')->pluck('test_id');
        $featuredTests = Test::whereIn('id', $testsIds)->where('is_featured', 1)->with('service', 'clinics', 'banners')->get();

        $data = [
            'banners' => $banners,
            'services' => $services,
            'tests' => $featuredTests,
        ];

        $response = Helper::createSuccessResponse($data);
        return response()->json($response);
    }

    public function staticContent(){
        $staticContent = StaticContent::orderBy('id', 'desc')->first();

        if($staticContent == null){
            $staticContent = (new StaticContentRepository())->initContent();
        }

        $response = Helper::createSuccessResponse($staticContent);
        return response()->json($response);
    }
}
