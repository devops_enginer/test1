<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use Exception;

use App\Http\Requests\Service\ShowServiceRequest;
use App\Http\Requests\Service\ShowServiceTestsRequest;

use App\Models\Service;
use App\Models\Test;

class ServiceApiController extends Controller
{
    public function index()
    {
        $services = Service::with('tests')->get();

        $response = Helper::createSuccessResponse($services->toArray());
        return response()->json($response);
    }

    public function categoryTests(ShowServiceTestsRequest $request)
    {
        $tests = Test::with('clinics', 'banners')->when($request->id, function($innerQuery) use ($request){
            return $innerQuery->where('service_id', $request->id);
        })->paginate(5);

        $response = Helper::createSuccessResponse($tests->toArray());
        return response()->json($response);
    }
}
