<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use Exception;

use App\Http\Requests\DoctorCategory\ShowCategoryDoctorsRequest;

use App\Models\DoctorCategory;
use App\Models\Doctor;

class DoctorCategoryApiController extends Controller
{
    public function index()
    {
        $categories = DoctorCategory::with('doctors')->get();

        $response = Helper::createSuccessResponse($categories->toArray());
        return response()->json($response);
    }

    public function categoryDoctors(ShowCategoryDoctorsRequest $request)
    {
        $doctors = Doctor::with('category')->when($request->id, function($innerQuery) use ($request){
            return $innerQuery->where('category_id', $request->id);
        })->paginate(5);

        $response = Helper::createSuccessResponse($doctors->toArray());
        return response()->json($response);
    }
}
