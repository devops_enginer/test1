<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use Exception;

use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\UserBookingsRequest;

use App\Models\User;
use App\Models\Notification;
use App\Models\Result;
use App\Models\AppointmentTime;
use App\Models\DoctorAppointmentTime;
use App\Models\Appointment;
use App\Models\DoctorAppointment;
use App\Models\Booking;
use App\Models\DoctorBooking;

use App\Traits\UploadFiles;

class UserApiController extends Controller
{
    use UploadFiles;

    public function profile()
    {
        $user = User::with('country', 'city', 'bookings')->find(Auth::id());

        $response = Helper::createSuccessResponse($user->toArray());
        return response()->json($response);
    }

    public function notifications()
    {
        $userId = Auth::id();
        $notifications = Notification::where('user_id', $userId)->with('test')->get();

        $response = Helper::createSuccessResponse($notifications->toArray());
        return response()->json($response);
    }

    public function results()
    {
        $userId = Auth::id();
        $appointmentTimes = Booking::where('user_id', $userId)->select('appointment_time_id')->pluck('appointment_time_id');
        
        $results = Result::whereIn('appointment_time_id', $appointmentTimes)->with('booking', 'appointmentTime.appointment.clinic', 'appointmentTime.appointment.test', 'parameters')->get();

        $response = Helper::createSuccessResponse($results->toArray());
        return response()->json($response);
    }

    public function bookings(UserBookingsRequest $request)
    {
        $userId = Auth::id();
        $bookings = Booking::where('user_id', $userId)
            ->with('appointmentTime.appointment.clinic','appointmentTime.appointment.test', 'reason', 'result')
            ->when($request->date, function($query) use ($request){
                $appointmentsIds = Appointment::where('date', $request->date)->select('id')->pluck('id');
                $appointmentTimesIds = AppointmentTime::whereIn('appointment_id', $appointmentsIds)->select('id')->pluck('id');

                return $query->whereIn('appointment_time_id', $appointmentTimesIds);
            })
            ->get();

        $response = Helper::createSuccessResponse($bookings->toArray());
        return response()->json($response);
    }

    public function doctorBookings(UserBookingsRequest $request)
    {
        $userId = Auth::id();
        $bookings = DoctorBooking::where('user_id', $userId)
            ->with('appointmentTime.appointment.doctor', 'reason')
            ->when($request->date, function($query) use ($request){
                $appointmentsIds = DoctorAppointment::where('date', $request->date)->select('id')->pluck('id');
                $appointmentTimesIds = DoctorAppointmentTime::whereIn('doctor_appointment_id', $appointmentsIds)->select('id')->pluck('id');

                return $query->whereIn('doctor_appointment_time_id', $appointmentTimesIds);
            })
            ->get();

        $response = Helper::createSuccessResponse($bookings->toArray());
        return response()->json($response);
    }

    public function update(UpdateUserRequest $updateUserRequest)
    {
        $userId = Auth::id();
        $data = $updateUserRequest->validated();

        if(isset($data['photo'])){
            $data['photo'] = $this->UploadFile($data['photo'], USER_IMG_PATH);
        }

        $data = array_filter($data);

        $user = User::where('id', $userId)->update($data);

        $user = User::with('country', 'city', 'bookings')->find($userId);

        $response = Helper::createSuccessResponse($user->toArray());
        return response()->json($response);
    }
}
