<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Helper\Helper;
use Exception;

use App\Repositories\Dashboard\ClinicRepository;

use App\Http\Requests\Clinic\ShowClinicsByLocationRequest;

use App\Models\Clinic;
use App\Models\ClinicTest;

class ClinicApiController extends Controller
{
    public function defaultClinic(){
        $clinic = (new ClinicRepository)->getDefaultClinic();
        $response = Helper::createSuccessResponse($clinic);
        return response()->json($response);
    }

    public function location(ShowClinicsByLocationRequest $request)
    {
        $lat = $request->input('lat');
        $lon = $request->input('lng');
        $user = Auth::user();
        $defaultClinic = (new ClinicRepository)->getDefaultClinic();

        $clinics = Clinic::where('city_id', $user->city_id)
                    ->where('id', '!=', $defaultClinic->id)
                    ->with('tests')
                    ->when($request->test_id, function($query) use ($request){
                        $clinicTestsIds = ClinicTest::where('test_id', $request->test_id)->pluck('clinic_id');
                        return $query->whereIn('id', $clinicTestsIds);
                    })
                    ->orderBy(DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                        * cos(radians(clinics.lat)) 
                        * cos(radians(clinics.lng) - radians(" . $lon . ")) 
                        + sin(radians(" .$lat. ")) 
                        * sin(radians(clinics.lat)))"), 'ASC')->get();

        $response = Helper::createSuccessResponse($clinics);
        return response()->json($response);
    }
}
