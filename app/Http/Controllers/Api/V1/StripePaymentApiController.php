<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;

use App\Exceptions\RequestException;
use Exception;

use App\Http\Requests\Payment\Stripe\StripePayTestRequest;
use App\Http\Requests\Payment\Stripe\StripePayDoctorAppointmentRequest;
use App\Http\Requests\Payment\Stripe\RemovePaymentMethodRequest;

use App\Models\StripePaymentMethod;
use App\Models\ClinicTest;
use App\Models\DoctorAppointmentTime;

use App\Repositories\Frontend\StripeChargeRepository;

class StripePaymentApiController extends Controller
{
    private $stripeChargeRepository;

    public function __construct(StripeChargeRepository $stripeChargeRepository){
        $this->stripeChargeRepository = $stripeChargeRepository;
    }

    public function paymentMethods()
    {
        $user = Auth::user();
        $paymentMethods = StripePaymentMethod::where('user_id', $user->id)->get();

        $response = Helper::createSuccessResponse($paymentMethods);
        return response()->json($response);
    }

    public function newPaymentMethod(Request $request)
    {
        $user = Auth::user();
        $token = Helper::getUserAuthToken();
        $url = route('payment.stripe.method', ['token' => $token]);
        $data = ['link' => $url];

        $response = Helper::createSuccessResponse($data);
        return response()->json($response);
    }

    public function removePaymentMethod(RemovePaymentMethodRequest $request)
    {
        $method = StripePaymentMethod::where('id', $request->payment_method_id)->delete();

        $response = Helper::createSuccessResponse(true, 'Deleted Successfully!');
        return response()->json($response);
    }

    public function payTest(StripePayTestRequest $request)
    {
        $clinicTest = ClinicTest::where([['test_id', $request->test_id], ['clinic_id', $request->clinic_id]])->with('clinic.country')->first();

        $paymentMethod = StripePaymentMethod::where('method_id', $request->payment_method)->select('id')->first();

        if($clinicTest == null){
            throw new RequestException('Clinic does not have such test', null, 422);
        }

        $user = Auth::user();
        try {
            $price = Helper::getStripePrice($clinicTest->test_price);
            $charge = $user->charge($price, $request->payment_method);
            $data = $charge->toArray();

            $data['payment_method_id'] = $paymentMethod->id;
            $data['user_id'] = $user->id;
            $stripeCharge = $this->stripeChargeRepository->createFromStripeResponse($data);

            $responseData = ['receipt_url' => $stripeCharge->receipt_url];
            $response = Helper::createSuccessResponse($responseData, 'Successfully Paid');
            return response()->json($response);
        } catch (Exception $exception) {
            throw new RequestException($exception->getMessage(), null, 500);
        }
    }

    public function payDoctorAppointment(StripePayDoctorAppointmentRequest $request)
    {
        $appointmentTime = DoctorAppointmentTime::find($request->appointment_time_id);

        $paymentMethod = StripePaymentMethod::where('method_id', $request->payment_method)->select('id')->first();

        $user = Auth::user();
        try {
            $price = Helper::getStripePrice($appointmentTime->price);
            $charge = $user->charge($price, $request->payment_method);
            $data = $charge->toArray();

            $data['payment_method_id'] = $paymentMethod->id;
            $data['user_id'] = $user->id;
            $stripeCharge = $this->stripeChargeRepository->createFromStripeResponse($data);

            $responseData = ['receipt_url' => $stripeCharge->receipt_url];
            $response = Helper::createSuccessResponse($responseData, 'Successfully Paid');
            return response()->json($response);
        } catch (Exception $exception) {
            throw new RequestException($exception->getMessage(), null, 500);
        }
    }
}
