<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use Exception;

use App\Models\Country;

class CountryApiController extends Controller
{
    public function index()
    {
        $countries = Country::with('cities')->get();

        $response = Helper::createSuccessResponse($countries);
        return response()->json($response);
    }
}
