<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use Exception;

use App\Http\Requests\Test\ShowTestAppointmentsRequest;
use App\Http\Requests\Clinic\ShowClinicAppointmentsRequest;
use App\Http\Requests\Appointment\BookAppointmentRequest;
use App\Http\Requests\Appointment\CancelBookRequest;
use App\Http\Requests\Appointment\RescheduleBookRequest;
use App\Http\Requests\Appointment\ShowClinicTestAppointmentsRequest;

use App\Models\Appointment;
use App\Models\AppointmentTime;
use App\Models\Booking;

use App\Repositories\Dashboard\BookingRepository;

class AppointmentApiController extends Controller
{
    public function testAppointments(ShowTestAppointmentsRequest $request)
    {
        $appointments = Appointment::where('test_id', $request->id)->with('clinic.country', 'times')->when($request->date, function($query) use ($request){
                return $query->whereDate('date', $request->date);
            })->get();

        $response = Helper::createSuccessResponse($appointments);
        return response()->json($response);
    }

    public function clinicAppointments(ShowClinicAppointmentsRequest $request)
    {
        $appointments = Appointment::where('clinic_id', $request->id)->with('test', 'times')->when($request->date, function($query) use ($request){
                return $query->whereDate('date', $request->date);
            })->get();

        $response = Helper::createSuccessResponse($appointments);
        return response()->json($response);
    }

    public function clinicTestAppointments(ShowClinicTestAppointmentsRequest $request)
    {
        $appointments = Appointment::where([['clinic_id', $request->clinic_id], ['test_id', $request->test_id]])->with('test', 'clinic.country', 'times')->when($request->date, function($query) use ($request){
                return $query->whereDate('date', $request->date);
            })->get();

        $response = Helper::createSuccessResponse($appointments);
        return response()->json($response);
    }

    public function bookAppointment(BookAppointmentRequest $request)
    {
        $userId = Auth::id();
        $time = AppointmentTime::with('appointment.test', 'appointment.clinic.country')->find($request->time_id);

        $booking = Booking::updateOrCreate([
            'user_id' => $userId,
            'appointment_time_id' => $request->time_id,
        ], [
            'status' => PENDING_BOOKING_STATUS,
            'user_id' => $userId,
            'appointment_time_id' => $request->time_id,
            'price' => (new BookingRepository())->getPrice($time->id),
            'payment_status' => Helper::getPendingBookingPaymentStatus($userId, $request->time_id),
        ]);

        $response = Helper::createSuccessResponse($time);
        return response()->json($response);
    }

    public function cancelBooking(CancelBookRequest $request)
    {
        $userId = Auth::id();
        $booking = Booking::find($request->book_id);

        $booking->update([
            'status' => CANCELED_BOOKING_STATUS,
            'reason_id' => $request->reason_id,
            'comment' => $request->comment,
        ]);

        $response = Helper::createSuccessResponse($booking);
        return response()->json($response);
    }

    public function rescheduleBooking(RescheduleBookRequest $request)
    {
        $userId = Auth::id();
        $booking = Booking::find($request->book_id);

        $booking->update([
            'appointment_time_id' => $request->new_time_id,
            'reason_id' => $request->reason_id,
            'comment' => $request->comment,
            'is_rescheduled' => 1,
            'price' => (new BookingRepository())->getPrice($request->new_time_id),
        ]);
        
        $response = Helper::createSuccessResponse($booking);
        return response()->json($response);
    }
}
