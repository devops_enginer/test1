<?php
    
namespace App\Http\Controllers\Web;
     
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Exception;

use App\Repositories\Frontend\TapPaymentMethodRepository;

use App\Helper\Helper;

use App\Models\User;
use App\Models\TapPaymentMethod;

use VMdevelopment\TapPayment\TapService;
use Session;
     
class TapPaymentController extends Controller
{
    private $tapPaymentMethodRepository;

    public function __construct(TapPaymentMethodRepository $tapPaymentMethodRepository){
        $this->tapPaymentMethodRepository = $tapPaymentMethodRepository;
    }

    public function createPaymentMethod(Request $request)
    {
        if(!isset($request['token'])) abort(404);
        $user = Helper::getUserFromToken($request['token']);
        $token = $request['token'];
        return view('frontend.payment.tap.new_payment_method', compact('token'));
    }

    public function createPaymentMethodPost(Request $request)
    {
        if(!isset($request['user_token'])) abort(404);
        $user = Helper::getUserFromToken($request['user_token']);
        $token = TapService::findToken($request['tap_token']);
        $data = $token->getData();
        $customer = $this->createCustomer($user);
        $savedCard = TapService::saveCard($customer['id'], $data['id']);
        $data['customer_id'] = $customer['id'];
        $data['customer_name'] = $user->full_name;
        $data['customer_email'] = $user->email;
        $data['customer_phone'] = $user->phone_number;
        $data['user_id'] = $user->id;

        $this->tapPaymentMethodRepository->createFromTapResponse($data);

        return back()->with('success', 'Method Added successfully!');
    }

    private function createCustomer($user){
        $customer = TapService::createCustomer();
        $customer->setName($user->full_name);
        $customer->setEmail($user->email);
        $customer->setPhoneNumber($user->phone_number);
        $customer->setCurrency($user->country->currency);
        $customer->setDescription('Health Lab Customer');
        $customerData = $customer->create();
        return $customerData;
    }
}