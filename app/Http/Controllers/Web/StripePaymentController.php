<?php
    
namespace App\Http\Controllers\Web;
     
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Exception;

use App\Repositories\Frontend\StripePaymentMethodRepository;

use App\Helper\Helper;

use App\Models\User;
use App\Models\StripePaymentMethod;

use Session;
use Stripe;
     
class StripePaymentController extends Controller
{
    private $stripePaymentMethodRepository;

    public function __construct(StripePaymentMethodRepository $stripePaymentMethodRepository){
        $this->stripePaymentMethodRepository = $stripePaymentMethodRepository;
    }

    public function createPaymentMethod(Request $request)
    {
        if(!isset($request['token'])) abort(404);
        $user = Helper::getUserFromToken($request['token']);
        $token = $request['token'];

        $intent = $user->createSetupIntent();

        return view('frontend.payment.stripe.create_payment_method', compact('intent', 'token'));
    }

    public function createPaymentMethodPost(Request $request)
    {
        if(!isset($request['user_token'])) abort(404);
        $user = Helper::getUserFromToken($request['user_token']);

        $paymentMethod = $request->input('payment_method');
        $userPaymentMethod = StripePaymentMethod::where('method_id', $paymentMethod)->select('id')->first();

        try {
            $user->createOrGetStripeCustomer();

            if($userPaymentMethod == null){
                $newPaymentMethod = $user->addPaymentMethod($paymentMethod);
                $newPaymentMethodData = $newPaymentMethod->toArray();
                $newPaymentMethodData['user_id'] = $user->id;
                $this->stripePaymentMethodRepository->createFromStripeResponse($newPaymentMethodData);
            }
            
            $user->updateDefaultPaymentMethod($paymentMethod);
        } catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', 'Method Added successfully!');
    }

    public function stripePay()
    {
        return view('frontend.payment.stripe.pay');
    }

    public function stripePayPost(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $customer = Stripe\Customer::create(array(
            "address" => [
                "line1" => "Virani Chowk",
                "postal_code" => "360001",
                "city" => "Rajkot",
                "state" => "GJ",
                "country" => "IN",
            ],
            "email" => "demo@gmail.com",
            "name" => "Hardik Savani",
            "source" => $request->stripeToken
        ));

        Stripe\Charge::create ([
            "amount" => 100 * 100,
            "currency" => "usd",
            "customer" => $customer->id,
            "description" => "Test payment from helth lab.",
            "shipping" => [
                "name" => "Jenny Rosen",
                "address" => [
                    "line1" => "510 Townsend St",
                    "postal_code" => "98140",
                    "city" => "San Francisco",
                    "state" => "CA",
                    "country" => "US",
                ],
            ]
        ]); 

        Session::flash('success', 'Payment successful!');
        return back();
    }
}