<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\City\CreateCityRequest;
use App\Http\Requests\Dashboard\City\UpdateCityRequest;
use App\Models\User;
use App\Models\City;
use App\Models\Country;
use App\Repositories\Dashboard\CityRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class CityController extends Controller
{
    public $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    public function index(){
        $cities = City::all();

        $data = [
            'cities' => $cities,
        ];

        return view('admin.pages.city.index', $data);
    }

    public function city_list(Request $request){
        if ($request->ajax()){
            $data = City::query()->with('country');
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                $countriesIds = Country::where('name', 'LIKE', '%' . $searchValue . '%')->select('id')->pluck('id');
                return $query->whereIn('country_id', $countriesIds)->orWhere('name', 'LIKE', '%' . $searchValue . '%');
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($city) {
                        return '<input type="checkbox" id="'.$city->id.'" name="someCheckbox" />';
                    })->editColumn('image', function (City $city){
                        return '<img src="' . $city->image . '" style="width: 100px" />';
                    })->addColumn('country', function (City $city){
                        return $city->country->name ?? '';
                    })->addColumn('action', function ($city) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-city="'.$city->id.'" data-url="' . route('city.delete_city') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('city.edit', $city->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'image', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.city.index');
    }

    public function create()
    {
        $countries = Country::all();

        $data = [
            'countries' => $countries
        ];

        return view('admin.pages.city.create', $data);
    }

    public function store(CreateCityRequest $request)
    {
        $this->cityRepository->create($request->all());
        session()->flash('success', 'City has been added successfully!');
        return redirect()->route('city.city_list');
    }

    public function show(City $city)
    {
        return view('admin.pages.city.show', compact('city'));
    }

    public function edit(City $city)
    {
        $countries = Country::all();

        $data = [
            'countries' => $countries,
            'city' => $city,
        ];

        return view('admin.pages.city.edit', $data);
    }

    public function update(UpdateCityRequest $request, $cityId)
    {
        $city = City::findOrFail($cityId);
        $this->cityRepository->update($city, $request->all());
        session()->flash('success', 'City has been updated successfully!');
        return redirect()->route('city.city_list');
    }

    public function delete_city(Request $request){
        $this->cityRepository->deleteById($request->id);
        session()->flash('success', 'City has been deleted successfully!');
    }

    public function getCityUsers(Request $request){
        $request->validate([
            'id' => 'required|integer|exists:cities',
        ]);

        $users = User::where('city_id', $request->id)->get();

        return response()->json(['users' => $users]);
    }
}
