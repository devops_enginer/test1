<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\DoctorBooking\CreateDoctorBookingRequest;
use App\Http\Requests\Dashboard\DoctorBooking\UpdateDoctorBookingRequest;
use App\Models\DoctorBooking;
use App\Models\User;
use App\Models\Doctor;
use App\Models\Reason;
use App\Models\DoctorAppointment;
use App\Models\DoctorAppointmentTime;
use App\Repositories\Dashboard\DoctorBookingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class DoctorBookingController extends Controller
{
    public $doctorBookingRepository;

    public function __construct(DoctorBookingRepository $doctorBookingRepository)
    {
        $this->doctorBookingRepository = $doctorBookingRepository;
    }

    public function index(){
        $doctorBookings = DoctorBooking::all();

        $data = [
            'doctorBookings' => $doctorBookings,
        ];

        return view('admin.pages.doctor_booking.index', $data);
    }

    public function booking_list(Request $request){
        if ($request->ajax()){
            $data = DoctorBooking::query()->with('appointmentTime.appointment.doctor', 'reason');
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                $doctorsIds = Doctor::where('name', 'LIKE', '%' . $searchValue . '%')->select('id')->pluck('id');
                $usersIds = User::where('full_name', 'LIKE', '%' . $searchValue . '%')->select('id')->pluck('id');

                $appointmentsIds = DoctorAppointment::where('date', 'LIKE', '%' . $searchValue . '%')->orWhereIn('doctor_id', $doctorsIds)->select('id')->pluck('id');
                $appointmentTimesIds = DoctorAppointmentTime::where('time', 'LIKE', '%' . $searchValue . '%')->orWhereIn('doctor_appointment_id', $appointmentsIds)->select('id')->pluck('id');

                return $query->where('status', 'LIKE', '%' . $searchValue . '%')
                            ->orWhere('created_at', 'LIKE', '%' . $searchValue . '%')
                            ->orWhereIn('user_id', $usersIds)
                            ->orWhereIn('doctor_appointment_time_id', $appointmentTimesIds);
            })->when($request->rescheduled, function($query){
                return $query->where('is_rescheduled', 1);
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($doctorBooking) {
                        return '<input type="checkbox" id="'.$doctorBooking->id.'" name="someCheckbox" />';
                    })->addColumn('reason', function (DoctorBooking $doctorBooking){
                        return $doctorBooking->reason->text ?? null;
                    })->addColumn('user', function (DoctorBooking $doctorBooking){
                        return $doctorBooking->user->full_name;
                    })->addColumn('date', function (DoctorBooking $doctorBooking){
                        return $doctorBooking->appointmentTime->appointment->date;
                    })->addColumn('time', function (DoctorBooking $doctorBooking){
                        return $doctorBooking->appointmentTime->time;
                    })->editColumn('created_at', function (DoctorBooking $doctorBooking){
                        return $doctorBooking->created_at->format('M d, Y H:i');
                    })->addColumn('doctor', function (DoctorBooking $doctorBooking){
                        return $doctorBooking->appointmentTime->appointment->doctor->name;
                    })->editColumn('status', function (DoctorBooking $doctorBooking){
                        return $doctorBooking->status_text;
                    })->editColumn('prescription', function (DoctorBooking $doctorBooking){
                        $prescription = $doctorBooking->real_prescription
                            ? '<a target="_blank" href="' . $doctorBooking->prescription . '">Download</a>'
                            : null;
                        return $prescription;
                    })->addColumn('action', function ($doctorBooking) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-booking="'.$doctorBooking->id.'" data-url="' . route('doctor.booking.delete_booking') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('doctor.booking.edit', $doctorBooking->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'prescription', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }

        $doctors = Doctor::all();
        $rescheduled = $request->rescheduled ? true : false;

        $data = [
            'doctors' => $doctors,
            'rescheduled' => $rescheduled,
        ];
        return view('admin.pages.doctor_booking.index', $data);
    }

    public function create()
    {
        $doctors = Doctor::all();
        $users = User::all();

        $data = [
            'users' => $users,
            'doctors' => $doctors,
        ];

        return view('admin.pages.doctor_booking.create', $data);
    }

    public function store(CreateDoctorBookingRequest $request)
    {
        $this->doctorBookingRepository->create($request->all());
        session()->flash('success', 'Doctor Booking has been added successfully!');
        return redirect()->route('doctor.booking.booking_list');
    }

    public function show(DoctorBooking $doctorBooking)
    {
        return view('admin.pages.doctor_booking.show',compact('DoctorBooking'));
    }

    public function edit($doctorBookingId)
    {
        $doctorBooking = DoctorBooking::findOrFail($doctorBookingId);
        $users = User::all();
        $doctors = Doctor::all();
        $reasons = Reason::all();
        $appointments = DoctorAppointment::where('doctor_id', $doctorBooking->appointmentTime->appointment->doctor_id)->get();
        $appointmentTimes = DoctorAppointmentTime::where('doctor_appointment_id', $doctorBooking->appointmentTime->doctor_appointment_id)->get();

        $data = [
            'reasons' => $reasons,
            'users' => $users,
            'doctors' => $doctors,
            'doctorBooking' => $doctorBooking,
            'appointments' => $appointments,
            'appointmentTimes' => $appointmentTimes,
        ];

        return view('admin.pages.doctor_booking.edit', $data);
    }

    public function update(UpdateDoctorBookingRequest $request, $doctorBookingId)
    {
        $doctorBooking = DoctorBooking::findOrFail($doctorBookingId);

        if(!$doctorBooking->real_prescription && $request->status == COMPLETED_BOOKING_STATUS && !isset($request['prescription'])){
            return redirect()->back()->with('error', 'Prescription must be uploaded');
        }

        $this->doctorBookingRepository->update($doctorBooking, $request->all());
        session()->flash('success', 'Doctor Booking has been updated successfully!');
        return redirect()->route('doctor.booking.booking_list');
    }

    public function delete_booking(Request $request){
        $this->doctorBookingRepository->deleteById($request->id);
        session()->flash('success', 'Doctor Booking has been deleted successfully!');
    }
}
