<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\User\CreateUserRequest;
use App\Http\Requests\Dashboard\User\UpdateUserRequest;
use App\Models\User;
use App\Models\Country;
use App\Models\City;
use App\Repositories\Dashboard\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request){
        $users = User::all();

        $data = [
            'users' => $users,
        ];

        if($request->ajax()){
            return response()->json($data);
        }

        return view('admin.pages.user.index', $data);
    }

    public function user_list(Request $request){
        if ($request->ajax()){
            $data = User::query();
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query->where('full_name', 'LIKE', '%' . $searchValue . '%')
                ->orWhere('email', 'LIKE', '%' . $searchValue . '%')
                ->orWhere('phone_number', 'LIKE', '%' . $searchValue . '%');
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($user) {
                        return '<input type="checkbox" id="'.$user->id.'" name="someCheckbox" />';
                    })->editColumn('language', function (User $user){
                        return $user->language == 'en' ? 'English' : 'Arabic';
                    })->addColumn('country', function (User $user){
                        return $user->country->name;
                    })->addColumn('city', function (User $user){
                        return $user->city->name;
                    })->addColumn('action', function ($user) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-user="'.$user->id.'" data-url="' . route('users.delete_user') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('users.edit', $user->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox','action'])->make(true);
            } catch (\Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.user.index');
    }

    public function create()
    {
        $countries = Country::all();

        $data = [
            'countries' => $countries,
        ];

        return view('admin.pages.user.create', $data);
    }

    public function store(CreateUserRequest $request)
    {
        $this->userRepository->create($request->all());
        session()->flash('success', 'User has been added successfully!');
        return redirect()->route('users.user_list');
    }

    public function show(User $user)
    {
        return view('admin.pages.user.show',compact('user'));
    }

    public function edit(User $user)
    {
        $countries = Country::all();
        $cities = City::where('country_id', $user->country_id)->get();

        $data = [
            'countries' => $countries,
            'cities' => $cities,
            'user' => $user,
        ];

        return view('admin.pages.user.edit', $data);
    }

    public function update(UpdateUserRequest $request, $userId)
    {
        $user = User::findOrFail($userId);
        $this->userRepository->update($user,$request->all());
        session()->flash('success', 'User has been updated successfully!');
        return redirect()->route('users.user_list');
    }

    public function delete_user(Request $request){
        $this->userRepository->deleteById($request->id);
        session()->flash('success', 'User has been deleted successfully!');
    }
}
