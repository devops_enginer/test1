<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\DoctorCategory\CreateDoctorCategoryRequest;
use App\Http\Requests\Dashboard\DoctorCategory\UpdateDoctorCategoryRequest;
use App\Models\DoctorCategory;
use App\Models\City;
use App\Repositories\Dashboard\DoctorCategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class DoctorCategoryController extends Controller
{
    public $doctorCategoryRepository;

    public function __construct(DoctorCategoryRepository $doctorCategoryRepository)
    {
        $this->doctorCategoryRepository = $doctorCategoryRepository;
    }

    public function index(){
        $doctorCategories = DoctorCategory::all();

        $data = [
            'doctorCategories' => $doctorCategories,
        ];

        return view('admin.pages.doctor_category.index', $data);
    }

    public function doctor_category_list(Request $request){
        if ($request->ajax()){
            $data = DoctorCategory::query();
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query;
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($doctorCategory) {
                        return '<input type="checkbox" id="'.$doctorCategory->id.'" name="someCheckbox" />';
                    })->addColumn('icon', function ($doctorCategory) {
                        return '<img src="' . $doctorCategory->icon . '" style="width:100px" />';
                    })->addColumn('action', function ($doctorCategory) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-category="'.$doctorCategory->id.'" data-url="' . route('doctor.category.delete_doctor_category') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('doctor.category.edit', $doctorCategory->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'icon', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.doctor_category.index');
    }

    public function create()
    {
        $data = [

        ];

        return view('admin.pages.doctor_category.create', $data);
    }

    public function store(CreateDoctorCategoryRequest $request)
    {
        $this->doctorCategoryRepository->create($request->all());
        session()->flash('success', 'Doctor Category has been added successfully!');
        return redirect()->route('doctor.category.doctor_category_list');
    }

    public function show(DoctorCategory $doctorCategory)
    {
        return view('admin.pages.doctor_category.show', compact('doctorCategory'));
    }

    public function edit(DoctorCategory $doctorCategory)
    {
        $data = [
            'doctorCategory' => $doctorCategory,
        ];

        return view('admin.pages.doctor_category.edit', $data);
    }

    public function update(UpdateDoctorCategoryRequest $request, $doctorCategoryId)
    {
        $doctorCategory = DoctorCategory::findOrFail($doctorCategoryId);
        $this->doctorCategoryRepository->update($doctorCategory, $request->all());
        session()->flash('success', 'Doctor Category has been updated successfully!');
        return redirect()->route('doctor.category.doctor_category_list');
    }

    public function delete_doctor_category(Request $request){
        $this->doctorCategoryRepository->deleteById($request->id);
        session()->flash('success', 'Doctor Category has been deleted successfully!');
    }

    public function getCategoryDoctors(Request $request){
        $request->validate([
            'id' => 'required|integer|exists:doctor_categories',
        ]);

        $doctors = Doctor::where('category_id', $request->id)->get();

        return response()->json(['doctors' => $doctors]);
    }
}
