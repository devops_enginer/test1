<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\DoctorAppointment\CreateDoctorAppointmentRequest;
use App\Http\Requests\Dashboard\DoctorAppointment\UpdateDoctorAppointmentRequest;
use App\Models\DoctorAppointment;
use App\Models\DoctorAppointmentTime;
use App\Models\Doctor;
use App\Repositories\Dashboard\DoctorAppointmentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class DoctorAppointmentController extends Controller
{
    public $doctorAppointmentRepository;

    public function __construct(DoctorAppointmentRepository $doctorAppointmentRepository)
    {
        $this->doctorAppointmentRepository = $doctorAppointmentRepository;
    }

    public function index(){
        $doctorAppointments = DoctorAppointment::all();

        $data = [
            'doctorAppointments' => $doctorAppointments,
        ];

        return view('admin.pages.doctor_appointment.index', $data);
    }

    public function appointment_list(Request $request){
        if ($request->ajax()){
            $data = DoctorAppointment::query()->with('doctor');
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query;
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($doctorAppointment) {
                        return '<input type="checkbox" id="'.$doctorAppointment->id.'" name="someCheckbox" />';
                    })->editColumn('doctor', function (DoctorAppointment $doctorAppointment){
                        return $doctorAppointment->doctor->name ?? '';
                    })->addColumn('action', function ($doctorAppointment) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-appointment="'.$doctorAppointment->id.'" data-url="' . route('doctor.appointment.delete_appointment') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('doctor.appointment.edit', $doctorAppointment->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.doctor_appointment.index');
    }

    public function create()
    {
        $doctors = Doctor::all();

        $data = [
            'doctors' => $doctors,
        ];

        return view('admin.pages.doctor_appointment.create', $data);
    }

    public function store(CreateDoctorAppointmentRequest $request)
    {
        $this->doctorAppointmentRepository->create($request->all());
        session()->flash('success', 'Doctor Appointment has been added successfully!');
        return redirect()->route('doctor.appointment.appointment_list');
    }

    public function show(DoctorAppointment $doctorAppointment)
    {
        return view('admin.pages.doctor_appointment.show',compact('doctorAppointment'));
    }

    public function edit($doctorAppointmentId)
    {
        $doctorAppointment = DoctorAppointment::findOrFail($doctorAppointmentId);
        $doctors = Doctor::all();

        $data = [
            'doctors' => $doctors,
            'doctorAppointment' => $doctorAppointment,
        ];

        return view('admin.pages.doctor_appointment.edit', $data);
    }

    public function update(UpdateDoctorAppointmentRequest $request, $doctorAppointmentId)
    {
        $doctorAppointment = DoctorAppointment::findOrFail($doctorAppointmentId);
        $this->doctorAppointmentRepository->update($doctorAppointment, $request->all());
        session()->flash('success', 'Doctor Appointment has been updated successfully!');
        return redirect()->route('doctor.appointment.appointment_list');
    }

    public function delete_appointment(Request $request){
        $this->doctorAppointmentRepository->deleteById($request->id);
        session()->flash('success', 'Doctor Appointment has been deleted successfully!');
    }

    public function doctorAppointments(Request $request){
        $request->validate([
            'doctor_id' => 'required|integer|exists:doctors,id',
        ]);

        $doctorAppointments = DoctorAppointment::where('doctor_id', $request->doctor_id)->get();

        return response()->json(['doctorAppointments' => $doctorAppointments]);
    }

    public function appointmentTimes(Request $request){
        $request->validate([
            'appointment_id' => 'required|integer|exists:doctor_appointments,id',
        ]);

        $times = DoctorAppointmentTime::where('doctor_appointment_id', $request->appointment_id)->get();

        return response()->json(['times' => $times]);
    }
}
