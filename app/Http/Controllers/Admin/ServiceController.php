<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Service\CreateServiceRequest;
use App\Http\Requests\Dashboard\Service\UpdateServiceRequest;
use App\Models\Service;
use App\Repositories\Dashboard\ServiceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class ServiceController extends Controller
{
    public $serviceRepository;

    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    public function index(){
        $services = Service::all();

        $data = [
            'services' => $services,
        ];

        return view('admin.pages.service.index', $data);
    }

    public function service_list(Request $request){
        if ($request->ajax()){
            $data = Service::query();
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query;
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($service) {
                        return '<input type="checkbox" id="'.$service->id.'" name="someCheckbox" />';
                    })->editColumn('icon', function (Service $service){
                        return '<img src="' . $service->icon . '" style="width: 100px" />';
                    })->addColumn('action', function ($service) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-service="'.$service->id.'" data-url="' . route('service.delete_service') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('service.edit', $service->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'icon', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.service.index');
    }

    public function create()
    {
        $data = [
            
        ];

        return view('admin.pages.service.create', $data);
    }

    public function store(CreateServiceRequest $request)
    {
        $this->serviceRepository->create($request->all());
        session()->flash('success', 'Service has been added successfully!');
        return redirect()->route('service.service_list');
    }

    public function show(Service $service)
    {
        return view('admin.pages.service.show', compact('service'));
    }

    public function edit(Service $service)
    {
        $data = [
            'service' => $service,
        ];

        return view('admin.pages.service.edit', $data);
    }

    public function update(UpdateServiceRequest $request, $serviceId)
    {
        $service = Service::findOrFail($serviceId);
        $this->serviceRepository->update($service, $request->all());
        session()->flash('success', 'Service has been updated successfully!');
        return redirect()->route('service.service_list');
    }

    public function delete_service(Request $request){
        $this->serviceRepository->deleteById($request->id);
        session()->flash('success', 'Service has been deleted successfully!');
    }
}
