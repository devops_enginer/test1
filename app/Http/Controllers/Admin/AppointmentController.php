<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Appointment\CreateAppointmentRequest;
use App\Http\Requests\Dashboard\Appointment\UpdateAppointmentRequest;
use App\Models\Appointment;
use App\Models\AppointmentTime;
use App\Models\Test;
use App\Models\Clinic;
use App\Repositories\Dashboard\AppointmentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class AppointmentController extends Controller
{
    public $appointmentRepository;

    public function __construct(AppointmentRepository $appointmentRepository)
    {
        $this->appointmentRepository = $appointmentRepository;
    }

    public function index(){
        $appointments = Appointment::all();

        $data = [
            'appointments' => $appointments,
        ];

        return view('admin.pages.appointment.index', $data);
    }

    public function appointment_list(Request $request){
        if ($request->ajax()){
            $data = Appointment::query()->with('clinic', 'test');
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                $testsIds = Test::where('title', 'LIKE', '%' . $searchValue . '%')->select('id')->pluck('id');
                $clinicsIds = Clinic::where('name', 'LIKE', '%' . $searchValue . '%')->select('id')->pluck('id');

                return $query->whereIn('test_id', $testsIds)->orWhereIn('clinic_id', $clinicsIds)->orWhere('date', 'LIKE', '%' . $searchValue . '%');
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($appointment) {
                        return '<input type="checkbox" id="'.$appointment->id.'" name="someCheckbox" />';
                    })->editColumn('clinic', function (Appointment $appointment){
                        return $appointment->clinic->name ?? '';
                    })->addColumn('test', function (Appointment $appointment){
                        return $appointment->test->title ?? '';
                    })->addColumn('action', function ($appointment) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-appointment="'.$appointment->id.'" data-url="' . route('appointment.delete_appointment') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('appointment.edit', $appointment->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'image', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.appointment.index');
    }

    public function create()
    {
        $tests = Test::all();
        $clinics = Clinic::all();

        $data = [
            'tests' => $tests,
            'clinics' => $clinics,
        ];

        return view('admin.pages.appointment.create', $data);
    }

    public function store(CreateAppointmentRequest $request)
    {
        $this->appointmentRepository->create($request->all());
        session()->flash('success', 'Appointment has been added successfully!');
        return redirect()->route('appointment.appointment_list');
    }

    public function show(Appointment $appointment)
    {
        return view('admin.pages.appointment.show',compact('appointment'));
    }

    public function edit(Appointment $appointment)
    {
        $tests = Test::all();
        $clinics = Clinic::all();

        $data = [
            'clinics' => $clinics,
            'tests' => $tests,
            'appointment' => $appointment,
        ];

        return view('admin.pages.appointment.edit', $data);
    }

    public function update(UpdateAppointmentRequest $request, $appointmentId)
    {
        $appointment = Appointment::findOrFail($appointmentId);
        $this->appointmentRepository->update($appointment, $request->all());
        session()->flash('success', 'Appointment has been updated successfully!');
        return redirect()->route('appointment.appointment_list');
    }

    public function delete_appointment(Request $request){
        $this->appointmentRepository->deleteById($request->id);
        session()->flash('success', 'Appointment has been deleted successfully!');
    }

    public function testClinicAppointments(Request $request){
        $request->validate([
            'test_id' => 'required|integer|exists:tests,id',
            'clinic_id' => 'required|integer|exists:clinics,id',
        ]);

        $appointments = Appointment::where([['test_id', $request->test_id], ['clinic_id', $request->clinic_id]])->get();

        return response()->json(['appointments' => $appointments]);
    }

    public function appointmentTimes(Request $request){
        $request->validate([
            'appointment_id' => 'required|integer|exists:appointments,id',
        ]);

        $times = AppointmentTime::where('appointment_id', $request->appointment_id)->get();

        return response()->json(['times' => $times]);
    }
}
