<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Doctor\CreateDoctorRequest;
use App\Http\Requests\Dashboard\Doctor\UpdateDoctorRequest;
use App\Models\Doctor;
use App\Models\DoctorCategory;
use App\Models\Country;
use App\Models\City;
use App\Repositories\Dashboard\DoctorRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class DoctorController extends Controller
{
    public $doctorRepository;

    public function __construct(DoctorRepository $doctorRepository)
    {
        $this->doctorRepository = $doctorRepository;
    }

    public function index(){
        $doctors = Doctor::all();

        $data = [
            'doctors' => $doctors,
        ];

        return view('admin.pages.doctor.index', $data);
    }

    public function Doctor_list(Request $request){
        $this->doctorRepository->getDefaultDoctor();
        
        if ($request->ajax()){
            $data = Doctor::query()->with('city', 'country');
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query;
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($doctor) {
                        return '<input type="checkbox" id="'.$doctor->id.'" name="someCheckbox" />';
                    })->editColumn('image', function (Doctor $doctor){
                        return '<img src="' . $doctor->image . '" style="width: 100px" />';
                    })->addColumn('city', function (Doctor $doctor){
                        return $doctor->city->name ?? null;
                    })->addColumn('country', function (Doctor $doctor){
                        return $doctor->country->name ?? null;
                    })->addColumn('category', function (Doctor $doctor){
                        return $doctor->category->name ?? null;
                    })->addColumn('action', function ($doctor) {
                        $actions = '<div class="activity-icon">
                                        <ul style="list-style: none">
                                            <li>
                                                <a id="delete" onclick="" data-doctor="' . $doctor->id . '" data-url="' . route('doctor.delete_doctor') . '" class=""><i class="mdi mdi-trash-can"></i>
                                                </a>
                                            </i>
                                            <li>
                                                <a  href="' . route('doctor.edit', $doctor->id) . '" class=""><i class="mdi mdi-grease-pencil"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>';
                        return $actions;
                    })->rawColumns(['checkbox', 'image', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.doctor.index');
    }

    public function create()
    {
        $countries = Country::all();
        $categories = DoctorCategory::all();

        $data = [
            'countries' => $countries,
            'categories' => $categories,
        ];

        return view('admin.pages.doctor.create', $data);
    }

    public function store(CreateDoctorRequest $request)
    {
        $this->doctorRepository->create($request->all());
        session()->flash('success', 'Doctor has been added successfully!');
        return redirect()->route('doctor.doctor_list');
    }

    public function show(Doctor $doctor)
    {
        return view('admin.pages.doctor.show',compact('doctor'));
    }

    public function edit(Doctor $doctor)
    {
        $countries = Country::all();
        $cities = City::where('country_id', $doctor->country_id)->get();
        $categories = DoctorCategory::all();

        $data = [
            'countries' => $countries,
            'cities' => $cities,
            'doctor' => $doctor,
            'categories' => $categories,
        ];

        return view('admin.pages.doctor.edit', $data);
    }

    public function update(UpdateDoctorRequest $request, $doctorId)
    {
        $doctor = Doctor::findOrFail($doctorId);
        $this->doctorRepository->update($doctor, $request->all());
        session()->flash('success', 'Doctor has been updated successfully!');
        return redirect()->route('doctor.doctor_list');
    }

    public function delete_doctor(Request $request){
        $doctor = Doctor::findOrFail($request->id);
        if($doctor->is_default) return redirect()->back()->with('error', 'Cannot Delete');
        $this->doctorRepository->deleteById($request->id);
        session()->flash('success', 'Doctor has been deleted successfully!');
    }
}
