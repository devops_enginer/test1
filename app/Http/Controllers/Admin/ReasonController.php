<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Reason\CreateReasonRequest;
use App\Http\Requests\Dashboard\Reason\UpdateReasonRequest;
use App\Models\Reason;
use App\Repositories\Dashboard\ReasonRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class ReasonController extends Controller
{
    public $reasonRepository;

    public function __construct(ReasonRepository $reasonRepository)
    {
        $this->reasonRepository = $reasonRepository;
    }

    public function index(){
        $reasons = Reason::all();

        $data = [
            'reasons' => $reasons,
        ];

        return view('admin.pages.reason.index', $data);
    }

    public function reason_list(Request $request){
        if ($request->ajax()){
            $data = Reason::query();
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query->where('text', 'LIKE', '%' . $searchValue . '%');
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($reason) {
                        return '<input type="checkbox" id="'.$reason->id.'" name="someCheckbox" />';
                    })->addColumn('action', function ($reason) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-reason="'.$reason->id.'" data-url="' . route('reason.delete_reason') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('reason.edit', $reason->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'image', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.reason.index');
    }

    public function create()
    {
        $data = [

        ];

        return view('admin.pages.reason.create', $data);
    }

    public function store(CreateReasonRequest $request)
    {
        $this->reasonRepository->create($request->all());
        session()->flash('success', 'Reason has been added successfully!');
        return redirect()->route('reason.reason_list');
    }

    public function show(Reason $reason)
    {
        return view('admin.pages.reason.show', compact('reason'));
    }

    public function edit(Reason $reason)
    {
        $data = [
            'reason' => $reason,
        ];

        return view('admin.pages.reason.edit', $data);
    }

    public function update(UpdateReasonRequest $request, $reasonId)
    {
        $reason = Reason::findOrFail($reasonId);
        $this->reasonRepository->update($reason, $request->all());
        session()->flash('success', 'Reason has been updated successfully!');
        return redirect()->route('reason.reason_list');
    }

    public function delete_reason(Request $request){
        $this->reasonRepository->deleteById($request->id);
        session()->flash('success', 'Reason has been deleted successfully!');
    }
}
