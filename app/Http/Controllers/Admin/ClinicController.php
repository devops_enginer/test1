<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Clinic\CreateClinicRequest;
use App\Http\Requests\Dashboard\Clinic\UpdateClinicRequest;
use App\Models\Clinic;
use App\Models\Country;
use App\Models\City;
use App\Repositories\Dashboard\ClinicRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class ClinicController extends Controller
{
    public $clinicRepository;

    public function __construct(ClinicRepository $clinicRepository)
    {
        $this->clinicRepository = $clinicRepository;
    }

    public function index(){
        $clinics = Clinic::all();

        $data = [
            'clinics' => $clinics,
        ];

        return view('admin.pages.clinic.index', $data);
    }

    public function clinic_list(Request $request){
        $this->clinicRepository->getDefaultClinic();
        
        if ($request->ajax()){
            $data = Clinic::query()->with('city', 'country');
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query;
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($clinic) {
                        return '<input type="checkbox" id="'.$clinic->id.'" name="someCheckbox" />';
                    })->editColumn('image', function (Clinic $clinic){
                        return '<img src="' . $clinic->image . '" style="width: 100px" />';
                    })->editColumn('is_default', function (Clinic $clinic){
                        return $clinic->is_default ? 'Yes' : 'No';
                    })->addColumn('city', function (Clinic $clinic){
                        return $clinic->city->name ?? null;
                    })->addColumn('country', function (Clinic $clinic){
                        return $clinic->country->name ?? null;
                    })->addColumn('action', function ($clinic) {
                        $actions = '<div class="activity-icon">
                                        <ul style="list-style: none">';
                        if($clinic->is_default == 0){
                            $actions .=     '<li>
                                                <a id="delete" onclick="" data-clinic="' . $clinic->id . '" data-url="' . route('clinic.delete_clinic') . '" class=""><i class="mdi mdi-trash-can"></i>
                                                </a>
                                            </i>';
                        }
                        $actions .=         '<li>
                                                <a  href="' . route('clinic.edit', $clinic->id) . '" class=""><i class="mdi mdi-grease-pencil"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>';
                        return $actions;
                    })->rawColumns(['checkbox', 'image', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.clinic.index');
    }

    public function create()
    {
        $countries = Country::all();

        $data = [
            'countries' => $countries
        ];

        return view('admin.pages.clinic.create', $data);
    }

    public function store(CreateClinicRequest $request)
    {
        $this->clinicRepository->create($request->all());
        session()->flash('success', 'Clinic has been added successfully!');
        return redirect()->route('clinic.clinic_list');
    }

    public function show(Clinic $clinic)
    {
        return view('admin.pages.clinic.show',compact('Clinic'));
    }

    public function edit(Clinic $clinic)
    {
        $countries = Country::all();
        $cities = City::where('country_id', $clinic->country_id)->get();

        $data = [
            'countries' => $countries,
            'cities' => $cities,
            'clinic' => $clinic,
        ];

        return view('admin.pages.clinic.edit', $data);
    }

    public function update(UpdateClinicRequest $request, $clinicId)
    {
        $clinic = Clinic::findOrFail($clinicId);
        $this->clinicRepository->update($clinic, $request->all());
        session()->flash('success', 'Clinic has been updated successfully!');
        return redirect()->route('clinic.clinic_list');
    }

    public function delete_clinic(Request $request){
        $clinic = Clinic::findOrFail($request->id);
        if($clinic->is_default) return redirect()->back()->with('error', 'Cannot Delete');
        $this->clinicRepository->deleteById($request->id);
        session()->flash('success', 'Clinic has been deleted successfully!');
    }
}
