<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\StaticContent\UpdateStaticContentRequest;
use App\Models\Admin\StaticContent;
use App\Repositories\Dashboard\StaticContentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class StaticContentController extends Controller
{
    public $staticContentRepository;

    public function __construct(StaticContentRepository $staticContentRepository)
    {
        $this->staticContentRepository = $staticContentRepository;
    }

    public function show(StaticContent $staticContent)
    {
        return view('admin.pages.static_content.show',compact('staticContent'));
    }

    public function edit()
    {
        $staticContent = StaticContent::orderBy('id', 'desc')->first();

        if($staticContent == null){
            $staticContent = $this->staticContentRepository->initContent();
        }

        $data = [
            'staticContent' => $staticContent,
        ];

        return view('admin.pages.static_content.edit', $data);
    }

    public function update(UpdateStaticContentRequest $request)
    {
        $staticContent = StaticContent::orderBy('id', 'desc')->first();
        if($staticContent == null) abort(404);
        
        $this->staticContentRepository->update($staticContent, $request->all());
        session()->flash('success', 'Website Content has been updated successfully!');
        return redirect()->route('static.content.edit');
    }
}
