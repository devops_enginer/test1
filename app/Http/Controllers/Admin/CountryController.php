<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Country\CreateCountryRequest;
use App\Http\Requests\Dashboard\Country\UpdateCountryRequest;
use App\Models\User;
use App\Models\Country;
use App\Models\City;
use App\Repositories\Dashboard\CountryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class CountryController extends Controller
{
    public $countryRepository;

    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function index(){
        $countries = Country::all();

        $data = [
            'countries' => $countries,
        ];

        return view('admin.pages.country.index', $data);
    }

    public function country_list(Request $request){
        if ($request->ajax()){
            $data = Country::query();
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query;
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($country) {
                        return '<input type="checkbox" id="'.$country->id.'" name="someCheckbox" />';
                    })->addColumn('flag', function ($country) {
                        return '<img src="' . $country->flag . '" style="width:100px" />';
                    })->addColumn('action', function ($country) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-country="'.$country->id.'" data-url="' . route('country.delete_country') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('country.edit', $country->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'flag', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.country.index');
    }

    public function create()
    {
        $data = [

        ];

        return view('admin.pages.country.create', $data);
    }

    public function store(CreateCountryRequest $request)
    {
        $this->countryRepository->create($request->all());
        session()->flash('success', 'Country has been added successfully!');
        return redirect()->route('country.country_list');
    }

    public function show(Country $country)
    {
        return view('admin.pages.country.show', compact('country'));
    }

    public function edit(Country $country)
    {
        $data = [
            'country' => $country,
        ];

        return view('admin.pages.country.edit', $data);
    }

    public function update(UpdateCountryRequest $request, $countryId)
    {
        $country = Country::findOrFail($countryId);
        $this->countryRepository->update($country, $request->all());
        session()->flash('success', 'Country has been updated successfully!');
        return redirect()->route('country.country_list');
    }

    public function delete_country(Request $request){
        $this->countryRepository->deleteById($request->id);
        session()->flash('success', 'Country has been deleted successfully!');
    }

    public function getCountryCities(Request $request){
        $request->validate([
            'id' => 'required|integer|exists:countries',
        ]);

        $cities = City::where('country_id', $request->id)->get();

        return response()->json(['cities' => $cities]);
    }

    public function getCountryUsers(Request $request){
        $request->validate([
            'id' => 'required|integer|exists:countries',
        ]);

        $users = User::where('country_id', $request->id)->get();

        return response()->json(['users' => $users]);
    }
}
