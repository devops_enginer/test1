<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Notification\PushNotificationRequest;
use App\Models\Country;
use App\Models\Notification;
use App\Models\User;
use App\Models\Test;
use App\Repositories\Dashboard\NotificationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class NotificationController extends Controller
{
    public $notificationRepository;

    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function index(){
        $notifications = Notification::all();

        $data = [
            'notifications' => $notifications,
        ];

        return view('admin.pages.notification.index', $data);
    }

    public function notification_list(Request $request){
        if ($request->ajax()){
            $data = Notification::query()->with('user', 'test');
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query;
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($notification) {
                        return '<input type="checkbox" id="'.$notification->id.'" name="someCheckbox" />';
                    })->addColumn('test', function (Notification $notification){
                        return $notification->test->title ?? '';
                    })->addColumn('user', function (Notification $notification){
                        return $notification->user->full_name;
                    })->addColumn('action', function ($notification) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-notification="'.$notification->id.'" data-url="' . route('notification.delete_notification') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.notification.index');
    }

    public function create()
    {
        $users = User::all();
        $tests = Test::all();
        $countries = Country::all();

        $data = [
            'tests' => $tests,
            'users' => $users,
            'countries' => $countries,
        ];

        return view('admin.pages.notification.create', $data);
    }

    public function store(PushNotificationRequest $request)
    {
        $this->notificationRepository->create($request->all());
        session()->flash('success', 'Notification has been added successfully!');
        return redirect()->route('notification.notification_list');
    }

    public function show(Notification $notification)
    {
        return view('admin.pages.notification.show',compact('notification'));
    }

    public function delete_notification(Request $request){
        $this->notificationRepository->deleteById($request->id);
        session()->flash('success', 'Notification has been deleted successfully!');
    }
}
