<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Booking\CreateBookingRequest;
use App\Http\Requests\Dashboard\Booking\UpdateBookingRequest;
use App\Models\Booking;
use App\Models\Country;
use App\Models\City;
use App\Models\User;
use App\Models\Clinic;
use App\Models\Test;
use App\Models\Reason;
use App\Models\Appointment;
use App\Models\AppointmentTime;
use App\Models\Result;
use App\Models\BookingResultParameter;
use App\Repositories\Dashboard\BookingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class BookingController extends Controller
{
    public $bookingRepository;

    public function __construct(BookingRepository $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }

    public function index(){
        $bookings = Booking::all();

        $data = [
            'bookings' => $bookings,
        ];

        return view('admin.pages.booking.index', $data);
    }

    public function booking_list(Request $request){
        if ($request->ajax()){
            $data = Booking::query()->with('appointmentTime.appointment.test', 'appointmentTime.appointment.clinic.country', 'appointmentTime.appointment.clinic.city', 'reason', 'user', 'result.parameters');
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                $countriesIds = Country::where('name', 'LIKE', '%' . $searchValue . '%')->select('id')->pluck('id');
                $citiesIds = City::where('name', 'LIKE', '%' . $searchValue . '%')->select('id')->pluck('id');
                $clinicsIds = Clinic::where('name', 'LIKE', '%' . $searchValue . '%')->orWhereIn('country_id', $countriesIds)->orWhereIn('city_id', $citiesIds)->select('id')->pluck('id');
                $testsIds = Test::where('title', 'LIKE', '%' . $searchValue . '%')->select('id')->pluck('id');
                $usersIds = User::where('full_name', 'LIKE', '%' . $searchValue . '%')->select('id')->pluck('id');

                $appointmentsIds = Appointment::where('date', 'LIKE', '%' . $searchValue . '%')->orWhereIn('test_id', $testsIds)->orWhereIn('clinic_id', $clinicsIds)->select('id')->pluck('id');
                $appointmentTimesIds = AppointmentTime::where('time', 'LIKE', '%' . $searchValue . '%')->orWhereIn('appointment_id', $appointmentsIds)->select('id')->pluck('id');

                return $query->where('status', 'LIKE', '%' . $searchValue . '%')
                            ->orWhere('created_at', 'LIKE', '%' . $searchValue . '%')
                            ->orWhere('price', 'LIKE', '%' . $searchValue . '%')
                            ->orWhere('payment_status', 'LIKE', '%' . $searchValue . '%')
                            ->orWhereIn('user_id', $usersIds)
                            ->orWhereIn('appointment_time_id', $appointmentTimesIds);
            })->when($request->rescheduled, function($query){
                return $query->where('is_rescheduled', 1);
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($booking) {
                        return '<input type="checkbox" id="'.$booking->id.'" name="someCheckbox" />';
                    })->addColumn('reason', function (Booking $booking){
                        return $booking->reason->text ?? null;
                    })->addColumn('user', function (Booking $booking){
                        return '<a target="_blank" href="' . route('users.edit', $booking->user->id) . '">' . $booking->user->full_name . '</a>';
                    })->addColumn('date', function (Booking $booking){
                        return $booking->appointmentTime->appointment->date;
                    })->addColumn('time', function (Booking $booking){
                        return $booking->appointmentTime->time;
                    })->editColumn('created_at', function (Booking $booking){
                        return $booking->created_at->format('M d, Y H:i');
                    })->addColumn('test', function (Booking $booking){
                        return $booking->appointmentTime->appointment->test->title;
                    })->addColumn('clinic', function (Booking $booking){
                        return $booking->appointmentTime->appointment->clinic->name;
                    })->addColumn('country', function (Booking $booking){
                        return $booking->appointmentTime->appointment->clinic->country->name ?? null;
                    })->addColumn('city', function (Booking $booking){
                        return $booking->appointmentTime->appointment->clinic->city->name ?? null;
                    })->editColumn('status', function (Booking $booking){
                        return $booking->status_text;
                    })->editColumn('payment_status', function (Booking $booking){
                        return $booking->payment_status_text;
                    })->addColumn('result', function (Booking $booking){
                        $link = '';
                        if($booking->result)
                            $link = '<a target="_blank" href="' . $booking->result->path . '">Download Result</a>';

                        return $link;
                    })->addColumn('action', function ($booking) {
                        $html = '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-booking="'.$booking->id.'" data-url="' . route('booking.delete_booking') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('booking.edit', $booking->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>';

                        if($booking->result){
                            $html .= '<li><a href="' . route('booking.result.details', ['id' => $booking->result->id]) . '" class="result-details"><i class="far fa-calendar-alt"></i></a></li>';
                        }

                        $html .= '</ul>
                                </div>';

                        return $html;
                    })->rawColumns(['checkbox', 'user', 'result', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }

        $clinics = Clinic::all();
        $countries = Country::all();

        $rescheduled = $request->rescheduled ? true : false;

        $data = [
            'clinics' => $clinics,
            'rescheduled' => $rescheduled,
            'countries' => $countries,
        ];
        
        return view('admin.pages.booking.index', $data);
    }

    public function create()
    {
        $tests = Test::all();
        $clinics = Clinic::all();
        $users = User::all();

        $data = [
            'users' => $users,
            'tests' => $tests,
            'clinics' => $clinics,
        ];

        return view('admin.pages.booking.create', $data);
    }

    public function store(CreateBookingRequest $request)
    {
        $this->bookingRepository->create($request->all());
        session()->flash('success', 'Booking has been added successfully!');
        return redirect()->route('booking.booking_list');
    }

    public function show(Booking $booking)
    {
        return view('admin.pages.booking.show',compact('booking'));
    }

    public function edit(Booking $booking)
    {
        $users = User::all();
        $tests = Test::all();
        $clinics = Clinic::all();
        $reasons = Reason::all();
        $appointments = Appointment::where([['test_id', $booking->appointmentTime->appointment->test_id], ['clinic_id', $booking->appointmentTime->appointment->clinic_id]])->get();
        $appointmentTimes = AppointmentTime::where('appointment_id', $booking->appointmentTime->appointment_id)->get();

        $data = [
            'reasons' => $reasons,
            'users' => $users,
            'tests' => $tests,
            'clinics' => $clinics,
            'booking' => $booking,
            'appointments' => $appointments,
            'appointmentTimes' => $appointmentTimes,
        ];

        return view('admin.pages.booking.edit', $data);
    }

    public function update(UpdateBookingRequest $request, $bookingId)
    {
        $booking = Booking::findOrFail($bookingId);
        
        if(!$booking->result && $request->status == COMPLETED_BOOKING_STATUS && !isset($request['file'])){
            return redirect()->back()->with('error', 'File must be uploaded');
        }

        $this->bookingRepository->update($booking, $request->all());
        session()->flash('success', 'Booking has been updated successfully!');
        return redirect()->route('booking.booking_list');
    }

    public function delete_booking(Request $request){
        $this->bookingRepository->deleteById($request->id);
        session()->flash('success', 'Booking has been deleted successfully!');
    }

    public function resultDetails(Request $request){
        $request->validate([
            'id' => 'required|integer|exists:results,id'
        ]);

        $parameters = BookingResultParameter::where('result_id', $request->id)->get();
        $data = ['parameters' => $parameters];
        $modal = view('admin.pages.booking.result_details_modal', $data)->render();
        return response()->json(['modal' => $modal]);
    }
}
