<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\PromotionalBanner\CreatePromotionalBannerRequest;
use App\Http\Requests\Dashboard\PromotionalBanner\UpdatePromotionalBannerRequest;
use App\Models\PromotionalBanner;
use App\Models\Test;
use App\Repositories\Dashboard\PromotionalBannerRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class PromotionalBannerController extends Controller
{
    public $promotionalBannerRepository;

    public function __construct(PromotionalBannerRepository $promotionalBannerRepository)
    {
        $this->promotionalBannerRepository = $promotionalBannerRepository;
    }

    public function index(){
        $promotionalBanners = PromotionalBanner::all();

        $data = [
            'PromotionalBanners' => $promotionalBanners,
        ];

        return view('admin.pages.promotional_banner.index', $data);
    }

    public function banner_list(Request $request){
        if ($request->ajax()){
            $data = PromotionalBanner::query();
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                $testsIds = Test::where('title', 'LIKE', '%' . $searchValue . '%')->select('id')->pluck('id');
                return $query->whereIn('test_id', $testsIds);
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($promotionalBanner) {
                        return '<input type="checkbox" id="'.$promotionalBanner->id.'" name="someCheckbox" />';
                    })->editColumn('image', function (PromotionalBanner $promotionalBanner){
                        return '<img src="' . $promotionalBanner->image . '" style="width: 100px" />';
                    })->addColumn('test', function (PromotionalBanner $promotionalBanner){
                        return $promotionalBanner->test->title ?? '';
                    })->addColumn('action', function ($promotionalBanner) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-banner="'.$promotionalBanner->id.'" data-url="' . route('promotional.banner.delete_banner') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('promotional.banner.edit', $promotionalBanner->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'image', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.promotional_banner.index');
    }

    public function create()
    {
        $tests = Test::all();

        $data = [
            'tests' => $tests
        ];

        return view('admin.pages.promotional_banner.create', $data);
    }

    public function store(CreatePromotionalBannerRequest $request)
    {
        $this->promotionalBannerRepository->create($request->all());
        session()->flash('success', 'Promotional Banner has been added successfully!');
        return redirect()->route('promotional.banner.banner_list');
    }

    public function show(PromotionalBanner $promotionalBanner)
    {
        return view('admin.pages.promotional_banner.show',compact('promotionalBanner'));
    }

    public function edit(PromotionalBanner $promotionalBanner)
    {
        $tests = Test::all();

        $data = [
            'tests' => $tests,
            'promotionalBanner' => $promotionalBanner,
        ];

        return view('admin.pages.promotional_banner.edit', $data);
    }

    public function update(UpdatePromotionalBannerRequest $request, $promotionalBannerId)
    {
        $promotionalBanner = PromotionalBanner::findOrFail($promotionalBannerId);
        $this->promotionalBannerRepository->update($promotionalBanner, $request->all());
        session()->flash('success', 'Promotional Banner has been updated successfully!');
        return redirect()->route('promotional.banner.banner_list');
    }

    public function delete_banner(Request $request){
        $this->promotionalBannerRepository->deleteById($request->id);
        session()->flash('success', 'Promotional Banner has been deleted successfully!');
    }
}
