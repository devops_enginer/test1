<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Test\CreateTestRequest;
use App\Http\Requests\Dashboard\Test\UpdateTestRequest;
use App\Models\Test;
use App\Models\Clinic;
use App\Models\Service;
use App\Repositories\Dashboard\TestRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class TestController extends Controller
{
    public $testRepository;

    public function __construct(TestRepository $testRepository)
    {
        $this->testRepository = $testRepository;
    }

    public function index(){
        $tests = Test::all();

        $data = [
            'Tests' => $tests,
        ];

        return view('admin.pages.test.index', $data);
    }

    public function test_list(Request $request){
        if ($request->ajax()){
            $data = Test::query()->with('service', 'clinics');
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query;
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($test) {
                        return '<input type="checkbox" id="'.$test->id.'" name="someCheckbox" />';
                    })->editColumn('is_featured', function (Test $test){
                        return $test->is_featured ? 'Yes' : 'No';
                    })->editColumn('thumbnail', function (Test $test){
                        return '<img src="' . $test->thumbnail . '" style="width: 100px" />';
                    })->editColumn('image', function (Test $test){
                        return '<img src="' . $test->image . '" style="width: 100px" />';
                    })->addColumn('service', function (Test $test){
                        return $test->service->name;
                    })->addColumn('clinics', function (Test $test){
                        $clinics = $test->clinics->pluck('name')->toArray();
                        $clinics = implode(', ', $clinics);
                        return $clinics;
                    })->addColumn('action', function ($test) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-test="'.$test->id.'" data-url="' . route('test.delete_test') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('test.edit', $test->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'image', 'thumbnail', 'description', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.test.index');
    }

    public function create()
    {
        $services = Service::all();
        $clinics = Clinic::all();

        $data = [
            'services' => $services,
            'clinics' => $clinics,
        ];

        return view('admin.pages.test.create', $data);
    }

    public function store(CreateTestRequest $request)
    {
        $this->testRepository->create($request->all());
        session()->flash('success', 'Test has been added successfully!');
        return redirect()->route('test.test_list');
    }

    public function show(Test $test)
    {
        return view('admin.pages.test.show',compact('Test'));
    }

    public function edit(Test $test)
    {
        $services = Service::all();
        $clinics = Clinic::all();
        
        $data = [
            'services' => $services,
            'clinics' => $clinics,
            'test' => $test,
        ];

        return view('admin.pages.test.edit', $data);
    }

    public function update(UpdateTestRequest $request, $testId)
    {
        $test = Test::findOrFail($testId);
        $this->testRepository->update($test, $request->all());
        session()->flash('success', 'Test has been updated successfully!');
        return redirect()->route('test.test_list');
    }

    public function delete_test(Request $request){
        $this->testRepository->deleteById($request->id);
        session()->flash('success', 'Test has been deleted successfully!');
    }
}
