<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\PromoCode\CreatePromoCodeRequest;
use App\Http\Requests\Dashboard\PromoCode\UpdatePromoCodeRequest;
use App\Models\PromoCode;
use App\Repositories\Dashboard\PromoCodeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class PromoCodeController extends Controller
{
    public $promoCodeRepository;

    public function __construct(PromoCodeRepository $promoCodeRepository)
    {
        $this->promoCodeRepository = $promoCodeRepository;
    }

    public function index(){
        $promoCodes = PromoCode::all();

        $data = [
            'promoCodes' => $promoCodes,
        ];

        return view('admin.pages.promo_code.index', $data);
    }

    public function code_list(Request $request){
        if ($request->ajax()){
            $data = PromoCode::query();
            $search = $request->get('search');
            $searchValue = $search['value'] ?? null;

            $data->when(isset($searchValue), function($query) use ($searchValue){
                return $query;
            });

            try {
                return datatables($data)
                    ->addColumn('checkbox', function ($promoCode) {
                        return '<input type="checkbox" id="'.$promoCode->id.'" name="someCheckbox" />';
                    })->addColumn('type', function (PromoCode $promoCode){
                        return $promoCode->type_text;
                    })->addColumn('action', function ($promoCode) {
                        return '<div class="activity-icon">
                                    <ul style="list-style: none">
                                        <li><a id="delete" onclick="" data-code="'.$promoCode->id.'" data-url="' . route('promo.code.delete_code') . '" class=""><i class="mdi mdi-trash-can"></i></a></li>
                                        <li><a  href="' . route('promo.code.edit', $promoCode->id) . '" class=""><i class="mdi mdi-grease-pencil"></i></a></li>
                                     </ul>
                                </div>';
                    })->rawColumns(['checkbox', 'action'])->make(true);
            } catch (Exception $e) {
                return new GeneralException($e);
            }
        }
        return view('admin.pages.promo_code.index');
    }

    public function create()
    {
        $data = [

        ];

        return view('admin.pages.promo_code.create', $data);
    }

    public function store(CreatePromoCodeRequest $request)
    {
        $this->promoCodeRepository->create($request->all());
        session()->flash('success', 'Promo Code has been added successfully!');
        return redirect()->route('promo.code.code_list');
    }

    public function show(PromoCode $promoCode)
    {
        return view('admin.pages.promo_code.show',compact('promoCode'));
    }

    public function edit(PromoCode $promoCode)
    {
        $data = [
            'promoCode' => $promoCode,
        ];

        return view('admin.pages.promo_code.edit', $data);
    }

    public function update(UpdatePromoCodeRequest $request, $promoCodeId)
    {
        $promoCode = PromoCode::findOrFail($promoCodeId);
        $this->promoCodeRepository->update($promoCode, $request->all());
        session()->flash('success', 'Promo Code has been updated successfully!');
        return redirect()->route('promo.code.code_list');
    }

    public function delete_code(Request $request){
        $this->promoCodeRepository->deleteById($request->id);
        session()->flash('success', 'Promo Code has been deleted successfully!');
    }
}
