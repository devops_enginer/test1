<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Notification;
use App\Models\DeviceToken;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class NotificationRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return Notification::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $users = isset($data['user_id']) ? $data['user_id'] : User::select('id')->pluck('id')->toArray();

            $this->pushNotification($users, $data['title'], $data['body']);
            
            $dataToStore = [];
            foreach ($users as $key => $user) {
                $dataToStore[] = [
                    'title' => $data['title'],
                    'body' => $data['body'],
                    'user_id' => $user,
                    'test_id' => $data['test_id'] ?? null,
                    'created_at' => date_create(),
                    'updated_at' => date_create(),
                ];
            }

            DB::table('notifications')->insert($dataToStore);
            return $dataToStore;
        });
        throw new GeneralException('error');
    }

    private function pushNotification($usersIds, $notificationTitle, $notificationBody){
        if(!is_array($usersIds)){
            $usersIds = [$usersIds];
        }

        $firebaseToken = DeviceToken::whereIn('user_id', $usersIds)->select('token')->pluck('token')->all();

        $SERVER_API_KEY = env('FIREBASE_API_KEY');

        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                "title" => $notificationTitle,
                "body" => $notificationBody,
            ]
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);
        return $response;
    }
}
