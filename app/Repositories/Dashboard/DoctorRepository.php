<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Doctor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class DoctorRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return Doctor::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $doctor = parent::create([
                'image' => $this->UploadFile(
                    $data['image'],
                    DOCTOR_IMG_PATH),
                'name' => $data['name'],
                'address' => $data['address'],
                'lat' => $data['lat'],
                'lng' => $data['lng'],
                'phone' => $data['phone'] ?? null,
                'country_id' => $data['country_id'],
                'city_id' => $data['city_id'],
                'rating' => $data['rating'],
                'description' => $data['description'],
                'experience' => $data['experience'],
                'category_id' => $data['category_id'],
            ]);

            return $doctor;
        });
        throw new GeneralException('error');
    }

    public function update(Doctor $doctor, array $data){
        return DB::transaction(function () use ($doctor, $data){
           if ($doctor->update([
               'image' => isset($data['image']) ? $this->Updatefile(
                    $data['image'],
                    DOCTOR_IMG_PATH, $doctor->real_image) : $doctor->real_image,
               'name' => $data['name'] ?? $doctor->name,
               'address' => $data['address'] ?? $doctor->address,
               'lat' => $data['lat'] ?? $doctor->lat,
               'lng' => $data['lng'] ?? $doctor->lng,
               'phone' => $data['phone'] ?? $doctor->phone,
               'country_id' => $data['country_id'] ?? $doctor->country_id,
               'city_id' => $data['city_id'] ?? $doctor->city_id,
               'rating' => $data['rating'] ?? $doctor->rating,
               'description' => $data['description'] ?? $doctor->description,
               'experience' => $data['experience'] ?? $doctor->experience,
               'category_id' => $data['category_id'] ?? $doctor->category_id,
           ])){
               return $doctor;
           };
        });

        throw new GeneralException('error');
    }
}
