<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Clinic;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class ClinicRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return Clinic::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $clinic = parent::create([
                'image' => $this->UploadFile(
                    $data['image'],
                    CLINIC_IMG_PATH),
                'name' => $data['name'],
                'address' => $data['address'],
                'lat' => $data['lat'],
                'lng' => $data['lng'],
                'rating' => $data['rating'],
                'number_of_ratings' => $data['number_of_ratings'],
                'working_hours' => $data['working_hours'],
                'city_id' => $data['city_id'],
                'country_id' => $data['country_id'],
            ]);

            return $clinic;
        });
        throw new GeneralException('error');
    }

    public function update(Clinic $clinic, array $data){
        return DB::transaction(function () use ($clinic, $data){
           if ($clinic->update([
               'image' => isset($data['image']) ? $this->Updatefile(
                    $data['image'],
                    CLINIC_IMG_PATH, $clinic->real_image) : $clinic->real_image,
               'name' => $data['name'] ?? $clinic->name,
               'address' => $data['address'] ?? $clinic->address,
               'lat' => $data['lat'] ?? $clinic->lat,
               'lng' => $data['lng'] ?? $clinic->lng,
               'rating' => $data['rating'] ?? $clinic->rating,
               'number_of_ratings' => $data['number_of_ratings'] ?? $clinic->number_of_ratings,
               'working_hours' => $data['working_hours'] ?? $clinic->working_hours,
               'city_id' => $data['city_id'] ?? $clinic->city_id,
               'country_id' => $data['country_id'] ?? $clinic->country_id,
           ])){
               return $clinic;
           };
        });

        throw new GeneralException('error');
    }

    public function getDefaultClinic(){
        $clinic = Clinic::with('tests', 'city', 'country')->where('is_default', 1)->first();
        if($clinic == null) $clinic = $this->createDefaultClinic();
        return $clinic;
    }

    private function createDefaultClinic(){
        $clinic = Clinic::create([
            'name' => 'Home Lab',
            'image' => 'uploaded_files/images/clinic/default_clinic.jpg',
            'address' => 'Home',
            'lat' => 0.0,
            'lng' => 0.0,
            'country_id' => 1,
            'city_id' => 1,
            'rating' => 5.0,
            'number_of_ratings' => 100,
            'working_hours' => '10:00 AM - 10:00 PM',
            'is_default' => 1,
        ]);

        return $clinic;
    }
}
