<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Result;
use App\Models\BookingResultParameter;
use App\Models\Booking;
use App\Models\AppointmentTime;
use App\Models\ClinicTest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;
use Smalot\PdfParser\Parser;

class BookingRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return Booking::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $booking = parent::create([
                'status' => $data['status'],
                'user_id' => $data['user_id'],
                'appointment_time_id' => $data['appointment_time_id'],
                'price' => $this->getPrice($data['appointment_time_id']),
            ]);

            return $booking;
        });
        throw new GeneralException('error');
    }

    public function update(Booking $booking, array $data){
        return DB::transaction(function () use ($booking, $data){
           if ($booking->update([
               'status' => $data['status'] ?? $booking->status,
               'user_id' => $data['user_id'] ?? $booking->user_id,
               'reason_id' => $data['reason_id'] ?? null,
               'comment' => $data['comment'] ?? null,
               'price' => $this->getPrice($data['appointment_time_id']),
               'appointment_time_id' => $data['appointment_time_id'] ?? $booking->appointment_time_id,
           ])){
                if(isset($data['file'])){
                    $this->createResult($booking, $data['file']); 
                }
                return $booking;
           };
        });

        throw new GeneralException('error');
    }

    private function createResult($booking, $file){
        $result = Result::updateOrCreate([
            'booking_id' => $booking->id,
        ], [
            'path' => $this->UploadFile($file, RESULT_FILE_PATH),
            'appointment_time_id' => $booking->appointment_time_id,
            'booking_id' => $booking->id,
        ]);

        $parameters = $this->readPDF($file);
        $storedParameters = $this->storeParameters($parameters, $result->id);
    }

    private function storeParameters($parameters, $resultId){
        BookingResultParameter::where('result_id', $resultId)->delete();

        foreach ($parameters as $key => $parameter) {
            if(!is_array($parameter) || !isset($parameter['result']) || !isset($parameter['test'])) continue;

            BookingResultParameter::create([
                'result_id' => $resultId,
                'test' => $parameter['test'],
                'result' => $parameter['result'],
                'units' => $parameter['units'] ?? null,
                'reference_range' => $parameter['reference_range'] ?? null,
                'methodology' => $parameter['methodology'] ?? null,
            ]);
        }
    }

    private function readPDF($file){
        // use of pdf parser to read content from pdf
        $fileName = $file->getClientOriginalName();

        $pdfParser = new Parser();

        $pdf = $pdfParser->parseFile($file->path());


        $pages = $pdf->getPages();
        $contents = [];
        // Loop over each page to extract text.
        foreach ($pages as $page) {
            $page = $page->getText();
            $page = str_replace("Sample Type: ", "Sample Type : \n", $page);
            $page = trim(preg_replace('/\t+/', "\n", $page));
            $contents[] = explode("\n", $page);
        }

        foreach ($contents as $content) {
            $sort_content = $content;
            $start_key = 0;
            $end_key = 0;

            foreach ($content as $key => $index) {
                if (stripos($index, "Methodology") !== false) {
                    $start_key = $key;

                }
                if (stripos($index, "End Of Report") !== false) {
                    $end_key += $key - 1;

                } elseif (str_contains($index, "*") !== false) {
                    unset($sort_content[$key]);
                    $end_key -= 1;
                }

                if (stripos($index, "This test should not") !== false
                    || stripos($index, "treponemal-specific antibody test") !== false
                    || stripos($index, "been reported in some infectious disease") !== false
                    || stripos($index, "In cases of untreated, late") !== false
                    || stripos($index, "methods including") !== false
                    || stripos($index, "Treponema pallidum  agglutination should be positive") !== false
                    || stripos($index, "Despite active syphilis, serologic ") !== false
                    || stripos($index, "A thorough clinical and historica") !== false
                    || stripos($index, " COI") !== false) {
                    unset($sort_content[$key]);
                    $end_key -= 1;
                }
                if (stripos($index, "ence Immunoassay") !== false) {
                    $sort_content[$key - 1] = $sort_content[$key - 1] . '' . $index;
                    unset($sort_content[$key]);
                    $end_key -= 1;
                }

                if (stripos($index, "(ECL)") !== false) {
                    $sort_content[$key - 2] = $sort_content[$key - 2] . '' . $index;
                    unset($sort_content[$key]);
                    $end_key -= 1;
                }

                if (str_starts_with($index, '<') !== false) {
                    $sort_content[$key] = $sort_content[$key] . ' ' . $sort_content[$key + 1] . ' ' . $sort_content[$key + 2];
                    unset($sort_content[$key + 1], $sort_content[$key + 2]);
                    $end_key -= 2;
                }
            }

            $outputs[] = array_values(array_filter(array_slice($sort_content, $start_key + 1, $end_key - $start_key)));
        }

        foreach ($outputs as $key => $output) {
            $length = count($output);
            $test = 1;
            $sub_test = 1;
            $next_test = false;

            for ($i = 0; $i < $length; $i++) {
                if($i == 0 || $next_test)
                {
                    $result[$key][$test]['test'] = $output[$i];

                    if(stripos($output[$i+1], "reactive") !== false || stripos($output[$i+1], "detected") !== false || is_numeric($output[$i+1]))
                    {
                        $result[$key][$test]['result'] = $output[$i+1];
                        $result[$key][$test]['methodology'] = $output[$i+2];
                        if (stripos($output[$i+3], "reactive") !== false)
                        {
                            $result[$key][$test]['reference_range'] = $output[$i+3];
                        }
                        else
                        {
                            $result[$key][$test]['reference_range'] = $output[$i+3];
                        }
                    }
                    else
                    {
                        $result[$key][$test]['methodology'] = $output[$i+1];

                    }
                    $next_test = false;
                } elseif (strpos($output[$i], 'Sample Type') !== false) {
                    $result[$key][$test]['sample_type'] = $output[$i+1];
                    $next_test = true;
                    $next_test_key = $i+2;
                    $i+=1;
                    $test++;
                }
            }
        }

        foreach ($result as $key => $result_index) {
            foreach ($result_index as $result_index_key => $result_index_index) {
                if ($result_index_index['test'] == 'STD 17 PCR')
                {
                    foreach ($outputs as $key => $output) {
                        if(in_array("STD 17 PCR", $output, true)){
                            unset($result[$key]);
                            $length = count($output);
                            $test = 1;
                            $next_test = false;

                            for ($i = 0; $i < $length; $i++) {
                                if ($i == 0)
                                {
                                    $result[$key][$test]['test'] = $output[$i];
                                    $result[$key][$test]['methodology'] = $output[$i+1];
                                    $i++;
                                    $test++;
                                }
                                elseif($output[$i] == 'Sample Type : ')
                                {
                                    $result[$key]['sample_type'] = $output[$i+1];
                                    break;
                                }
                                elseif ($i+1!=$length)
                                {
                                    $result[$key][$test]['test'] = $output[$i];
                                    $result[$key][$test]['result'] = $output[$i+1];
                                    $result[$key][$test]['reference_range'] = $output[$i+2];
                                    $i+=2;
                                    $test++;
                                }
                            }
                        }
                    }
                }
            }
        }

        $finalResult = [];
        foreach ($result as $key => $array) {
            $finalResult = array_merge($finalResult, $array);
        }

        return $finalResult;
    }

    public function getPrice($appointmentTimeId){
        $appointmentTime = AppointmentTime::find($appointmentTimeId);
        $appointment = $appointmentTime->appointment;
        $clinicTest = ClinicTest::where([['test_id', $appointment->test_id], ['clinic_id', $appointment->clinic_id]])->first();
        return $clinicTest->test_price ?? 0.0;
    }
}
