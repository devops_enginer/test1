<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\PromotionalBanner;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class PromotionalBannerRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return PromotionalBanner::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $promotionalBanner = parent::create([
                'image' => $this->UploadFile(
                    $data['image'],
                    BANNER_IMG_PATH),
                'test_id' => $data['test_id'] ?? null,
                'order' => $data['order'],
            ]);

            return $promotionalBanner;
        });
        throw new GeneralException('error');
    }

    public function update(PromotionalBanner $promotionalBanner, array $data){
        return DB::transaction(function () use ($promotionalBanner, $data){
           if ($promotionalBanner->update([
               'image' => isset($data['image']) ? $this->Updatefile(
                    $data['image'],
                    BANNER_IMG_PATH, $promotionalBanner->real_image) : $promotionalBanner->real_image,
               'test_id' => $data['test_id'] ?? $promotionalBanner->test_id,
               'order' => $data['order'] ?? $promotionalBanner->order,
           ])){
               return $promotionalBanner;
           };
        });

        throw new GeneralException('error');
    }
}
