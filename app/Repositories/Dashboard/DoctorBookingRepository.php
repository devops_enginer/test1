<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Result;
use App\Models\DoctorBooking;
use App\Models\DoctorAppointmentTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class DoctorBookingRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return DoctorBooking::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $appointmentTime = DoctorAppointmentTime::where('id', $data['doctor_appointment_time_id'])->update([
                'user_id' => $data['user_id'],
            ]);

            $doctorBooking = parent::create([
                'status' => $data['status'],
                'user_id' => $data['user_id'],
                'doctor_appointment_time_id' => $data['doctor_appointment_time_id'],
                'prescription' => isset($data['prescription']) ? $this->UploadFile(
                    $data['prescription'],
                    DOCTOR_BOOKING_PRESCRIPTION_PATH) : null,
            ]);

            return $doctorBooking;
        });
        throw new GeneralException('error');
    }

    public function update(DoctorBooking $doctorBooking, array $data){
        if(isset($data['doctor_appointment_time_id']) && $data['doctor_appointment_time_id'] != $doctorBooking->doctor_appointment_time_id){
            $appointmentTime = DoctorAppointmentTime::where('id', $doctorBooking->doctor_appointment_time_id)->update([
                'user_id' => null,
            ]);

            $appointmentTime = DoctorAppointmentTime::where('id', $data['doctor_appointment_time_id'])->update([
                'user_id' => $data['user_id'] ?? $doctorBooking->user_id,
            ]);
        }

        return DB::transaction(function () use ($doctorBooking, $data){
           if ($doctorBooking->update([
               'status' => $data['status'] ?? $doctorBooking->status,
               'user_id' => $data['user_id'] ?? $doctorBooking->user_id,
               'reason_id' => $data['reason_id'] ?? null,
               'comment' => $data['comment'] ?? null,
               'doctor_appointment_time_id' => $data['doctor_appointment_time_id'] ?? $doctorBooking->doctor_appointment_time_id,
               'prescription' => isset($data['prescription']) ? $this->Updatefile(
                    $data['prescription'],
                    DOCTOR_BOOKING_PRESCRIPTION_PATH, $doctorBooking->real_prescription) : $clinic->real_prescription,
           ])){
                
                return $doctorBooking;
           };
        });

        throw new GeneralException('error');
    }
}
