<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\DoctorAppointment;
use App\Models\DoctorAppointmentTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class DoctorAppointmentRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return DoctorAppointment::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $doctorAppointment = parent::create([
                'doctor_id' => $data['doctor_id'],
                'date' => $data['date'],
            ]);

            if(isset($data['times'])){
                $this->addTimes($data['times'], $doctorAppointment->id);
            }

            return $doctorAppointment;
        });
        throw new GeneralException('error');
    }

    public function update(DoctorAppointment $doctorAppointment, array $data){
        return DB::transaction(function () use ($doctorAppointment, $data){
           if ($doctorAppointment->update([
               'doctor_id' => $data['doctor_id'] ?? $doctorAppointment->doctor_id,
               'date' => $data['date'] ?? $doctorAppointment->date,
           ])){

                if(isset($data['times'])){
                    $this->updateTimes($data['times'], $doctorAppointment->id);
                }
               return $doctorAppointment;
           };
        });

        throw new GeneralException('error');
    }

    private function addTimes($times, $doctorAppointmentId){
        foreach($times as $key => $time){
            DoctorAppointmentTime::create([
                'time' => $time['time'],
                'price' => $time['price'],
                'doctor_appointment_id' => $doctorAppointmentId,
            ]);
        }
    }

    private function deleteTimes($doctorAppointmentId){
        $result = DoctorAppointmentTime::where('doctor_appointment_id', $doctorAppointmentId)->delete();
        return $result;
    }

    private function updateTimes($times, $doctorAppointmentId){
        $this->deleteTimes($doctorAppointmentId);
        $this->addTimes($times, $doctorAppointmentId);
    }
}
