<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class ServiceRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return Service::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $service = parent::create([
                'icon' => $this->UploadFile(
                    $data['icon'],
                    SERVICE_ICON_PATH),
                'name' => $data['name'],
            ]);

            return $service;
        });
        throw new GeneralException('error');
    }

    public function update(Service $service, array $data){
        return DB::transaction(function () use ($service, $data){
            if ($service->update([
                'icon' => isset($data['icon']) ? $this->Updatefile(
                    $data['icon'],
                    SERVICE_ICON_PATH, $service->real_icon) : $service->real_icon,
                'name' => $data['name'] ?? $service->name,
            ])){
               return $service;
            };
        });

        throw new GeneralException('error');
    }
}
