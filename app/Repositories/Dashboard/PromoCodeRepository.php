<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\PromoCode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class PromoCodeRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return PromoCode::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $promoCode = parent::create([
                'code' => $data['code'],
                'type' => $data['type'],
                'value' => $data['value'],
                'limit' => $data['limit'] ?? null,
                'expiry_date' => $data['expiry_date'] ?? null,
            ]);

            return $promoCode;
        });
        throw new GeneralException('error');
    }

    public function update(PromoCode $promoCode, array $data){
        return DB::transaction(function () use ($promoCode, $data){
           if ($promoCode->update([
               'code' => $data['code'] ?? $promoCode->test_id,
               'type' => $data['type'] ?? $promoCode->order,
               'value' => $data['value'] ?? $promoCode->order,
               'limit' => $data['limit'] ?? $promoCode->order,
               'expiry_date' => $data['expiry_date'] ?? $promoCode->order,
           ])){
               return $promoCode;
           };
        });

        throw new GeneralException('error');
    }
}
