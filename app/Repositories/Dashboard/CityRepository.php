<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\City;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class CityRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return City::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $city = parent::create([
                'name' => $data['name'],
                'country_id' => $data['country_id'],
            ]);

            return $city;
        });
        throw new GeneralException('error');
    }

    public function update(City $city, array $data){
        return DB::transaction(function () use ($city, $data){
           if ($city->update([
               'name' => $data['name'] ?? $city->name,
               'country_id' => $data['country_id'] ?? $city->country_id,
           ])){
               return $city;
           };
        });

        throw new GeneralException('error');
    }
}
