<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class UserRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return User::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $user = parent::create([
                'full_name' => $data['full_name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'photo' => isset($data['photo']) ? $this->UploadFile($data['photo'], USER_IMG_PATH) : null,
                'phone_number' => $data['phone_number'] ?? null,
                'account_type' => $data['account_type'] ?? 'normal',
                'address_line_1' => $data['address_line_1'] ?? null,
                'address_line_2' => $data['address_line_2'] ?? null,
                'language' => $data['language'] ?? null,
                'city_id' => $data['city_id'] ?? null,
                'country_id' => $data['country_id'] ?? null,
            ]);

            return $user;
        });
        throw new GeneralException('error');
    }

    public function update(User $user, array $data){
        return DB::transaction(function () use ($user, $data){
           if ($user->update([
               'full_name' => $data['full_name'] ?? $user->full_name,
               'email' => $data['email'] ?? $user->email,
               'password' => isset($data['password']) ? Hash::make($data['password']) : $user->password,
               'phone_number' => $data['phone_number'] ?? $user->phone_number,
               'photo' => isset($data['photo']) ? $this->Updatefile($data['photo'], USER_IMG_PATH, $user->real_photo) : $user->real_photo,
               'account_type' => $data['account_type'] ?? $user->account_type,
               'address_line_1' => $data['address_line_1'] ?? $user->address_line_1,
               'address_line_2' => $data['address_line_2'] ?? $user->address_line_2,
               'language' => $data['language'] ?? $user->language,
               'city_id' => $data['city_id'] ?? $user->city_id,
               'country_id' => $data['country_id'] ?? $user->country_id,
           ])){
               return $user;
           };
        });

        throw new GeneralException('error');
    }
}
