<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\DoctorCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class DoctorCategoryRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return DoctorCategory::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $doctorCategory = parent::create([
                'icon' => $this->UploadFile(
                    $data['icon'],
                    DOCTOR_CATEGORY_ICON_PATH),
                'name' => $data['name'],
            ]);

            return $doctorCategory;
        });
        throw new GeneralException('error');
    }

    public function update(DoctorCategory $doctorCategory, array $data){
        return DB::transaction(function () use ($doctorCategory, $data){
           if ($doctorCategory->update([
               'icon' => isset($data['icon']) ? $this->Updatefile(
                    $data['icon'],
                    DOCTOR_CATEGORY_ICON_PATH, $doctorCategory->real_icon) : $doctorCategory->real_icon,
               'name' => $data['name'] ?? $doctorCategory->name,
           ])){
               return $doctorCategory;
           };
        });

        throw new GeneralException('error');
    }
}
