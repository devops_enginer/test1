<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class CountryRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return Country::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $country = parent::create([
                'flag' => $this->UploadFile(
                    $data['flag'],
                    COUNTRY_FLAG_PATH),
                'name' => $data['name'],
                'currency' => $data['currency'],
            ]);

            return $country;
        });
        throw new GeneralException('error');
    }

    public function update(Country $country, array $data){
        return DB::transaction(function () use ($country, $data){
           if ($country->update([
               'flag' => isset($data['flag']) ? $this->Updatefile(
                    $data['flag'],
                    COUNTRY_FLAG_PATH, $country->real_flag) : $country->real_flag,
               'name' => $data['name'] ?? $country->name,
               'currency' => $data['currency'] ?? $country->currency,
           ])){
               return $country;
           };
        });

        throw new GeneralException('error');
    }
}
