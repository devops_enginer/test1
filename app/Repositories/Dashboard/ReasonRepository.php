<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Reason;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class ReasonRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return Reason::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $reason = parent::create([
                'text' => $data['text'],
            ]);

            return $reason;
        });
        throw new GeneralException('error');
    }

    public function update(Reason $reason, array $data){
        return DB::transaction(function () use ($reason, $data){
           if ($reason->update([
               'text' => $data['text'] ?? $reason->text,
           ])){
               return $reason;
           };
        });

        throw new GeneralException('error');
    }
}
