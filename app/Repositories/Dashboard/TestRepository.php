<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Test;
use App\Models\ClinicTest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class TestRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return Test::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $test = parent::create([
                'image' => $this->UploadFile(
                    $data['image'],
                    TEST_IMG_PATH),
                'thumbnail' => $this->UploadFile(
                    $data['thumbnail'],
                    TEST_THUMBNAIL_PATH),
                'title' => $data['title'],
                'description' => $data['description'],
                'is_featured' => isset($data['is_featured']) ? 1 : 0,
                'service_id' => $data['service_id'],
            ]);

            if(isset($data['clinics'])){
                $this->addClinics($data['clinics'], $test->id, $data['prices'] ?? []);
            }

            return $test;
        });
        throw new GeneralException('error');
    }

    public function update(Test $test, array $data){
        return DB::transaction(function () use ($test, $data){
           if ($test->update([
               'image' => isset($data['image']) ? $this->Updatefile(
                    $data['image'],
                    TEST_IMG_PATH, $test->real_image) : $test->real_image,
                'thumbnail' => isset($data['thumbnail']) ? $this->Updatefile(
                    $data['thumbnail'],
                    TEST_THUMBNAIL_PATH, $test->real_thumbnail) : $test->real_thumbnail,
               'title' => $data['title'] ?? $test->title,
               'description' => $data['description'] ?? $test->description,
               'is_featured' => isset($data['is_featured']) ? 1 : 0,
               'service_id' => $data['service_id'] ?? $test->service_id,
           ])){

                if(isset($data['clinics'])){
                    $this->updateClinics($data['clinics'], $test->id, $data['prices'] ?? []);
                }

               return $test;
           };
        });

        throw new GeneralException('error');
    }

    private function getPriceFromArray($prices, $clinicId, $priceKey){
        foreach ($prices as $key => $price) {
            if($price['clinic_id'] == $clinicId) return $price[$priceKey];
        }
        return 0;
    }

    private function addClinics($clinics, $testId, $prices){
        foreach ($clinics as $key => $clinicId) {
            ClinicTest::create([
                'test_id' => $testId,
                'clinic_id' => $clinicId,
                'sale_price' => $this->getPriceFromArray($prices, $clinicId, 'sale_price'),
                'test_price' => $this->getPriceFromArray($prices, $clinicId, 'test_price'),
            ]);
        }
    }

    private function deleteClinics($testId){
        $result = ClinicTest::where('test_id', $testId)->delete();
        return $result;
    }

    private function updateClinics($clinics, $testId, $prices){
        $this->deleteClinics($testId);
        $this->addClinics($clinics, $testId, $prices);
    }
}
