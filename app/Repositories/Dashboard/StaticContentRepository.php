<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Admin\StaticContent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class StaticContentRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return StaticContent::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $staticContent = parent::create([
                'phone_number' => $data['phone_number'],
                'email' => $data['email'],
                'facebook_link' => $data['facebook_link'],
                'address' => $data['address'],
                'about' => $data['about'],
                'privacy_policy' => $data['privacy_policy'],
                'terms' => $data['terms'],
                'lat' => $data['lat'],
                'lng' => $data['lng'],
            ]);

            return $staticContent;
        });
        throw new GeneralException('error');
    }

    public function update(StaticContent $staticContent, array $data){
        return DB::transaction(function () use ($staticContent, $data){
           if ($staticContent->update([
               'phone_number' => $data['phone_number'] ?? $staticContent->phone_number,
               'email' => $data['email'] ?? $staticContent->email,
               'facebook_link' => $data['facebook_link'] ?? $staticContent->facebook_link,
               'address' => $data['address'] ?? $staticContent->address,
               'about' => $data['about'] ?? $staticContent->about,
               'privacy_policy' => $data['privacy_policy'] ?? $staticContent->privacy_policy,
               'terms' => $data['terms'] ?? $staticContent->terms,
               'lat' => $data['lat'] ?? $staticContent->lat,
               'lng' => $data['lng'] ?? $staticContent->lng,
           ])){
               return $staticContent;
           };
        });

        throw new GeneralException('error');
    }

    public function initContent(){
        $data = [
            'phone_number' => '',
            'email' => '',
            'facebook_link' => '',
            'address' => 'address',
            'about' => 'about',
            'privacy_policy' => 'privacy policy',
            'terms' => 'terms',
            'lat' => 25.276987,
            'lng' => 55.296249,
        ];

        return $this->create($data);
    }
}
