<?php

namespace App\Repositories\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Appointment;
use App\Models\AppointmentTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class AppointmentRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return Appointment::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $appointment = parent::create([
                'test_id' => $data['test_id'],
                'clinic_id' => $data['clinic_id'],
                'date' => $data['date'],
            ]);

            if(isset($data['times'])){
                $this->addTimes($data['times'], $appointment->id);
            }

            return $appointment;
        });
        throw new GeneralException('error');
    }

    public function update(Appointment $appointment, array $data){
        return DB::transaction(function () use ($appointment, $data){
           if ($appointment->update([
               'test_id' => $data['test_id'] ?? $appointment->test_id,
               'clinic_id' => $data['clinic_id'] ?? $appointment->clinic_id,
               'date' => $data['date'] ?? $appointment->date,
           ])){

                if(isset($data['times'])){
                    $this->updateTimes($data['times'], $appointment->id);
                }
               return $appointment;
           };
        });

        throw new GeneralException('error');
    }

    private function addTimes($times, $appointmentId){
        foreach($times as $key => $time){
            AppointmentTime::create([
                'time' => $time,
                'appointment_id' => $appointmentId,
            ]);
        }
    }

    private function deleteTimes($appointmentId){
        $result = AppointmentTime::where('appointment_id', $appointmentId)->delete();
        return $result;
    }

    private function updateTimes($times, $appointmentId){
        $this->deleteTimes($appointmentId);
        $this->addTimes($times, $appointmentId);
    }
}
