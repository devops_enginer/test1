<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;
use App\Models\TapPaymentMethod;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

use VMdevelopment\TapPayment\Facade\TapPayment;
use VMdevelopment\TapPayment\TapService;

class TapPaymentMethodRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return TapPaymentMethod::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $data = $this->createTapToken($data);

            $tapPaymentMethod = parent::create([
                'token' => $data['token'],
                'customer_id' => $data['customer_id'],
                'card_id' => $data['card_id'],
                'card_type' => $data['card_type'],
                'card_holder_name' => $data['card_holder_name'],
                'customer_name' => $data['customer_name'],
                'customer_email' => $data['customer_email'],
                'customer_phone' => $data['customer_phone'],
                'card_number' => $data['card_number'],
                'card_exp_month' => $data['card_exp_month'],
                'card_exp_year' => $data['card_exp_year'],
                'card_cvc' => $data['card_cvc'],
                'user_id' => $data['user_id'],
            ]);

            return $tapPaymentMethod;
        });
        throw new GeneralException('error');
    }

    public function createFromTapResponse(array $data)
    {
        return DB::transaction(function () use ($data) {
            $tapPaymentMethod = parent::create([
                'token' => $data['id'],
                'customer_id' => $data['customer_id'],
                'card_id' => $data['card']['id'],
                'card_type' => $data['card']['brand'],
                'customer_name' => $data['customer_name'],
                'customer_email' => $data['customer_email'],
                'customer_phone' => $data['customer_phone'],
                'card_holder_name' => $data['card']['name'],
                'card_number' => $data['card']['first_six']. '****' .$data['card']['last_four'],
                'card_exp_month' => $data['card']['exp_month'],
                'card_exp_year' => $data['card']['exp_year'],
                'card_cvc' => '***',
                'user_id' => $data['user_id'],
            ]);

            return $tapPaymentMethod;
        });
        throw new GeneralException('error');
    }

    public function update(TapPaymentMethod $tapPaymentMethod, array $data){
        return DB::transaction(function () use ($tapPaymentMethod, $data){
            if ($tapPaymentMethod->update([
                'token' => $data['token'] ?? $tapPaymentMethod->token,
                'customer_id' => $data['customer_id'] ?? $tapPaymentMethod->customer_id,
                'card_id' => $data['card_id'] ?? $tapPaymentMethod->card_id,
                'card_type' => $data['card_type'] ?? $tapPaymentMethod->card_type,
                'customer_name' => $data['customer_name'] ?? $tapPaymentMethod->customer_name,
                'customer_email' => $data['customer_email'] ?? $tapPaymentMethod->customer_email,
                'customer_phone' => $data['customer_phone'] ?? $tapPaymentMethod->customer_phone,
                'card_holder_name' => $data['card_holder_name'] ?? $tapPaymentMethod->card_holder_name,
                'card_number' => $data['card_number'] ?? $tapPaymentMethod->card_number,
                'card_exp_month' => $data['card_exp_month'] ?? $tapPaymentMethod->card_exp_month,
                'card_exp_year' => $data['card_exp_year'] ?? $tapPaymentMethod->card_exp_year,
                'card_cvc' => $data['card_cvc'] ?? $tapPaymentMethod->card_cvc,
                'user_id' => $data['user_id'] ?? $tapPaymentMethod->user_id,
            ])){
                return $tapPaymentMethod;
            };
        });

        throw new GeneralException('error');
    }

    private function createTapToken($data){
        $card = TapService::createToken();
        $card->setName($data['card_holder_name']);
        $card->setNumber($data['card_number']);
        $card->setExpMonth($data['card_exp_month']);
        $card->setExpYear($data['card_exp_year']);
        $card->setCVC($data['card_cvc']);
        // $card->setReturnUrl(route('tap.card.redirect'));

        $newData = $card->create();
        $newData = $this->getDataFromTapCardRequest($data, $newData);
        return $newData;
    }

    private function getDataFromTapCardRequest($requestData, $data){
        $newData = [];
        $newData['token'] = $data['id'];
        $newData['user_id'] = $requestData['user_id'];
        $newData['customer_name'] = $requestData['customer_name'];
        $newData['customer_email'] = $requestData['customer_email'];
        $newData['customer_phone'] = $requestData['customer_phone'];
        $newData['card_type'] = $data['type'];
        return $newData;
    }
}
