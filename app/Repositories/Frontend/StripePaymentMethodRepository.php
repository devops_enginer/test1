<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;
use App\Models\StripePaymentMethod;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class StripePaymentMethodRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return StripePaymentMethod::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $stripePaymentMethod = parent::create([
                'method_id' => $data['method_id'],
                'billing_city' => $data['billing_city'] ?? null,
                'billing_state' => $data['billing_state'] ?? null,
                'billing_country' => $data['billing_country'] ?? null,
                'billing_address_line_1' => $data['billing_address_line_1'] ?? null,
                'billing_address_line_2' => $data['billing_address_line_2'] ?? null,
                'billing_postal_code' => $data['billing_postal_code'] ?? null,
                'billing_email' => $data['billing_email'] ?? null,
                'billing_name' => $data['billing_name'] ?? null,
                'billing_phone' => $data['billing_phone'] ?? null,
                'card_brand' => $data['card_brand'] ?? null,
                'card_country' => $data['card_country'] ?? null,
                'card_exp_month' => $data['card_exp_month'] ?? null,
                'card_exp_year' => $data['card_exp_year'] ?? null,
                'card_fingerprint' => $data['card_fingerprint'] ?? null,
                'card_funding' => $data['card_funding'] ?? null,
                'card_last_4' => $data['card_last_4'] ?? null,
                'type' => $data['type'] ?? null,
                'customer' => $data['customer'] ?? null,
                'user_id' => $data['user_id'],
            ]);

            return $stripePaymentMethod;
        });
        throw new GeneralException('error');
    }

    public function createFromStripeResponse(array $data)
    {
        return DB::transaction(function () use ($data) {
            $stripePaymentMethod = parent::create([
                'method_id' => $data['id'],
                'billing_city' => $data['billing_details']['address']['city'] ?? null,
                'billing_state' => $data['billing_details']['address']['state'] ?? null,
                'billing_country' => $data['billing_details']['address']['country'] ?? null,
                'billing_address_line_1' => $data['billing_details']['address']['line1'] ?? null,
                'billing_address_line_2' => $data['billing_details']['address']['line2'] ?? null,
                'billing_postal_code' => $data['billing_details']['address']['postal_code'] ?? null,
                'billing_email' => $data['billing_details']['email'] ?? null,
                'billing_name' => $data['billing_details']['name'] ?? null,
                'billing_phone' => $data['billing_details']['phone'] ?? null,
                'card_brand' => $data['card']['brand'] ?? null,
                'card_country' => $data['card']['country'] ?? null,
                'card_exp_month' => $data['card']['exp_month'] ?? null,
                'card_exp_year' => $data['card']['exp_year'] ?? null,
                'card_fingerprint' => $data['card']['fingerprint'] ?? null,
                'card_funding' => $data['card']['funding'] ?? null,
                'card_last_4' => $data['card']['last4'] ?? null,
                'type' => $data['type'] ?? null,
                'customer' => $data['customer'] ?? null,
                'user_id' => $data['user_id'],
            ]);

            return $stripePaymentMethod;
        });
        throw new GeneralException('error');
    }

    public function update(StripePaymentMethod $stripePaymentMethod, array $data){
        return DB::transaction(function () use ($stripePaymentMethod, $data){
           if ($stripePaymentMethod->update([
               'method_id' => $data['method_id'] ?? $stripePaymentMethod->method_id,
               'billing_city' => $data['billing_city'] ?? $stripePaymentMethod->billing_city,
               'billing_state' => $data['billing_state'] ?? $stripePaymentMethod->billing_state,
               'billing_country' => $data['billing_country'] ?? $stripePaymentMethod->billing_country,
               'billing_address_line_1' => $data['billing_address_line_1'] ?? $stripePaymentMethod->billing_address_line_1,
               'billing_address_line_2' => $data['billing_address_line_2'] ?? $stripePaymentMethod->billing_address_line_2,
               'billing_postal_code' => $data['billing_postal_code'] ?? $stripePaymentMethod->billing_postal_code,
               'billing_email' => $data['billing_email'] ?? $stripePaymentMethod->billing_email,
               'billing_name' => $data['billing_name'] ?? $stripePaymentMethod->billing_name,
               'billing_phone' => $data['billing_phone'] ?? $stripePaymentMethod->billing_phone,
               'card_brand' => $data['card_brand'] ?? $stripePaymentMethod->card_brand,
               'card_country' => $data['card_country'] ?? $stripePaymentMethod->card_country,
               'card_exp_month' => $data['card_exp_month'] ?? $stripePaymentMethod->card_exp_month,
               'card_exp_year' => $data['card_exp_year'] ?? $stripePaymentMethod->card_exp_year,
               'card_fingerprint' => $data['card_fingerprint'] ?? $stripePaymentMethod->card_fingerprint,
               'card_funding' => $data['card_funding'] ?? $stripePaymentMethod->card_funding,
               'card_last_4' => $data['card_last_4'] ?? $stripePaymentMethod->card_last_4,
               'type' => $data['type'] ?? $stripePaymentMethod->type,
               'customer' => $data['customer'] ?? $stripePaymentMethod->customer,
               'user_id' => $data['user_id'] ?? $stripePaymentMethod->user_id,
           ])){
               return $stripePaymentMethod;
           };
        });

        throw new GeneralException('error');
    }
}
