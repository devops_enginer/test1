<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;
use App\Models\TapCharge;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class TapChargeRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return TapCharge::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $tapCharge = parent::create([
                'amount' => $data['amount'],
                'charge_id' => $data['charge_id'],
                'payment_method_id' => $data['payment_method_id'] ?? null,
                'user_id' => $data['user_id'],
            ]);

            return $tapCharge;
        });
        throw new GeneralException('error');
    }

    public function createFromTapResponse(array $data)
    {
        return DB::transaction(function () use ($data) {
            $tapCharge = parent::create([
                'charge_id' => $data['id'],
                'amount' => $data['amount'],
                'payment_method_id' => $data['payment_method_id'] ?? null,
                'user_id' => $data['user_id'],
            ]);

            return $tapCharge;
        });
        throw new GeneralException('error');
    }

    public function update(TapCharge $tapCharge, array $data){
        return DB::transaction(function () use ($tapCharge, $data){
           if ($tapCharge->update([
               'amount' => $data['amount'] ?? $tapCharge->amount,
               'charge_id' => $data['charge_id'] ?? $tapCharge->charge_id,
               'payment_method_id' => $data['payment_method_id'] ?? $tapCharge->payment_method_id,
               'user_id' => $data['user_id'] ?? $tapCharge->user_id,
           ])){
               return $tapCharge;
           };
        });

        throw new GeneralException('error');
    }
}
