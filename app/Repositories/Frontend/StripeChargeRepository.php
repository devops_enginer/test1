<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;
use App\Models\StripeCharge;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\BaseRepository;
use App\Traits\UploadFiles;

class StripeChargeRepository extends BaseRepository
{
    use UploadFiles;

    public function model()
    {
        return StripeCharge::class;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $stripeCharge = parent::create([
                'payment_intent' => $data['payment_intent'],
                'payment_method' => $data['payment_method'],
                'amount' => $data['amount'],
                'charge_id' => $data['charge_id'],
                'currency' => $data['currency'],
                'customer' => $data['customer'],
                'receipt_url' => $data['receipt_url'],
                'payment_method_id' => $data['payment_method_id'],
                'user_id' => $data['user_id'],
            ]);

            return $stripeCharge;
        });
        throw new GeneralException('error');
    }

    public function createFromStripeResponse(array $data)
    {
        return DB::transaction(function () use ($data) {
            $stripeCharge = parent::create([
                'payment_intent' => $data['id'],
                'payment_method' => $data['payment_method'] ?? '',
                'amount' => $data['amount'] ?? 0,
                'charge_id' => $data['charges']['data'][0]['id'] ?? '',
                'currency' => $data['currency'] ?? '',
                'customer' => $data['customer'] ?? '',
                'receipt_url' => $data['charges']['data'][0]['receipt_url'] ?? '',
                'payment_method_id' => $data['payment_method_id'],
                'user_id' => $data['user_id'],
            ]);

            return $stripeCharge;
        });
        throw new GeneralException('error');
    }

    public function update(StripeCharge $stripeCharge, array $data){
        return DB::transaction(function () use ($stripeCharge, $data){
           if ($stripeCharge->update([
               'payment_intent' => $data['payment_intent'] ?? $stripeCharge->payment_intent,
               'payment_method' => $data['payment_method'] ?? $stripeCharge->payment_method,
               'amount' => $data['amount'] ?? $stripeCharge->amount,
               'charge_id' => $data['charge_id'] ?? $stripeCharge->charge_id,
               'currency' => $data['currency'] ?? $stripeCharge->currency,
               'customer' => $data['customer'] ?? $stripeCharge->customer,
               'receipt_url' => $data['receipt_url'] ?? $stripeCharge->receipt_url,
               'payment_method_id' => $data['payment_method_id'] ?? $stripeCharge->payment_method_id,
               'user_id' => $data['user_id'] ?? $stripeCharge->user_id,
           ])){
               return $stripeCharge;
           };
        });

        throw new GeneralException('error');
    }
}
