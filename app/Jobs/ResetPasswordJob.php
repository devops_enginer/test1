<?php

namespace App\Jobs;

use App\Http\Services\MailService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPasswordMail;

class ResetPasswordJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('mail send start');

        Mail::to($this->data['email'])->send(new ResetPasswordMail($this->data['token']));

        Log::info('mail send end');
    }

    public function getMailData($userData){
        $data = [
            'from' => [
                'email' => 'info@bajadesigns.ae'
            ],
            'personalizations' => [
                [
                    'to' => [
                        [
                            'email' => $userData['userEmail']
                        ]
                    ],
                    'dynamic_template_data' => [
                        'phone_number' => $userData['userPhone'],
                        'email' => $userData['userEmail'],
                        'reset_token' => $userData['resetToken'],
                    ],
                ]
            ],
        ];

        $data['template_id'] = 'd-229ef9b88e2743678dca22507914a0a7';
        return $data;
    }
}
