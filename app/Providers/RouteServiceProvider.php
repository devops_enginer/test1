<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/admin/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    protected $namespace = 'App\\Http\\Controllers';
    private $adminNamespace = 'App\\Http\\Controllers\\Admin';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {

            $this->mapApiRoutes();

            $this->mapApiV1Routes('v1', $this->namespace . '\\Api\\V1');

            $this->mapWebRoutes();

            $this->mapAdminWebRoutes();

        });
    }

    protected function mapAdminWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/home.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/country.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/city.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/user.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/promotional_banner.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/service.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/test.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/clinic.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/appointment.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/doctor_appointment.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/booking.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/doctor_booking.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/promo_code.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/static_content.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/notification.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/reasons.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/doctor.php'));

        Route::middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin/doctor_category.php'));
    }

    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));

        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/payment/stripe.php'));

        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/payment/tap.php'));
    }

    protected function mapApiRoutes()
    {
        $namespace = $this->namespace . '\\Api';

        Route::prefix('api')
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api.php'));
    }

    protected function mapApiV1Routes($version, $namespace)
    {
        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/country.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/city.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/home.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/auth.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/users.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/service.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/doctor_category.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/appointments.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/doctor_appointments.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/clinics.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/reasons.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/stripe_payment.php'));

        Route::prefix('api/' . $version)
            ->middleware('api')
            ->namespace($namespace)
            ->group(base_path('routes/api/' . $version . '/tap_payment.php'));
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
