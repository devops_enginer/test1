<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClinicTest extends Model
{
    use HasFactory;

    protected $table = 'clinic_tests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'test_id',
        'clinic_id',
        'sale_price',
        'test_price',
    ];

    public function test(){
        return $this->belongsTo(Test::class, 'test_id');
    }

    public function clinic(){
        return $this->belongsTo(Clinic::class, 'clinic_id');
    }
}
