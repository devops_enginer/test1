<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StripeCharge extends Model
{
    use HasFactory;

    protected $table = 'stripe_charges';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'payment_intent',
        'payment_method',
        'amount',
        'charge_id',
        'currency',
        'customer',
        'receipt_url',
        'payment_method_id',
        'user_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function paymentMethod(){
        return $this->belongsTo(StripePaymentMethod::class, 'payment_method_id');
    }
}
