<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    use HasFactory;

    protected $table = 'reasons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'text',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function bookings(){
        return $this->hasMany(Booking::class, 'reason_id');
    }
}
