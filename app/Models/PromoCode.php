<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    use HasFactory;

    protected $table = 'promo_codes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'type',
        'value',
        'limit',
        'expiry_date',
    ];

    public function getTypeTextAttribute(){
        if($this->type == FIXED_CODE_TYPE) return 'Fixed Amount';
        if($this->type == PERCENTAGE_CODE_TYPE) return 'Percentage';
        return '';
    }
}
