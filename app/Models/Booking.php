<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $table = 'bookings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'comment',
        'reason_id',
        'status',
        'user_id',
        'appointment_time_id',
        'is_rescheduled',
        'payment_status',
        'price'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function getStatusTextAttribute(){
        if($this->status == PENDING_BOOKING_STATUS) return 'Booked';
        if($this->status == COMPLETED_BOOKING_STATUS) return 'Completed';
        if($this->status == CANCELED_BOOKING_STATUS) return 'Canceled';
        return '';
    }

    public function getPaymentStatusTextAttribute(){
        if($this->payment_status == PAID_BOOKING_PAYMENT_STATUS) return 'Paid';
        if($this->payment_status == UNPAID_BOOKING_PAYMENT_STATUS) return 'Unpaid';
        return '';
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function appointmentTime(){
        return $this->belongsTo(AppointmentTime::class, 'appointment_time_id');
    }

    public function result(){
        return $this->hasOne(Result::class, 'booking_id')->with('parameters');
    }

    public function reason(){
        return $this->belongsTo(Reason::class, 'reason_id');
    }
}
