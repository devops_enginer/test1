<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'full_name',
        'email',
        'email_verified_at',
        'password',
        'phone_number',
        'photo',
        'google_id',
        'facebook_id',
        'apple_id',
        'account_type',
        'address_line_1',
        'address_line_2',
        'language',
        'city_id',
        'country_id',
        'reset_token',
        'stripe_id',
        'pm_type',
        'pm_last_four',
        'trial_ends_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'reset_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getPhotoAttribute(){
        return isset($this->attributes['photo']) && $this->attributes['photo'] ? (url('/') . '/storage/' . $this->attributes['photo']) : null;
    }

    public function getRealPhotoAttribute(){
        return $this->attributes['photo'];
    }

    public function country(){
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id');
    }

    public function deviceTokens(){
        return $this->hasMany(DeviceToken::class, 'user_id');
    }

    public function notifications(){
        return $this->hasMany(Notification::class, 'user_id');
    }

    public function bookings(){
        return $this->hasMany(Booking::class, 'user_id');
    }

    public function subscriptions(){
        return $this->hasMany(Subscription::class, 'user_id');
    }

    public function stripPaymentMethods(){
        return $this->hasMany(StripePaymentMethod::class, 'user_id');
    }
}
