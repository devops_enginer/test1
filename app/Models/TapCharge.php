<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TapCharge extends Model
{
    use HasFactory;

    protected $table = 'tap_charges';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'amount',
        'charge_id',
        'payment_method_id',
        'user_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function paymentMethod(){
        return $this->belongsTo(TapPaymentMethod::class, 'payment_method_id');
    }
}
