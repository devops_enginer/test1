<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StripePaymentMethod extends Model
{
    use HasFactory;

    protected $table = 'stripe_payment_methods';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'method_id',
        'billing_city',
        'billing_state',
        'billing_country',
        'billing_address_line_1',
        'billing_address_line_2',
        'billing_postal_code',
        'billing_email',
        'billing_name',
        'billing_phone',
        'card_brand',
        'card_country',
        'card_exp_month',
        'card_exp_year',
        'card_fingerprint',
        'card_funding',
        'card_last_4',
        'type',
        'customer',
        'user_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
