<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $table = 'appointments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'date',
        'test_id',
        'clinic_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function test(){
        return $this->belongsTo(Test::class, 'test_id');
    }

    public function clinic(){
        return $this->belongsTo(Clinic::class, 'clinic_id');
    }

    public function times(){
        return $this->hasMany(AppointmentTime::class, 'appointment_id');
    }

    public function availableTimes(){
        return $this->hasMany(AppointmentTime::class, 'appointment_id')->where('user_id', null);
    }
}
