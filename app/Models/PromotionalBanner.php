<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PromotionalBanner extends Model
{
    use HasFactory;

    protected $table = 'promotional_banners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'image',
        'test_id',
        'order',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function getImageAttribute(){
        return isset($this->attributes['image']) && $this->attributes['image'] ? (url('/') . '/storage/' . $this->attributes['image']) : null;
    }

    public function getRealImageAttribute(){
        return $this->attributes['image'];
    }

    public function test(){
        return $this->belongsTo(Test::class, 'test_id');
    }
}
