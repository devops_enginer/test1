<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;

    protected $table = 'doctors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'image',
        'address',
        'lat',
        'lng',
        'phone',
        'country_id',
        'city_id',
        'rating',
        'description',
        'experience',
        'category_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function getImageAttribute(){
        return isset($this->attributes['image']) ? (url('/') . '/storage/' . $this->attributes['image']) : null;
    }

    public function getRealImageAttribute(){
        return $this->attributes['image'];
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id');
    }

    public function country(){
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function category(){
        return $this->belongsTo(DoctorCategory::class, 'category_id');
    }
}
