<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use HasFactory;

    protected $table = 'results';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'path',
        'appointment_time_id',
        'booking_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function getPathAttribute(){
        return isset($this->attributes['path']) && $this->attributes['path'] ? (url('/') . '/storage/' . $this->attributes['path']) : null;
    }

    public function getRealPathAttribute(){
        return $this->attributes['path'];
    }

    public function appointmentTime(){
        return $this->belongsTo(AppointmentTime::class, 'appointment_time_id');
    }

    public function booking(){
        return $this->belongsTo(Booking::class, 'booking_id');
    }

    public function parameters(){
        return $this->hasMany(BookingResultParameter::class, 'result_id');
    }
}
