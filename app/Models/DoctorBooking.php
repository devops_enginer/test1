<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorBooking extends Model
{
    use HasFactory;

    protected $table = 'doctor_bookings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'comment',
        'reason_id',
        'status',
        'user_id',
        'doctor_appointment_time_id',
        'is_rescheduled',
        'prescription',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function getPrescriptionAttribute(){
        return isset($this->attributes['prescription']) && $this->attributes['prescription'] ? (url('/') . '/storage/' . $this->attributes['prescription']) : null;
    }

    public function getRealPrescriptionAttribute(){
        return $this->attributes['prescription'];
    }

    public function getStatusTextAttribute(){
        if($this->status == PENDING_BOOKING_STATUS) return 'Booked';
        if($this->status == COMPLETED_BOOKING_STATUS) return 'Completed';
        if($this->status == CANCELED_BOOKING_STATUS) return 'Canceled';
        return '';
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function appointmentTime(){
        return $this->belongsTo(DoctorAppointmentTime::class, 'doctor_appointment_time_id');
    }

    public function reason(){
        return $this->belongsTo(Reason::class, 'reason_id');
    }
}
