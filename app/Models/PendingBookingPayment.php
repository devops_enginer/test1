<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PendingBookingPayment extends Model
{
    use HasFactory;

    protected $table = 'pending_booking_payments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'appointment_time_id',
        'payment_status',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function appointmentTime(){
        return $this->belongsTo(AppointmentTime::class, 'appointment_time_id');
    }
}
