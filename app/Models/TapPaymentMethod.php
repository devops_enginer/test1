<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TapPaymentMethod extends Model
{
    use HasFactory;

    protected $table = 'tap_payment_methods';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'token',
        'customer_id',
        'card_id',
        'card_type',
        'customer_name',
        'customer_email',
        'customer_phone',
        'card_holder_name',
        'card_number',
        'card_exp_month',
        'card_exp_year',
        'card_cvc',
        'user_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
