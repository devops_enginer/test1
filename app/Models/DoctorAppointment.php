<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorAppointment extends Model
{
    use HasFactory;

    protected $table = 'doctor_appointments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'date',
        'doctor_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function doctor(){
        return $this->belongsTo(Doctor::class, 'doctor_id');
    }

    public function times(){
        return $this->hasMany(DoctorAppointmentTime::class, 'doctor_appointment_id');
    }

    public function availableTimes(){
        return $this->hasMany(DoctorAppointmentTime::class, 'doctor_appointment_id')->where('user_id', null);
    }
}
