<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppointmentTime extends Model
{
    use HasFactory;

    protected $table = 'appointment_times';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'time',
        'appointment_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function getTimeAttribute(){
        return date('H:i', strtotime($this->attributes['time']));
    }

    public function appointment(){
        return $this->belongsTo(Appointment::class, 'appointment_id');
    }

    public function bookings(){
        return $this->hasMany(Booking::class, 'appointment_time_id');
    }

    public function results(){
        return $this->hasMany(Result::class, 'appointment_time_id');
    }
}
