<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;

    protected $table = 'tests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'description',
        'image',
        'thumbnail',
        'is_featured',
        'service_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function getImageAttribute(){
        return isset($this->attributes['image']) && $this->attributes['image'] ? (url('/') . '/storage/' . $this->attributes['image']) : null;
    }

    public function getThumbnailAttribute(){
        return isset($this->attributes['thumbnail']) && $this->attributes['thumbnail'] ? (url('/') . '/storage/' . $this->attributes['thumbnail']) : null;
    }

    public function getRealImageAttribute(){
        return $this->attributes['image'];
    }

    public function getRealThumbnailAttribute(){
        return $this->attributes['thumbnail'];
    }

    public function service(){
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function clinics(){
        return $this->belongsToMany(Clinic::class, 'clinic_tests', 'test_id', 'clinic_id')->withPivot('sale_price', 'test_price');
    }

    public function notifications(){
        return $this->hasMany(Notification::class, 'test_id');
    }

    public function banners(){
        return $this->hasMany(PromotionalBanner::class, 'test_id');
    }
}
