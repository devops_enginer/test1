<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorCategory extends Model
{
    use HasFactory;

    protected $table = 'doctor_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'icon',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function getIconAttribute(){
        return isset($this->attributes['icon']) && $this->attributes['icon'] ? (url('/') . '/storage/' . $this->attributes['icon']) : null;
    }

    public function getRealIconAttribute(){
        return $this->attributes['icon'];
    }

    public function doctors(){
        return $this->hasMany(Doctor::class, 'category_id');
    }
}
