<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'currency',
        'flag',
    ];

    public function getFlagAttribute(){
        return isset($this->attributes['flag']) && $this->attributes['flag'] ? (url('/') . '/storage/' . $this->attributes['flag']) : null;
    }

    public function getRealFlagAttribute(){
        return $this->attributes['flag'];
    }

    public function users(){
        return $this->hasMany(User::class, 'country_id');
    }

    public function cities(){
        return $this->hasMany(City::class, 'country_id');
    }
}
