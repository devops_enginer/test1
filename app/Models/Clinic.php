<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clinic extends Model
{
    use HasFactory;

    protected $table = 'clinics';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'image',
        'address',
        'lat',
        'lng',
        'country_id',
        'city_id',
        'rating',
        'number_of_ratings',
        'working_hours',
        'is_default',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function getImageAttribute(){
        return isset($this->attributes['image']) && $this->attributes['image'] ? (url('/') . '/storage/' . $this->attributes['image']) : null;
    }

    public function getRealImageAttribute(){
        return $this->attributes['image'];
    }

    public function tests(){
        return $this->belongsToMany(Test::class, 'clinic_tests', 'clinic_id', 'test_id')->withPivot('sale_price', 'test_price');
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id');
    }

    public function country(){
        return $this->belongsTo(Country::class, 'country_id');
    }
}
