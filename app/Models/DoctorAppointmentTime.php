<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorAppointmentTime extends Model
{
    use HasFactory;

    protected $table = 'doctor_appointment_times';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'time',
        'doctor_appointment_id',
        'price',
        'user_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function getTimeAttribute(){
        return date('H:i', strtotime($this->attributes['time']));
    }

    public function appointment(){
        return $this->belongsTo(DoctorAppointment::class, 'doctor_appointment_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function bookings(){
        return $this->hasMany(DoctorBooking::class, 'doctor_appointment_time_id');
    }
}
